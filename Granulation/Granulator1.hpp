/** @brief File Granulator1.hpp, that implements a granulator for generating an alphabet of symbols in strategy 1 og GRALG.
 *
 * @file Granulator1.hpp
 * @author Filippo Bianchi
 */

#ifndef GRANULATOR1_HPP_
#define GRANULATOR1_HPP_

//STD INCLUDES
#include <string>
#include <vector>
#include <list>


/** @brief This class generates an alphabet of symbols starting from a set of expanded graphs of the training set.
 *
 *  An ensemble of partitions is generated. Clusters of the ensemble are filtered, using a cost function, and then the
 *  alphabet of symbols is extracted from clusters with higher quality value.
 *  Each symbols is represented with a cluster and a set of parameters of quality.
 *
 * <b>Template Parameters</b>
 *  <table class="contents">
 *  <tr>
 *      <td class="indexkey"><b>Name</b></td>
 *      <td class="indexkey"><b>Description</b></td>
 *  </tr>
 *  <tr>
 *  	<td class="indexvalue">GraphType</td>
 *  	<td class="indexvalue">The type of graph processed.</td>
 *  </tr>
 *  <tr>
 *  	<td class="indexvalue">ClusteringEnsembles</td>
 *  	<td class="indexvalue">A class which implements a function for generating an ebsembles of clustering partitions.</td>
 *  </tr>
 *  <tr>
 *  	<td class="indexvalue">GranuleType</td>
 *  	<td class="indexvalue">A class which define the type of the symbols of the alphabet.</td>
 *  </tr>
 *  </table>
 *
 *  <b>Parameters Summary</b>
 *   <table class="contents">
 *   <tr>
 *  	<td class="indexkey"><b>Name</b></td>
 *  	<td class="indexkey"><b>Domain</b></td>
 *  	<td class="indexkey"><b>Description</b></td>
 *  	<td class="indexkey"><b>Default</b></td>
 *   </tr>
 *   <tr>
 *  	<td class="indexvalue">ni</td>
 *  	<td class="indexvalue">[0, 1]</td>
 *  	<td class="indexvalue">Weight in cost function F of the cardinality vs the complexity.</td>
 *  	<td class="indexvalue">0.5</td>
 *   </tr>
 *   <tr>
 *  	<td class="indexvalue">t_F</td>
 *  	<td class="indexvalue">[0, 1]</td>
 *  	<td class="indexvalue">Thresold for cluster's cost function F.</td>
 *  	<td class="indexvalue">0.7</td>
 *   </tr>
 *   <tr>
 *  	<td class="indexvalue">eps</td>
 *  	<td class="indexvalue">[0, inf]</td>
 *  	<td class="indexvalue">Tolerance value in symbols recognition.</td>
 *  	<td class="indexvalue">1.1</td>
 *   </tr>
 *  </table>
 */
template <class ClusteringEnsembles, class GranuleType, class GraphType>
class Granulator1 {

public:

	typedef ClusteringEnsembles EnsembleStrategy;

    //MEMBER FUNCTION
	/**
	 * Function that given a set of samples generates an alphabet of symbols
	 * @param[in] samples Set of structures from which the alphabet is going to be extracted.
	 * @param[out] symbols Data structure where the newly generated alphabet is going to be stored.
	 */
    template <typename SamplesContainer, typename SymbolsContainer, typename LabelsContainer>
    void Granulate(const SamplesContainer& samples, const LabelsContainer& samplesLabels, SymbolsContainer& symbols, const LabelsContainer& Labels);


    //ACCESS

    /**
     * R/W access to Clustering ensembles strategy
     */
    ClusteringEnsembles& EnsembleAgent(){
        return mClusteringEnsemblesStrategy;
    }

    /**
     * R-only access to Clustering ensembles strategy
     */
    const ClusteringEnsembles& EnsembleAgent() const{
	   return mClusteringEnsemblesStrategy;
    }

    /**
     * R/W access to the variable ni, used in the fuction that evaluate the goodness of a cluster for becoming a symbol
     */
    spare::RealType& Ni(){
        return ni;
    }

    /**
     * R-Only access to the variable ni, used in the fuction that evaluate the goodness of a cluster for becoming a symbol
     */
    const spare::RealType& Ni() const{
           return ni;
    }

    /**
     * R/W access to the variable phi, used as a threshold for cluster purity
     */
    spare::RealType& Phi(){
       return phi;
    }

    /**
     * R-Only access to the variable phi, used as a threshold for cluster purity
     */
    const spare::RealType& Phi() const{
	   return phi;
    }

    /**
     * R/W access to the function's F threshold, used for evaluating the candidates clusters for becoming symbols
     */
    spare::RealType& T_F(){
        return t_F;
    }

    /**
     * R-Only access to the function's F threshold, used for evaluating the candidates clusters for becoming symbols
     */
    const spare::RealType& T_F() const{
	    return t_F;
    }

    /**
     * R/W access to a scalar value representing the tolerance used recognize an incoming pattern as a given symbol
     */
    spare::RealType& Eps(){
    	return eps;
    }

    /**
     * R-Only access to a scalar value representing the tolerance used recognize an incoming pattern as a given symbol
     */
    const spare::RealType& Eps() const{
		return eps;
	}

	/**
	 * R/W access to compacntess computational mode
	 */
	bool& CompMed(){
		return compMed;
	}

	/**
	 * R-Only access to compacntess computational mode
	 */
	const bool& CompMed() const{
		return compMed;
	}

    //CONSTRUCTORS
    /**
     * default constructor
     */
    Granulator1(){
    	ni = 0.5;
    	eps = 1.1;
    	t_F = 0.7;
    	compMed = false;
    	phi = 0.5;
    }

    /*
     * Constructor
     * @param[in] n value of ni
     * @param[in] e value of eps
     * @param[in] c value of cardinality threshold
     * @param[in] compactness cost modality
     */
    Granulator1(spare::RealType n, spare::RealType fi, spare::RealType e, spare::RealType f, bool compM){
        ni = n;
        eps = e;
        t_F = f;
        compMed = compM;
        phi = fi;
    }


private:

    typedef typename ClusteringEnsembles::partitionType partition;
    typedef typename ClusteringEnsembles::partitionContainerType partitionContainer;
    typedef typename ClusteringEnsembles::thetaContainerType thetaContainer;
    typedef typename ClusteringEnsembles::LabelContainerType labelsType;
    typedef std::vector<typename ClusteringEnsembles::LabelContainerType> labelsContainer;
    typedef typename partition::value_type MinSodRep;
    typedef typename MinSodRep::SodVector sodVector; //Warning: works only with MinSod representative (correct assuming we are working with graphs)


    /**
     * Clustering ensambles strategy.
     */
    ClusteringEnsembles mClusteringEnsemblesStrategy;

    /**
     * Container of partitions generated by clustering algorithm.
     */
    partitionContainer pc;

    /**
     * Container of theta determination which generated a partition.
     */
    thetaContainer tc;

    /**
     * Container of the labels assigned to the pattern in each partition.
     */
    labelsContainer lc;

    /**
     * Threshold for symbol matching.
     */
    spare::RealType eps;

    /**
     * Parameters used for evaluating the quality of a cluster candidate for becoming a symbol.
     */
    spare::RealType ni, phi, t_F;

    /**
     * Flag for computing compactness using median or mean
     */
    bool compMed;

    /*
     * Private function used for generating the alphabed starting from the ensemble of
     * clusters and using the quality parameters
     */
    template <typename SymbolsContainer, typename LabelsContainer>
    void getSymbol(SymbolsContainer& symC, spare::NaturalType nSub, LabelsContainer& samplesLabels, LabelsContainer& Labels) const;
};


// IMPL
template <class ClusteringEnsembles, class GranuleType, class GraphType>
template <typename SamplesContainer, typename SymbolsContainer, typename LabelsContainer>
void Granulator1<ClusteringEnsembles, GranuleType, GraphType>::Granulate(const SamplesContainer& samples, const LabelsContainer& samplesLabels, SymbolsContainer& symbols, const LabelsContainer& Labels)
{
    //determine the ensembles of partitions using a clustering algorithm
	mClusteringEnsemblesStrategy.Process(samples);
    pc = mClusteringEnsemblesStrategy.getPartitions();
    tc = mClusteringEnsemblesStrategy.getThetaValues();
    lc = mClusteringEnsemblesStrategy.PartitionsLabels();

//    //DEBUG
//    std::cout<<"Processing "<<samples.size()<<"elements. " << "Total number of partitions generated: " << pc.size() <<std::endl;
//    for(unsigned int i = 0; i< pc.size(); i++){
//        std::cout <<"Partition "<<i<<"--> #Clusters: "<< pc[i].size() << ", theta value: "<<tc[i]<<std::endl;
//    }

    //extract symbols from the ensemble
    getSymbol(symbols, samples.size(), samplesLabels, Labels);

}


template <class ClusteringEnsembles, class GranuleType, class GraphType>
template <typename SymbolsContainer, typename LabelsContainer>
void Granulator1<ClusteringEnsembles, GranuleType, GraphType>::getSymbol(SymbolsContainer& symC, spare::NaturalType nSub, LabelsContainer& samplesLabels, LabelsContainer& Labels) const
{
    partition Ptemp; 	//partizione m-esima
    labelsType Ltemp;	//etichette di CLUSTER della partizione m-esima
    MinSodRep MStemp;	//cluster i-esimo nella partizione m-esima
    sodVector sodv;
    for(spare::NaturalType m=0; m <pc.size(); m++){
        spare::RealType theta = tc[m];
        Ptemp = pc[m];
        Ltemp = lc[m];

		for(spare::NaturalType i=0; i <Ptemp.size(); i++){
            MStemp = Ptemp[i];

            if(MStemp.GetCount() > 1 && MStemp.GetCount() < nSub){ //filter clusters with 1 element or with all the elements

            	//compute cardinality cost
				spare::RealType card = 1. - (spare::RealType)(MStemp.GetCount())/(spare::RealType)nSub;
				card = card*0.1;

				//compute compactness cost
				spare::NaturalType mMinSodIndex = MStemp.GetMinSodIndex();
				spare::RealType comp;
				spare::NaturalType sampleSize = MStemp.GetSamples().size();
				if(compMed==false){
					//mean version
					sodVector sv = MStemp.GetSods();
					comp = (spare::RealType)sv[mMinSodIndex]/(spare::RealType)(sampleSize - 1);
				}
				else{
					//median version
					std::list<spare::RealType> l;
					for(spare::NaturalType i = 0; i < sampleSize; i++){
						l.push_back(MStemp.DissimilarityMatrix()(i,mMinSodIndex));
					}
					l.sort();
					std::list<spare::RealType>::iterator lIt = l.begin();
					if(sampleSize%2 == 0){
						advance(lIt, (sampleSize-2)/2);
						spare::RealType c1 = *lIt++;
						spare::RealType c2 = *lIt;
						comp = (c1+c2)/2;
					}else{
						advance(lIt, (sampleSize-1)/2);
						comp = *lIt;
					}
				}

				//compute entropy
				std::vector<spare::NaturalType> classCounter;
				classCounter.resize(Labels.size());	//conta quante occorrenze ci sono relativamente a ciascuna classe

				for(spare::NaturalType j=0; j<Ltemp.size(); j++){
					if(Ltemp[j]==i){	//scorre tutti i pattern contenuti nel cluster (MSTemp) i-esimo
						for(spare::NaturalType k=0; k<classCounter.size(); k++){	//incrementa i campi del classcounter relativo al cluster i-esimo in base alla classe dei pattern contenuti al suo interno
							if(Labels[k]==samplesLabels[j])
								classCounter[k]++;
						}
					}
				}

				spare::RealType en=0;
				for(spare::NaturalType j=0; j<classCounter.size();j++){
					spare::RealType pj = (spare::RealType)classCounter[j]/(spare::RealType)MStemp.GetCount();
					if(pj==0)
						en=0;
					else
						en += pj*log2(1.0/pj);
					//std::cout<<"class "<<Labels[j]<<": "<<classCounter[j]<<std::endl;
				}
				en = en/log2((spare::RealType)Labels.size());
				//std::cout<<"entropy: "<<en<<std::endl;


				//compute F
				spare::RealType F = phi*en + (1-phi)*((1 - ni)*card + ni*comp);
				//cout<<"entropy: "<<en<<", card: "<<card<<", comp: "<<comp<<", F:"<<F<<", phi: "<<phi<<endl;

				if(F <= t_F)
				{
					//extract MinSod representative
					GraphType g;
					g = MStemp.getRepresentativeSample();

					//extract diameter
					spare::RealType r = comp*eps;

					GranuleType newgr(g, r, theta, F, card, comp);

					symC.push_back(newgr);
				}
            }
        }
    }
    //std::cout<<"enMed: "<<enMed/(spare::RealType)alphabetSize<<", enMin: "<<enMin<<", enMax: "<<enMax<<", alphabetSize: "<<alphabetSize<<std::endl;
}

#endif /* GRANULATOR1_HPP_ */

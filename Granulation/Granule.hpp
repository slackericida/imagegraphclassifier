/** @brief File Granule.hpp, that contains the definition of the granule class.
 *
 * @file Granule.hpp
 * @author Filippo Bianchi
 */

#ifndef GRANULE_HPP
#define GRANULE_HPP

//SPARE INCLUDES
#include <spare/SpareTypes.hpp>


/** @brief The file contains the granule class
 *
 * The granule is the generic symbol used in the alphabet which defines the
 * Rn vector in which the graphs is going to be transformed with the embedding tecnique
 *
 * <b>Template Parameters</b>
 *  <table class="contents">
 *  <tr>
 *      <td class="indexkey"><b>Name</b></td>
 *      <td class="indexkey"><b>Description</b></td>
 *  </tr>
 *  <tr>
 *  	<td class="indexvalue">StructureType</td>
 *  	<td class="indexvalue">Type of the graph of the dataset, which is the representative.</td>
 *  </tr>
 *  </table>
 */
template <class StructureType>
class Granule {

public:

	typedef StructureType RepresentantType;

    /**
     * Default constructor
     */
    Granule() {}

    /**
     * Constructor
     * @param[in] aSub: The substructure reference
     * @param[in] aDissMetric: the threshold that represent the maximum distance under which a generic pattern is associated to the granule
     * @param[in] t: the value of theta of the BSAS that generated the cluster this symbol represents
     * @param[in] f: the value of the fucntion (1 - ni)*card + ni*comp
     * @param[in] card: cardinality of the cluster this symbol represents
     * @param[in] comp: compactness of the cluster this symbol represents
     */
    Granule(StructureType& aSub, spare::RealType& aDissMetric, spare::RealType& t, spare::RealType& f, spare::RealType& ca, spare::RealType& co)
    {
        substructure=aSub;
        dissMetric=aDissMetric;
        theta = t;
        F = f;
        card = ca;
        comp = co;
    }


    /**
     * Read/Write access to the internal substructure.
     */
    StructureType& getSubstructure() { return substructure; }

    /**
     *  Read-only access to the internal substructure.
     */
    const StructureType& getSubstructure() const { return substructure; }

    /**
     * Read/Write access to the local metric threshold.
     */
    spare::RealType& getDissMetric() { return dissMetric; }

    /**
     * Read-only access to the local metric threshold.
     */
    const spare::RealType& getDissMetric() const { return dissMetric; }

    /**
     * Read/Write access to the theta.
     */
	spare::RealType& getTheta() { return theta; }

	/**
	 * Read-only access to the theta.
	 */
	const spare::RealType& getTheta() const { return theta; }

	/**
	 * Read/Write access to the F value.
	 */
	spare::RealType& getF() { return F; }

	/**
	 * Read-only access to the F value.
	 */
	const spare::RealType& getF() const { return F; }

	/**
	 * Read/Write access to the cardinality of the cluster.
	 */
	spare::RealType& getCard() { return card; }

	/**
	 * Read-only access to the cardinality of the cluster.
	 */
	const spare::RealType& getCard() const { return card; }

	/**
	 * Read/Write access to the compactness of the cluster.
	 */
	spare::RealType& getComp() { return comp; }

	/**
	 * Read-only access to the compactness of the cluster.
	 */
	const spare::RealType& getComp() const { return comp; }



private:

    /**
     * Minsod representative of the cluster.
     */
    StructureType substructure;

    /**
     * Tolerance value for associating a cluster to the symbol used during the granulation.
     */
    spare::RealType dissMetric;

    /**
     * Value of the cost function, which represent the "goodness" of the cluster in terms of compactness and cardinality.
     */
    spare::RealType F;

    /**
     * Theta of BSAS taht generated the cluster.
     */
    spare::RealType theta;

    /**
     * Cardinality of the cluster.
     */
    spare::RealType card;

    /**
     * Compactness of the cluster.
     */
    spare::RealType comp;

};



#endif


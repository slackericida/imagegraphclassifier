In order to work properly, the program requires the following software libraries:

* SPARE (http://sourceforge.net/p/libspare/home/Spare/)
* libboost-all-dev
* libarmadillo-dev
* libx11-6
* libpthread-stubs0-dev
* libjpeg-dev
* imagemagick
* CImg


Suppose you have a folder on the desktop with the datasets (eg. / Home / user / Desktop / SPImR1), you will have two dif-ferent types of tests. There are two different mode for running the program.

**Mode 1: manual segmentation + classification**

This first test consists of two phases. In the first you would use the segmentator and produce graphs relating to images of Training and Testing of a given problem. Take for example the problem "P01": go to the appropriate folder (for example which resides at location / home / user / Desktop / SPImR1 / P01) and creates two folders where else to go your graphs, and call them "TestGraphs" and "TrainingGraphs". Then you will need to first create graphs of training and then the test. To do that launches the executable "ImageSegmentation" from the command line, specifying the path of the source and destina-tion. So for example, go to the command line in the directory where the executable and type "./ImageSegmentation -1 / home / user / Desktop / SPImR1 / P01 / Training \ Sep / -2 / home / user / Desktop / SPImR1 / P01 / TrainingGraphs / ". This will create the graphs relating to the images of the training set of the problem P01. Obviously repeat the same process for the images of the Test Set, building graphs that will this time included in its portfolio TestGraphs.

This way you build your graph using the segmentaor with its default values. Obviously, these values will not go well for all the problems and then every time you will need to modify and control eye if the segmentation has been done well or not. To see the output of the segmentation and to change the parameters there are a number of commands that you list below.
* 1: sets the path of origin where fish pictures
* 2: set the destination path where to write graphs
* b: sets the value B (window size). I think you can leave it at the default value.
* g: sets the value wBrg
* n: set value wBin
* t: Sets the value tBin, namely the binarization threshold used to determine if a pixel is stable or unstable
* f: sets the value TFUs, used in the step of clustering. It is used to determine how much do I merge similar regions into a single region
* r: sets a value called Treg. This threshold is used to eliminate regions of connected pixels stable, if it is too small. It 'used to prevent spontaneous hypersegmentation.
* a: sets the parameter alpha. When you go to assign pixels unstable to stable regions use a similarity measure to de-cide the region most similar to which to assign the pixels unstable. This similarity is calculated considering the prox-imity of the actual pixels unstable representative from the centroid of the region stable and the similarity of the signatures of pixels unstable and centroid. The alpha parameter is used to give more weight to the proximity or similarity of the signatures (eg alpha = 1 gives weight only to the vicinity).
* R: used to remove the background. No need to touch this parameter.
* d: debug mode. This is ESSENTIAL to see if segmentation is going well or not. In this mode it is done to see how each image is segmented by a series of photos (you have to close the windoe that pops up every time to see the next photo).
First photo: original image.
Second photo: the results as binarization (the white pixels are stable areas, those black unstable areas).
Third photo: identification of connected regions. Each color corresponds to a region connected distinguished.
Fourth photo: step resorption pixel unstable.
Fifth photo: final result after application of the merger process. Please note, if the merger process does not take place (for example, when seven TFUs = 0), this image is not shown.



An example of how to call the segmentator is:
*./ImageSegmentation -1 /home/User/Desktop/SPImR1/P01/Test\ Set/ -2 /home/User/Desktop/SPImR1/P01/TestGraphs/ -g 0.33 -n 0.1 -t 0.15 -f 0.1 -r 0.001 -a 0.95 -d
	*
Obviously once you're sure to find the parameters by which it is segmented correctly, stop using the debug mode (-d).

At this point, after you create graphs of test and training we pass the classification, which will effect using the executable ImageGraphClassifier. This program will seek to classify the graphs contained in TestGraphs using graphs contained in TrainingGraphs using a classifier type Knn. This program uses a genetic algorithm to determine the value of the weights used in the dissimilarity measure between graphs and the sections to be considered for solving the problem (for example, a problem is helpful to consider the color, but not the roundness etc  ...). The output of this program will be printed on the screen and it will contains:

* the values of the weights used in the measure of dissimilarity between graphs (pAlpha, pBeta and pGamma)
* sections of the labels of node and arc considered to solve the problem. Each section is set to 1 if it is used or to 0 otherwise. The sections of the nodes are as follows: WXC, WYC, wCrCb, Sat, wBrg, wWlet, wArea, wSymm, wRnd, WCMP, wdir, WOR, while sections of the Arc labels are wdXc, wdYc, WDB. The value of the sections al-lows you to make useful analysis to see if the problem was solved by considering the fields actually relevant (for example, if the problem is to identify objects red or yellow objects fields related to the color, brightness and satura-tion are expected They are placed at the first, though not necessarily all, and 3).
* The error obtained on the Test Set. Of course, we want this number to be as low as possible.

Again you will have several options for starting the program:

* 1: sets the path where are the graphs of Training
* 2: sets the path where there are graphs of test
* s: seed the genetic algorithm. This value should be assigned randomly (to assign a random number from the com-mand line to do "-s $ RANDOM"
* e: number of evolutions of the genetic algorithm. The more you do, the better the chance to find a better solution, but of course it will take more time. A reasonable number is make a few dozen.
* r: resolution of the genetic algorithm. If it is too high you may not find the solution in the evolution available be-cause the search space is too large, if it is too low, the solution may be inaccurate. A reasonable range to set the resolution is [10, 1000].
* k: number of neighbors used in Knn to make the classification. To do the test choose odd numbers in the range [1, 7] (maximum can push up to 9).
* f: the evaluation of the goodness of each parameter setting is evaluated by performing several subdivisions of the original data. The number of these subdivisions is given precisely by f. obviously, increasing f increases the accu-racy of estimation of the error together with the time required for the exercise (this is especially important when you do the workout by segmenting the images). In general it is reasonable to a value of 5 (5-fold cross validation).


An example of how to call the classificator is:
*./main  -1 /home/User/Desktop/SPImR1/P01/TrainingGraphs/  -2 /home/User/Desktop/SPImR1/P01/TestGraphs/ -e 15 -s $RANDOM -k 3 -r 100 -f 5*

Summarizing: after you successfully create the data set of graphs of Training and Testing (having set in hand the various weights and controlled through the debug mode that everything worked) , throw a little ' run of the classifier , putting a number of evolutions enough , an acceptable resolution and trying with some different value of K. Launch every time with a different seed and then calculate the average error obtained .


**Mode 2- automatic segmentation with genetic algoritm and classification**

The segmentation in this second case is made inside of the genetic algorithm itself, that will automatically set the following parameters of the segmentator:

* wBrg
* wBin
* tBin
* tFus
* alpha
* tReg

The segmentator then will segment the image and produce as the first graph that this time, however, will NOT be written to the file, but will remain in memory and will be passed directly to the classifier. This second executable therefore will not produce any output file, but will return only the number for the performance achieved in the classification stage.

An example of use is then 

*./main  -1 /home/User/Desktop/SPImR1/P01/Training\ Set/  -2 /home/User/Desktop/SPImR1/P01/Test\ Set/ -e 15 -s $RANDOM -k 3 -r 100 -f 5*

Obviously, the time required by this executable is much higher than the previous one that simply ranking, as the segmentation phase must be repeated each time for the required number of fold. Moreover, since the search space wider than before it is reasonable to use a number of iterations in turn higher.
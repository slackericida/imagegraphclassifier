/*
 * ImageSegmentation.h
 *
 *  Created on: Jul 11, 2013
 *      Author: scardax
 */

#ifndef VIRTUAL_IMAGESEGMENTATION_H_
#define VIRTUAL_IMAGESEGMENTATION_H_

#include "TypeDefinitions.hpp"
#include "Image.h"
#include "FeatureExtraction.h"

class ImageSegmentation {
public:

	ImageSegmentation() {
		fExtract.getB() = 4;
		fExtract.getM() = 1;
	}

	//access
	const IntegerType& getB() const { return fExtract.getB(); }
	IntegerType& getB(){ return fExtract.getB(); }

	const IntegerType& getM() const { return fExtract.getM(); }
	IntegerType& getM(){ return fExtract.getM(); }

	virtual ~ImageSegmentation(){}

	virtual list<RegionType> segmentImage(Image mImg) = 0;

protected:
	FeatureExtraction fExtract;

};

#endif /* VIRTUAL_IMAGESEGMENTATION_H_ */

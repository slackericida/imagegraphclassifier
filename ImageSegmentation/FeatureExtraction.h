/*
 * FeatureExtraction.h
 *
 *  Created on: Jul 11, 2013
 *      Author: scardax
 */

#ifndef FEATUREEXTRACTION_H_
#define FEATUREEXTRACTION_H_
#include <boost/numeric/ublas/matrix.hpp>
#include <vector>
#include <spare/SpareTypes.hpp>
#include "Image.h"
#include "Daubechies.h"

using boost::numeric::ublas::matrix;
using namespace std;
using namespace spare;

class FeatureExtraction {
public:
	FeatureExtraction();
	virtual ~FeatureExtraction();

	//access
	const IntegerType& getB() const { return B; }
	IntegerType& getB(){ return B; }

	const IntegerType& getM() const { return M; }
	IntegerType& getM(){ return M; }

	matrix<vector<RealType> > extractFeatures(Image mImg);

private:
	IntegerType B, M;
};

#endif /* FEATUREEXTRACTION_H_ */

/*
 * DatasetBuilder.h
 *
 *  Created on: Jul 5, 2013
 *      Author: filippo
 */

#ifndef DATASETBUILDER_H_
#define DATASETBUILDER_H_
#include <string>
#include <dirent.h>
#include "ImageGraphBuilder.h"
#include "WatershedSegmentation.h"
#include "ImageGraph.hpp"
#include "ImageGraphIO.h"
#include "ImageSegmentation.h"

using namespace std;

class DatasetBuilder {
public:
    DatasetBuilder();
    virtual ~DatasetBuilder();

    void build(const string sourcePath, const string destinationPath, bool writeToFile, vector<ImageGraph>& graphs, vector<string>& labels, bool& badSegm);

    ImageSegmentation* wSegm;
};

#endif /* DATASETBUILDER_H_ */

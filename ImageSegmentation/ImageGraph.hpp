#ifndef IMAGEGRAPH_HPP
#define IMAGEGRAPH_HPP

// STD INCLUDES
#include <string>
#include <vector>

// SPARE INCLUDES
#include <spare/SpareTypes.hpp>

// BOOST INCLUDES
#include <boost/graph/adjacency_list.hpp>

// DEFINITIONS
struct v_info_t
{
   typedef boost::vertex_property_tag kind;
};

struct e_info_t
{
   typedef boost::edge_property_tag kind;
};



/**
 * vertex label structure
 */
class ImageVertexLabel
{
public:
    spare::RealType Xc;
    spare::RealType Yc;
    std::vector<spare::RealType> CrCb;
    spare::RealType Sat;
    spare::RealType Brg;
    std::vector<spare::RealType> Wlet;
    spare::RealType Area;
    spare::RealType Symm;
    spare::RealType Rnd;
    spare::RealType Cmp;
    spare::ComplexType Dir;
    spare::ComplexType Or;
};

class ImageEdgeLabel
{
public:
    spare::RealType dXc;
    spare::RealType dYc;
    spare::RealType dB;
};

// vertices and edges labels types
typedef ImageVertexLabel VertexLabelType;
typedef ImageEdgeLabel EdgeLabelType;

// These two typedefs set the types of vertex and edge labels
typedef boost::property<v_info_t, VertexLabelType > VertexInfoProp;
typedef boost::property<e_info_t, EdgeLabelType > EdgeInfoProp;

// IAM Letter graph type
typedef boost::adjacency_list<
                              boost::vecS,
                              boost::vecS,
                              boost::undirectedS,
                              VertexInfoProp,
                              EdgeInfoProp> ImageGraph;

typedef boost::graph_traits<ImageGraph>::vertex_descriptor vDesc;
typedef boost::graph_traits<ImageGraph>::vertex_iterator vIterator;
typedef boost::graph_traits<ImageGraph>::edge_descriptor eDesc;
typedef boost::graph_traits<ImageGraph>::edge_iterator eIterator;

#endif

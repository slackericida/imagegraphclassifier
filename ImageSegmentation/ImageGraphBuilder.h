/*
 * ImageGraphBuilder.h
 *
 *  Created on: Jun 26, 2013
 *      Author: filippo
 */

#ifndef IMAGEGRAPHBUILDER_H_
#define IMAGEGRAPHBUILDER_H_
#include <list>
#include<boost/math/constants/constants.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <spare/Dissimilarity/ModuleDistance.hpp>
#include <spare/Dissimilarity/Euclidean.hpp>
#include <armadillo>
#include "ImageGraph.hpp"
#include "TypeDefinitions.hpp"

using std::list;
using namespace arma;

class ImageGraphBuilder {
public:
    ImageGraphBuilder(IntegerType W, IntegerType H);
    virtual ~ImageGraphBuilder();

    ImageGraph buildGraph(list<RegionType> regionList);

private:
    IntegerType W, H;

    void addVertexFromRegion(ImageGraph& g, RegionType& region);
    RealType computeSaturation(const vector<RealType>& pixelSignature);
    NaturalType computeSymmetricPixels(const RegionType& region, RealType centroidX, RealType centroidY);
    bool findPixel(const RegionType& region, RealType symmX, RealType symmY);
    RealType computeMaxDistance(const RegionType& region, RealType Xc, RealType Yc);
    NaturalType countPixelsInsideCircle(const RegionType& region, RealType centroidX, RealType centroidY, RealType equivalentRadius);
    RealType getEuclideanDistance(pair<RealType, RealType> p1, pair<RealType, RealType>  p2, bool normalized);
    void computeRegionShape(const RegionType& region, RealType centroidX, RealType centroidY, ComplexType& dir, ComplexType& orient) ;
    void addEdgeLabel(eDesc edgeDesc, ImageGraph& g, const RegionType& Region1, const RegionType& Region2,vDesc vd1, vDesc vd2);
};

#endif /* IMAGEGRAPHBUILDER_H_ */

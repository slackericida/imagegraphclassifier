/*
 * DatasetBuilder.cpp
 *
 *  Created on: Jul 5, 2013
 *      Author: filippo
 */

#include "DatasetBuilder.h"
#include <string>
#include <cstdlib>

DatasetBuilder::DatasetBuilder() {
    // TODO Auto-generated constructor stub
    wSegm = new WatershedSegmentation();
}

DatasetBuilder::~DatasetBuilder() {
    // TODO Auto-generated destructor stub
}

void DatasetBuilder::build(const string sourcePath, const string destinationPath, bool writeToFile, vector<ImageGraph>& graphs, vector<string>& labels, bool& badSegm){

    DIR *dir;
    struct dirent *ent;

    NaturalType ID = 0;
    ImageGraphIO imgIO;

    NaturalType counter = 0;

    if((dir = opendir(sourcePath.c_str())) != NULL){

        while((ent = readdir(dir)) != NULL){

            counter++;
            string filePath = ent->d_name;

            if(filePath != "." && filePath != ".."){
                NaturalType l = filePath.length();
                string fileClass = filePath.substr(l-6,2);

                //cout << "Processing image " << filePath << "..." << endl;
                Image img((sourcePath + filePath).c_str());
                //cout << "Segmenting... " << endl;
                size_t fit_s = clock();
                list<RegionType> segments = wSegm->segmentImage(img);
                size_t fit_e = clock();

                cout << "\t\t IMAGE " << counter << ": " << segments.size()<< " segments generated (" << (double)((fit_e-fit_s)/CLOCKS_PER_SEC)<<" sec)" <<endl;

                if(segments.size() > 10  || segments.size() == 0){
                    badSegm = true;
                    closedir(dir);
                    return;
                }else{
                    //cout << "Building graph... " << endl;
                    ImageGraphBuilder gBuilder(img.getWidth(), img.getHeight());
                    fit_s = clock();
                    ImageGraph imgGraph = gBuilder.buildGraph(segments);
                    fit_e = clock();
                    cout << "\t\t Done building graphs in " << (double)((fit_e-fit_s)/CLOCKS_PER_SEC)<<" sec" <<endl;
                    graphs.push_back(imgGraph);
                    labels.push_back(fileClass);
                    if(writeToFile)
                        imgIO.writeToFile(imgGraph, destinationPath, ID++, fileClass);
                }

            }

        }
        closedir(dir);

    }
    else {
        cerr << "Path " << sourcePath << " not found!" << endl;
    }

}


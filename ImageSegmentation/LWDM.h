/*
 * LWDM.h
 *
 *  Created on: Jun 17, 2013
 *      Author: filippo
 */

#ifndef LWDM_H_
#define LWDM_H_

#include "TypeDefinitions.hpp"
using namespace spare;
using namespace std;

class LWDM {
public:
    LWDM();
    virtual ~LWDM();

    const RealType& getWbrg() const { return Wbrg; }
    RealType& getWbrg(){ return Wbrg; }

    const RealType& getWbin() const { return Wbin; }
    RealType& getWbin(){ return Wbin; }

    RealType Diss(const vector<RealType> s1, const vector<RealType> s2) const;

private:
    RealType Wbrg, Wbin;
};

#endif /* LWDM_H_ */

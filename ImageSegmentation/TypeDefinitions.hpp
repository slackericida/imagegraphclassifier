/*
 * TypeDefinitions.hpp
 *
 *  Created on: Jun 18, 2013
 *      Author: filippo
 */

#ifndef TYPEDEFINITIONS_HPP_
#define TYPEDEFINITIONS_HPP_
#include <spare/Representative/MinSod.hpp>
#include <spare/Clustering/Bsas.hpp>
#include <utility>
#include <vector>
#include <spare/SpareTypes.hpp>
#include <spare/Dissimilarity/Euclidean.hpp>
#include <list>
#include "LWDM.h"

using spare::MinSod;
using spare::Bsas;
using std::pair;
using std::vector;
using spare::RealType;
using spare::IntegerType;
using spare::NaturalType;
using std::list;

    typedef pair<vector<RealType>, pair<IntegerType, IntegerType> > PixelType;
    typedef list<PixelType> RegionType;
    //typedef pair<vector<RealType>, NaturalType> CentroidType;

    struct CentroidType{

        vector<RealType> signature;
        NaturalType ID;
        pair<RealType, RealType> position;

    };


#endif /* TYPEDEFINITIONS_HPP_ */

/*
 * GraphBasedSegmentation.h
 *
 *  Created on: Jul 11, 2013
 *      Author: scardax
 */

#ifndef GRAPHBASEDSEGMENTATION_H_
#define GRAPHBASEDSEGMENTATION_H_

#include "ImageSegmentation.h"
#include "graphsegmentation/pnmfile.h"
#include "graphsegmentation/segment-image.h"

using namespace gsegmentation;

class GraphBasedSegmentation: public ImageSegmentation {

public:
	GraphBasedSegmentation();

	virtual list<RegionType> segmentImage(Image mImg);

	//access
	const float& getSigma() const { return Sigma; }
	float& getSigma(){ return Sigma; }

	const float& getK() const { return K; }
	float& getK(){ return K; }

    const bool& getRemoveBackground() const { return RemoveBackground; }
    bool& getRemoveBackground() { return RemoveBackground; }

	const NaturalType& getMinSize() const { return MinSize; }
	NaturalType& getMinSize() { return MinSize; }

private:
	float Sigma;
	float K;
	NaturalType MinSize;
	bool RemoveBackground;

};

#endif /* GRAPHBASEDSEGMENTATION_H_ */

/*
 * GraphBasedSegmentation.cpp
 *
 *  Created on: Jul 11, 2013
 *      Author: scardax
 */

#include "GraphBasedSegmentation.h"

GraphBasedSegmentation::GraphBasedSegmentation():Sigma(1), K(500), MinSize(30),RemoveBackground(0){}

list<RegionType> GraphBasedSegmentation::segmentImage(Image mImg){
	list<RegionType> regionList;

	matrix<vector<RealType> > features = fExtract.extractFeatures(mImg);
	//image<rgb> *input = loadPPM(mImg.getImageURI().c_str());
	image<rgb> *input = new image<rgb>(mImg.getWidth(), mImg.getHeight(), true);
	for(IntegerType i = 0; i < mImg.getHeight(); i++){
	    for(IntegerType j = 0; j < mImg.getWidth(); j++){
	        (imPtr(input, j,i))->r = mImg.getRed(i,j);
	        (imPtr(input, j,i))->g = mImg.getBlue(i,j);
	        (imPtr(input, j,i))->b = mImg.getBlue(i,j);
	    }
	}
	const char* saveFile = "data/last_segmentation.ppm";
	int num_ccs;
	image<rgb> *seg = segment_image(input, Sigma, K, MinSize, &num_ccs, features, regionList);

	if(RemoveBackground){
        list<RegionType>::iterator compIt = regionList.begin();
        while(compIt != regionList.end()){
            RegionType currentRegion = *compIt;
            list<PixelType>::iterator pixIt = currentRegion.begin();
            bool erased = false;
            while(pixIt != currentRegion.end() && !erased){
                PixelType currentPixel = *pixIt++;
                IntegerType x = currentPixel.second.first, y = currentPixel.second.second;
                if((x == 0 && y == 0) || (x == 0 && y == mImg.getWidth()-1) ||
                        (x == mImg.getHeight()-1 && y == 0) || (x == mImg.getHeight()-1 && y == mImg.getWidth()-1)){
                    erased = true;
                }
            }
            if(erased)
                compIt = regionList.erase(compIt);
            else
                compIt++;
        }
	}

	savePPM(seg, saveFile);
	return regionList;
}

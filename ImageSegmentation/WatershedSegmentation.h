/*
 * WatershedSegmentation.h
 *
 *  Created on: Jun 11, 2013
 *      Author: scardax
 */

#ifndef WATERSHEDSEGMENTATION_H_
#define WATERSHEDSEGMENTATION_H_

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <algorithm>
#include "ImageSegmentation.h"
#include "Daubechies.h"
#include "ELWDM.h"
#include "LWDM.h"

using namespace spare;
using boost::numeric::ublas::matrix;
using namespace std;

class WatershedSegmentation : public ImageSegmentation{
public:

    //constructor
	WatershedSegmentation();

	//typedef
    typedef MinSod<CentroidType,ELWDM> RepresentativeType;
    typedef Bsas<RepresentativeType> BsasType;

    const RealType& getWbrg() const { return Wbrg; }
    RealType& getWbrg() { return Wbrg; }

    const RealType& getWbin() const { return Wbin; }
    RealType& getWbin() { return Wbin; }

    const RealType& getTbin() const { return Tbin; }
    RealType& getTbin() { return Tbin; }

    const RealType& getTfus() const { return Tfus; }
    RealType& getTfus() { return Tfus; }

    const RealType& getTreg() const { return Treg; }
    RealType& getTreg() { return Treg; }

    const RealType& getAlpha() const { return alpha; }
    RealType& getAlpha() { return alpha; }

    const bool& getRemoveBackground() const { return RemoveBackground; }
    bool& getRemoveBackground() { return RemoveBackground; }

    const bool& getDebugImages() const { return DebugImages; }
    bool& getDebugImages() { return DebugImages; }

    const bool& getDebugImagesFinal() const { return DebugImagesFinal; }
        bool& getDebugImagesFinal() { return DebugImagesFinal; }

    //member function
    virtual list<RegionType> segmentImage(Image mImg);

private:

	matrix<vector<RealType> > extractFeatures(Image mImg);

	matrix<RealType> getLmatrix(const matrix<vector<RealType> >& featuresMatrix);

	matrix<bool> thresholdMatrix(const matrix<RealType>& matrixL);

	matrix<NaturalType> getConnectedRegions(const matrix<bool>& lbin, const matrix<vector<RealType> >& features,
	        list<CentroidType>& centroids, list<RegionType>& regionList);

	void setID(matrix<NaturalType>& CRegions, const matrix<bool>& lbin, IntegerType i, IntegerType j,
	        IntegerType H, IntegerType W, NaturalType ID, RegionType& region_k, CentroidType& centroid_k,
	        const matrix<vector<RealType> >& features);

	void absorption(matrix<NaturalType>& CRegions, list<CentroidType>& centroids,
	        const matrix<vector<RealType> >& features, list<RegionType>& regionList);

	void centroidAssociation(list<RegionType>& outputSegments, const vector<RepresentativeType> & representatives,
	        const list<RegionType>& regions);



	RealType Wbrg, Wbin;

	RealType Tbin;

	RealType Tfus;

	RealType alpha;

	RealType Treg;

	bool RemoveBackground;

	bool DebugImages, DebugImagesFinal;
};

#endif /* WATERSHEDSEGMENTATION_H_ */

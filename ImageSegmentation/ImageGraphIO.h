/*
 * ImageGraphIO.h
 *
 *  Created on: Jun 28, 2013
 *      Author: filippo
 */

#ifndef IMAGEGRAPHIO_H_
#define IMAGEGRAPHIO_H_
#include <string>
#include <spare/SpareTypes.hpp>
#include "ImageGraph.hpp"
#include <fstream>
#include <sstream>
#include <iomanip>

using namespace std;
using namespace spare;

class ImageGraphIO {
public:
    ImageGraphIO();
    virtual ~ImageGraphIO();

    void writeToFile(ImageGraph h, string fileURI, NaturalType id, string label);
    ImageGraph readFromFile(string fileURI);

private:
    string doubleToStr(RealType d);
};

#endif /* IMAGEGRAPHIO_H_ */

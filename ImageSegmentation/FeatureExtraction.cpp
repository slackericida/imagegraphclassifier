/*
 * FeatureExtraction.cpp
 *
 *  Created on: Jul 11, 2013
 *      Author: scardax
 */

#include "FeatureExtraction.h"

FeatureExtraction::FeatureExtraction() {
	// TODO Auto-generated constructor stub

}

FeatureExtraction::~FeatureExtraction() {
	// TODO Auto-generated destructor stub
}

matrix<vector<RealType> > FeatureExtraction::extractFeatures(Image mImg){

    IntegerType h = (IntegerType)mImg.getHeight()/M;
    IntegerType w = (IntegerType)mImg.getWidth()/M;

    //cout<<"w: "<<w<<", h: "<<h<<endl;

	matrix<vector<RealType> > outputFeatures(h,w);

    // Extract YCbCr components
	for(IntegerType i=0; i<h; i++){
		for(IntegerType j=0; j<w; j++){

			RealType red = mImg.getRed(i*M,j*M)/255;
			RealType green = mImg.getGreen(i*M,j*M)/255;
			RealType blue = mImg.getBlue(i*M,j*M)/255;

			RealType Y = 0.299*red + 0.587*green + 0.114*blue;
			RealType Cr = 0.5*red + -0.419*green + -0.081*blue +0.5;
			RealType Cb = -0.169*red + -0.331*green + 0.5*blue +0.5;

			outputFeatures(i,j).push_back(Y);
			outputFeatures(i,j).push_back(Cr);
			outputFeatures(i,j).push_back(Cb);
		}
	}


	//Extract wavelet components
    matrix<RealType> dTransform(B+1, B+1);

	for(IntegerType i=0; i<h; i++){
        for(IntegerType j=0; j<w; j++){

            //compute the BxB brightness matrix centered in pixel (i,j)
            NaturalType ii = 0;
            for (IntegerType x = i - (B/2); x < i + 1 + (B / 2); x++) {
                NaturalType jj = 0;
                for (IntegerType y = j - (B / 2); y < j + 1 + (B/2); y++) {
                    if (x >= 0 && x < h && y >= 0 && y < w )
                        dTransform(ii,jj) = outputFeatures(x, y)[0];
                    else
                        dTransform(ii,jj) = 0;
                    jj++;
                }
                ii++;
            }

            //transform the brightness matrix
            Daubechies daubTransform;
            daubTransform.bidiDaubTrans(dTransform, B+1);

            //evaluate the wavelet components of pixel (i,j) using the transformed matrix
            RealType wtx1=0, wtx2=0, wtx3=0;
            for (IntegerType x = 0; x < B/2; x++){
                for(IntegerType y = (B/2)+1; y<B+1; y++){
                    wtx1 += pow((RealType)dTransform(x,y),2);
                }
            }

            for (IntegerType x = (B/2)+1; x < B+1; x++) {
                for (IntegerType y = 0; y < B/2; y++) {
                        wtx2 += pow(dTransform(x, y), 2);
                }
            }

            for (IntegerType x = (B/2)+1; x < B+1; x++){
                for (IntegerType y = (B/2)+1; y < B+1; y++) {
                        wtx3 += pow(dTransform(x, y), 2);
                }
            }

            wtx1 = pow(4*wtx1/(B*B), 0.25);
            wtx2 = pow(4*wtx2/(B*B), 0.25);
            wtx3 = pow(4*wtx3/(B*B), 0.25);

            outputFeatures(i,j).push_back(wtx1);
            outputFeatures(i,j).push_back(wtx2);
            outputFeatures(i,j).push_back(wtx3);

            //cout<<"pixel("<<i<<","<<j<<") - y: "<<outputFeatures(i,j)[0]<<" - CrCb: "<<outputFeatures(i,j)[0]<<","<<outputFeatures(i,j)[2]<<" - wtx = [" << wtx1 << "," << wtx2 << "," << wtx3<<"]" << endl;
        }
    }

	return outputFeatures;
}

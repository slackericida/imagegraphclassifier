#ifndef DaubechiesH
#define DaubechiesH

#include <spare/SpareTypes.hpp>
#include <vector>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>

using namespace std;
using namespace spare;
using boost::numeric::ublas::matrix;

class Daubechies {
   private:
   /** coefficienti della scaling function */
   RealType h0, h1, h2, h3;
   /** coefficienti della wavelet function */
   RealType g0, g1, g2, g3;

   RealType Ih0, Ih1, Ih2, Ih3;
   RealType Ig0, Ig1, Ig2, Ig3;
   /**
     Trasformata Daubechies D4
    */
   void transform( vector<RealType>& a, const NaturalType n );
   /**
     Trasformata inversa Daubechies D4
    */
   //void invTransform( vector<RealType>& a, const NaturalType n );

   public:

   Daubechies();

   void daubTrans( vector<RealType>& ts, NaturalType N );

   //void invDaubTrans( vector<RealType>& coef, NaturalType N );

   void bidiDaubTrans(matrix<RealType>& bright, NaturalType B );
};
#endif


/*
 * ImageGraphEdgeDiss.hpp
 *
 *  Created on: Jun 26, 2013
 *      Author: filippo
 */

#ifndef IMAGEGRAPHEDGEDISS_HPP_
#define IMAGEGRAPHEDGEDISS_HPP_
#include "ImageGraph.hpp"
#include <spare/SpareTypes.hpp>
#include <spare/Dissimilarity/ModuleDistance.hpp>

class ImageGraphEdgeDiss{

private:

    spare::RealType wdXc, wdYc, wdB;

public:

    spare::RealType& WdXc(){
        return wdXc;
    }

    const spare::RealType& WdXc() const{
        return wdXc;
    }

    spare::RealType& WdYc(){
        return wdYc;
    }

    const spare::RealType& WdYc() const{
        return wdYc;
    }

    spare::RealType& WdB(){
        return wdB;
    }

    const spare::RealType& WdB() const{
        return wdB;
    }

    spare::RealType Diss(const EdgeLabelType& E1, const EdgeLabelType& E2) const{

        spare::ModuleDistance mDiss;
        spare::RealType D = 0;

        D += (1.0/3.0) * wdXc * mDiss.Diss(E1.dXc, E2.dXc);

        D += (1.0/3.0) * wdYc * mDiss.Diss(E1.dYc, E2.dYc);

        D += (1.0/3.0) * wdB  * mDiss.Diss(E1.dB, E2.dB);

        return D;
    }

};




#endif /* IMAGEGRAPHEDGEDISS_HPP_ */

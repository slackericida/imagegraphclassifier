/*
 * ImageGraphVertexDiss.hpp
 *
 *  Created on: Jun 26, 2013
 *      Author: filippo
 */

#ifndef IMAGEGRAPHVERTEXDISS_HPP_
#define IMAGEGRAPHVERTEXDISS_HPP_
#include "ImageGraph.hpp"
#include <spare/Dissimilarity/ModuleDistance.hpp>
#include <spare/Dissimilarity/Euclidean.hpp>
#include <spare/SpareTypes.hpp>
#include <vector>


class ImageGraphVertexDissimilarity{

private:

    spare::RealType wXc,wYc,wCrCb,wSat,wBrg,wWlet,wArea,wSymm,wRnd,wCmp,wDir,wOr;

public:

    spare::RealType& WXc(){
        return wXc;
    }

    const spare::RealType& WXc() const{
        return wXc;
    }

    spare::RealType& WYc(){
        return wYc;
    }

    const spare::RealType& WYc() const{
        return wYc;
    }

    spare::RealType& WCrCb(){
        return wCrCb;
    }

    const spare::RealType& WCrCb() const{
        return wCrCb;
    }

    spare::RealType& WSat(){
        return wSat;
    }

    const spare::RealType& WSat() const{
        return wSat;
    }

    spare::RealType& WBrg(){
        return wBrg;
    }

    const spare::RealType& WBrg() const{
        return wBrg;
    }

    spare::RealType& WWlet(){
        return wWlet;
    }

    const spare::RealType& WWlet() const{
        return wWlet;
    }

    spare::RealType& WArea(){
        return wArea;
    }

    const spare::RealType& WArea() const{
        return wArea;
    }

    spare::RealType& WSymm(){
        return wSymm;
    }

    const spare::RealType& WSymm() const{
        return wSymm;
    }

    spare::RealType& WRnd(){
        return wRnd;
    }

    const spare::RealType& WRnd() const{
        return wRnd;
    }

    spare::RealType& WCmp(){
        return wCmp;
    }

    const spare::RealType& WCmp() const{
        return wCmp;
    }

    spare::RealType& WDir(){
        return wDir;
    }

    const spare::RealType& WDir() const{
        return wDir;
    }

    spare::RealType& WOr(){
        return wOr;
    }

    const spare::RealType& WOr() const{
        return wOr;
    }

    spare::RealType Diss(const VertexLabelType& V1, const VertexLabelType& V2) const{

        spare::ModuleDistance mDiss;
        spare::Euclidean eDiss;

        spare::RealType D = 0;

        D += (1.0/12.0) * wXc * mDiss.Diss(V1.Xc, V2.Xc);

        D += (1.0/12.0) * wYc * mDiss.Diss(V1.Yc, V2.Yc);

        std::vector<spare::RealType> weightsCrCb(2, 1.0/2.0);
        eDiss.WeightSetup(weightsCrCb);
        D += (1.0/12.0) * wCrCb * eDiss.Diss(V1.CrCb, V2.CrCb);

        D += (1.0/12.0) * wSat * mDiss.Diss(V1.Sat, V2.Sat);

        D += (1.0/12.0) * wBrg * mDiss.Diss(V1.Brg, V2.Brg);

        std::vector<spare::RealType> weightsWlet(3, 1.0/3.0);
        eDiss.WeightSetup(weightsWlet);
        D += (1.0/12.0) * wWlet * eDiss.Diss(V1.Wlet, V2.Wlet);

        D += (1.0/12.0) * wArea * mDiss.Diss(V1.Area, V2.Area);

        D += (1.0/12.0) * wSymm * mDiss.Diss(V1.Symm, V2.Symm);

        D += (1.0/12.0) * wRnd * mDiss.Diss(V1.Rnd, V2.Rnd);

        D += (1.0/12.0) * wCmp * mDiss.Diss(V1.Cmp, V2.Cmp);

        std::vector<spare::RealType> weightsDir(2, 1.0/2.0);
        eDiss.WeightSetup(weightsDir);
        std::vector<spare::RealType> direction1, direction2;
        std::vector<spare::RealType> orientation1, orientation2;

        direction1.push_back(V1.Dir.real());
        direction1.push_back(V1.Dir.imag());
        direction2.push_back(V2.Dir.real());
        direction2.push_back(V2.Dir.imag());

        D += (1.0/12.0) * wDir * eDiss.Diss(direction1, direction2);

        orientation1.push_back(V1.Or.real());
        orientation1.push_back(V1.Or.imag());
        orientation2.push_back(V2.Or.real());
        orientation2.push_back(V2.Or.imag());

        D += (1.0/12.0) * wDir * eDiss.Diss(orientation1, orientation2);

        return D;
    }
};


#endif /* IMAGEGRAPHVERTEXDISS_HPP_ */

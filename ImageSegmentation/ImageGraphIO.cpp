/*
 * ImageGraphIO.cpp
 *
 *  Created on: Jun 28, 2013
 *      Author: filippo
 */

#include "ImageGraphIO.h"
#include <boost/graph/adjacency_list.hpp>
#include "ImageGraph.hpp"

ImageGraphIO::ImageGraphIO() {
    // TODO Auto-generated constructor stub

}

ImageGraphIO::~ImageGraphIO() {
    // TODO Auto-generated destructor stub
}

void ImageGraphIO::writeToFile(ImageGraph h, string fileURI, NaturalType id, string label){
        ofstream stream;
        stringstream ss;
        string fileName;
        string fullPath;
        typename boost::graph_traits<ImageGraph>::vertex_descriptor vDesc, vDescSource, vDescTarget;
        typename boost::graph_traits<ImageGraph>::vertex_iterator vIT, vITend;
        typename boost::graph_traits<ImageGraph>::edge_iterator edgeIT, edgeITend;
        typename boost::graph_traits<ImageGraph>::edge_descriptor eDesc;
        //std::cout << "vertici: "<<boost::num_vertices(sample)<<", archi: "<<boost::num_edges(sample)<<endl;
        //typename boost::property_map<BoostGraphType, VertexInfoProp>::type vlabel=boost::get(v_info_t(), sample);
        //typename boost::property_map<BoostGraphType, EdgeInfoProp>::type elabel=boost::get(e_info_t(), sample);

        ss<<"graph_"<<id<<"_"<<label<<".dot";
        fileName=ss.str();
        ss.str("");
        fullPath=fileURI+fileName;


      stream.open(fullPath.c_str(), std::ios::out);
      if(stream.is_open())
      {
          ss<<"graph graph_"<<id<<"_"<<label<< " { \n";//\t graph [classid="<<label<<"]; \n";

          vIT = boost::vertices(h).first;
          vITend = boost::vertices(h).second;
          while(vIT != vITend){
              vDesc = boost::vertex(*vIT++, h);
              ss <<"\t vertex_"<<vDesc<<" [\n";
              VertexLabelType v = boost::get(v_info_t(), h, vDesc);
              ss << "\t\t Xc=" << doubleToStr(v.Xc) <<",\n";
              ss << "\t\t Yc=" << doubleToStr(v.Yc) <<",\n";
              ss << "\t\t Cr=" << doubleToStr(v.CrCb[0]) << ",\n";
              ss << "\t\t Cb=" << doubleToStr(v.CrCb[1]) << ",\n";
              ss << "\t\t Sat=" << doubleToStr(v.Sat) <<",\n";
              ss << "\t\t Brg=" << doubleToStr(v.Brg) <<",\n";
              ss << "\t\t Wlet0=" << doubleToStr(v.Wlet[0]) << ",\n";
              ss << "\t\t Wlet1=" << doubleToStr(v.Wlet[1]) << ",\n";
              ss << "\t\t Wlet2=" << doubleToStr(v.Wlet[2]) << ",\n";
              ss << "\t\t Area=" << doubleToStr(v.Area) <<",\n";
              ss << "\t\t Symm=" << doubleToStr(v.Symm) <<",\n";
              ss << "\t\t Rnd=" << doubleToStr(v.Rnd) <<",\n";
              ss << "\t\t Cmp=" << doubleToStr(v.Cmp) <<",\n";
              ss << "\t\t DirReal=" << doubleToStr(v.Dir.real()) << ",\n";
              ss << "\t\t DirImag="<< doubleToStr(v.Dir.imag()) << ",\n";
              ss << "\t\t OrReal=" << doubleToStr(v.Or.real()) << ",\n";
              ss << "\t\t OrImag="<< doubleToStr(v.Or.imag()) << "\n \t];\n";

              stream<<ss.str();
              ss.str("");

          }



          edgeIT = boost::edges(h).first;
          edgeITend = boost::edges(h).second;
          while(edgeIT != edgeITend){

              eDesc = *edgeIT++;
              vDescSource = boost::source(eDesc,h);
              vDescTarget = boost::target(eDesc,h);
              EdgeLabelType e = boost::get(e_info_t(), h, eDesc);

              ss <<"\t vertex_"<<vDescSource<<" -- vertex_"<<vDescTarget<<"[\n";
              ss << "\t\t dXc=" << doubleToStr(e.dXc) <<",\n";
              ss << "\t\t dYc=" << doubleToStr(e.dYc) <<",\n";
              ss << "\t\t dB=" << doubleToStr(e.dB) <<"\n \t]\n";

              stream<<ss.str();
              ss.str("");
          }


          stream<<"}";
          stream.close();
      }
      else{
          cout << "Error in opening "<<fullPath<<endl;
      }

}


ImageGraph ImageGraphIO::readFromFile(string fileURI){
    ImageGraph imgGraph;
    //TODO
    return imgGraph;
}

string ImageGraphIO::doubleToStr(RealType d)
{
    stringstream ss;
    ss << fixed << setprecision(5) << d;
    return ss.str();

}

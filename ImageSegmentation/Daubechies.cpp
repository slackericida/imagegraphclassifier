#include "Daubechies.h"

Daubechies :: Daubechies()
   {
      const RealType sqrt_3 = sqrt( 3 );
      const RealType denom = 4 * sqrt( 2 );

      h0 = (1 + sqrt_3)/denom;
      h1 = (3 + sqrt_3)/denom;
      h2 = (3 - sqrt_3)/denom;
      h3 = (1 - sqrt_3)/denom;

      g0 =  h3;
      g1 = -h2;
      g2 =  h1;
      g3 = -h0;

      Ih0 = h2;
      Ih1 = g2;  // h1
      Ih2 = h0;
      Ih3 = g0;  // h3

      Ig0 = h3;
      Ig1 = g3;  // -h0
      Ig2 = h1;
      Ig3 = g1;  // -h2
   }

void Daubechies::transform(vector<RealType>& a, const NaturalType n) {
	if (n >= 4) {
		NaturalType i, j;
		const NaturalType half = n >> 1;

		vector<RealType> tmp(n);

		for (i = 0, j = 0; j < n - 3; j += 2, i++) {
			tmp[i] = a[j] * h0 + a[j + 1] * h1 + a[j + 2] * h2 + a[j + 3] * h3;
			tmp[i + half] = a[j] * g0 + a[j + 1] * g1 + a[j + 2] * g2
					+ a[j + 3] * g3;
		}

		tmp[i] = a[n - 2] * h0 + a[n - 1] * h1 + a[0] * h2 + a[1] * h3;
		tmp[i + half] = a[n - 2] * g0 + a[n - 1] * g1 + a[0] * g2 + a[1] * g3;

		for (i = 0; i < n; i++) {
			a[i] = tmp[i];
		}
	}
}

void Daubechies :: daubTrans( vector<RealType>& ts, NaturalType N )
   {
      NaturalType n;
      for (n = N; n >= 4; n >>= 1) {
         transform( ts, n );
      }
   }

void Daubechies :: bidiDaubTrans(matrix<RealType>& bright, NaturalType B )
   {
	NaturalType i, j;
	vector<RealType> temp(B);
	for (j = 0; j < B; j++) {
		for (i = 0; i < B; i++)
			temp[i] = bright(i, j);
		daubTrans(temp, B);
		for (i = 0; i < B; i++)
			bright(i, j) = temp[i];
	}
	for (i = 0; i < B; i++) {
		for (j = 0; j < B; j++)
			temp[j] = bright(i, j);
		daubTrans(temp, B);
		for (j = 0; j < B; j++)
			bright(i, j) = temp[j];
	}
}

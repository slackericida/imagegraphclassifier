/*
 * LWDM.cpp
 *
 *  Created on: Jun 17, 2013
 *      Author: filippo
 */

#include "LWDM.h"

using spare::RealType;
using std::vector;
using spare::Euclidean;

LWDM::LWDM(){

}

LWDM::~LWDM() {
    // TODO Auto-generated destructor stub
}

RealType LWDM::Diss(const vector<RealType> s1, const vector<RealType> s2) const{

    RealType n0 = (1-Wbrg)*(1-Wbin);
    RealType n1 = Wbrg * (1-Wbin);
    RealType n2 = Wbin;

    Euclidean mDiss;
    RealType dissTot = 0;

    //y component
    RealType Y1 = s1[0];
    RealType Y2 = s2[0];
    dissTot += n1*sqrt(pow(Y1-Y2,2));

    //cbcr components
    vector<RealType> CbCr1, CbCr2;
    CbCr1.push_back(s1[1]);
    CbCr1.push_back(s1[2]);
    CbCr2.push_back(s2[1]);
    CbCr2.push_back(s2[2]);
    vector<RealType> CbCrW(2,1.0/2.0);
    mDiss.WeightSetup(CbCrW);
    dissTot += n0*mDiss.Diss(CbCr1, CbCr2);

    //wtx components
    vector<RealType> wtx1, wtx2;
    wtx1.push_back(s1[3]);
    wtx1.push_back(s1[4]);
    wtx1.push_back(s1[5]);
    wtx2.push_back(s2[3]);
    wtx2.push_back(s2[4]);
    wtx2.push_back(s2[5]);
    vector<RealType> wtxW(3,1.0/3.0);
    mDiss.WeightSetup(wtxW);
    //cout<<"wtx1= ["<<wtx1[0]<<","<<wtx1[1]<<","<<wtx1[2]<<"], wtx2 = ["<<wtx2[0]<<","<<wtx2[1]<<","<<wtx2[2]<<", diss: "<<mDiss.Diss(wtx1, wtx2)<<endl;
    dissTot += n2*mDiss.Diss(wtx1, wtx2);

    //cout<<"d:"<<dissTot<<endl;
    return dissTot;

}

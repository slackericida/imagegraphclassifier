/*
 * ImageGraphBuilder.cpp
 *
 *  Created on: Jun 26, 2013
 *      Author: filippo
 */

#include "ImageGraphBuilder.h"


ImageGraphBuilder::ImageGraphBuilder(IntegerType Wnew, IntegerType Hnew) {
    H = Hnew;
    W = Wnew;


}

ImageGraphBuilder::~ImageGraphBuilder() {
    // TODO Auto-generated destructor stub
}

ImageGraph ImageGraphBuilder::buildGraph(list<RegionType> regionList){

    ImageGraph graph;


    list<RegionType>::iterator regionIT = regionList.begin();

    //add vertices
    while(regionIT != regionList.end()){

        RegionType currentRegion = *regionIT++;
        //cout << "adding region..." << endl;
        addVertexFromRegion(graph, currentRegion);
    }

    //add edges
    vIterator vIT1 = boost::vertices(graph).first;
    list<RegionType>::iterator regionIT1 = regionList.begin();
    while(vIT1 != boost::vertices(graph).second){

        vDesc vd1 = *vIT1++;
        vIterator vIT2 = vIT1;
        RegionType Region1 = *regionIT1++;
        list<RegionType>::iterator regionIT2 = regionIT1;
        while(vIT2 != boost::vertices(graph).second){
            //cout << "adding edge..." << endl;
            vDesc vd2 = *vIT2++;
            RegionType Region2 = *regionIT2++;
            pair<eDesc, bool> edge = boost::add_edge(vd1, vd2, graph);
            if(edge.second == false)
                cerr<<"ERROR: edge already present"<<endl;
            else
                addEdgeLabel(edge.first, graph, Region1, Region2, vd1, vd2);
        }
    }

    if(boost::num_vertices(graph) == 0){
    	vDesc vd = add_vertex(graph);

    	    RealType Xc = 0.0, Yc = 0.0;
    	    RealType Brg = 0.0, Cr = 0.0, Cb = 0.0, Wtx1 = 0.0, Wtx2 = 0.0, Wtx3 = 0.0;
    	    RealType Sat = 0.0;

    	    boost::get(v_info_t(), graph, vd).Xc = Xc;
    	    boost::get(v_info_t(), graph, vd).Yc = Yc;
    	    boost::get(v_info_t(), graph, vd).Brg = Brg;
    	    boost::get(v_info_t(), graph, vd).CrCb.resize(2);
    	    boost::get(v_info_t(), graph, vd).CrCb[0] = Cr;
    	    boost::get(v_info_t(), graph, vd).CrCb[1] = Cb;
    	    boost::get(v_info_t(), graph, vd).Wlet.resize(3);
    	    boost::get(v_info_t(), graph, vd).Wlet[0] = Wtx1;
    	    boost::get(v_info_t(), graph, vd).Wlet[1] = Wtx2;
    	    boost::get(v_info_t(), graph, vd).Wlet[2] = Wtx3;
    	    boost::get(v_info_t(), graph, vd).Sat = Sat;
    	    boost::get(v_info_t(), graph, vd).Area = 0.0;
    	    boost::get(v_info_t(), graph, vd).Symm = 0.0;
    	    boost::get(v_info_t(), graph, vd).Rnd = 0.0;
    	    boost::get(v_info_t(), graph, vd).Cmp = 0.0;
    	    boost::get(v_info_t(), graph, vd).Dir = 0.0;
    	    boost::get(v_info_t(), graph, vd).Or = 0.0;
    }

    //cout <<"generated a graph of "<<boost::num_vertices(graph)<<" vertices and "<<boost::num_edges(graph)<<" edges"<<endl;

    return graph;
}

void ImageGraphBuilder::addVertexFromRegion(ImageGraph& g, RegionType& region){

    vDesc vd = add_vertex(g);

    RealType Xc = 0.0, Yc = 0.0;
    RealType Brg = 0.0, Cr = 0.0, Cb = 0.0, Wtx1 = 0.0, Wtx2 = 0.0, Wtx3 = 0.0;
    RealType Sat = 0.0;
    NaturalType regionSize = region.size();
    list<PixelType>::iterator pixelIT = region.begin();

    while(pixelIT != region.end()){
        PixelType pix = *pixelIT++;

        Xc += (RealType)pix.second.first;
        Yc += (RealType)pix.second.second;

        Brg += (RealType)pix.first[0];
        Cr += (RealType)pix.first[1];
        Cb += (RealType)pix.first[2];
        Wtx1 += (RealType)pix.first[3];
        Wtx2 += (RealType)pix.first[4];
        Wtx3 += (RealType)pix.first[5];
        Sat += (RealType)computeSaturation(pix.first);
    }

    Xc = Xc/(RealType)((H-1)*regionSize);
    Yc = Yc/(RealType)((W-1)*regionSize);

    Brg = Brg/(RealType)(regionSize);
    Cr = Cr/(RealType)(regionSize);
    Cb = Cb/(RealType)(regionSize);
    Wtx1 = Wtx1/(RealType)(regionSize);
    Wtx2 = Wtx2/(RealType)(regionSize);
    Wtx3 = Wtx3/(RealType)(regionSize);
    Sat = Sat/(RealType)(regionSize);

    RealType centroidX = Xc*(H-1);
    RealType centroidY = Yc*(W-1);
    RealType radius = computeMaxDistance(region, centroidX, centroidY);

    ComplexType dir, orient;
    computeRegionShape(region, centroidX, centroidY, dir, orient);

    RealType equivalentRadius = (sqrt(regionSize/boost::math::constants::pi<RealType>()));

    boost::get(v_info_t(), g, vd).Xc = Xc;
    boost::get(v_info_t(), g, vd).Yc = Yc;
    boost::get(v_info_t(), g, vd).Brg = Brg;
    boost::get(v_info_t(), g, vd).CrCb.resize(2);
    boost::get(v_info_t(), g, vd).CrCb[0] = Cr;
    boost::get(v_info_t(), g, vd).CrCb[1] = Cb;
    boost::get(v_info_t(), g, vd).Wlet.resize(3);
    boost::get(v_info_t(), g, vd).Wlet[0] = Wtx1;
    boost::get(v_info_t(), g, vd).Wlet[1] = Wtx2;
    boost::get(v_info_t(), g, vd).Wlet[2] = Wtx3;
    boost::get(v_info_t(), g, vd).Sat = Sat;
    boost::get(v_info_t(), g, vd).Area = regionSize / ((RealType)W*H);
    boost::get(v_info_t(), g, vd).Symm = (RealType)computeSymmetricPixels(region, centroidX, centroidY) / (RealType)regionSize;
    boost::get(v_info_t(), g, vd).Rnd = equivalentRadius/radius;
    boost::get(v_info_t(), g, vd).Cmp = (RealType) countPixelsInsideCircle(region, centroidX, centroidY, equivalentRadius)/regionSize;
    boost::get(v_info_t(), g, vd).Dir = dir;
    boost::get(v_info_t(), g, vd).Or = orient;


}

RealType ImageGraphBuilder::computeSaturation(const vector<RealType>& pixelSignature){
    RealType R = pixelSignature[0] + 1.4017*(pixelSignature[1] - 0.5) - 0.00092674*(pixelSignature[2] - 0.5);
    RealType G = pixelSignature[0] - 0.71417*(pixelSignature[1] - 0.5) - 0.3437*(pixelSignature[2] - 0.5);
    RealType B = pixelSignature[0] + 0.00099022*(pixelSignature[1] - 0.5) + 1.7722*(pixelSignature[2] - 0.5);

    RealType Cmax = std::max(R, std::max(G,B));
    RealType Cmin = std::min(R, std::min(G,B));
    RealType delta = Cmax - Cmin;

    if(delta == 0)
        return 0;
    else
        return delta/Cmax;

}

NaturalType ImageGraphBuilder::computeSymmetricPixels(const RegionType& region, RealType centroidX, RealType centroidY){

	NaturalType symmetricPixelsCount = 0;
	list<PixelType>::const_iterator pixelIT = region.begin();

	    while(pixelIT != region.end()){
	        PixelType pix = *pixelIT++;
	        IntegerType symmX, symmY;

	        RealType deltaX = abs(centroidX - (RealType)pix.second.first);
	        RealType deltaY = abs(centroidY - (RealType)pix.second.second);
	        if(pix.second.first >= centroidX)
	        	symmX = centroidX - deltaX;
	        else
	        	symmX = centroidX + deltaX;

	        if(pix.second.second >= centroidY)
	        	symmY = centroidY - deltaY;
	        else
	        	symmY = centroidY + deltaY;

	        if(findPixel(region, symmX, symmY))
	        	symmetricPixelsCount++;
	    }

	    return symmetricPixelsCount;

}

bool ImageGraphBuilder::findPixel(const RegionType& region, RealType symmX, RealType symmY){
	bool found = false;
	list<PixelType>::const_iterator pixelIT = region.begin();

	IntegerType symmXround = round(symmX);
	IntegerType symmYround = round(symmY);

	while(pixelIT != region.end() && !found){
		PixelType pix = *pixelIT++;
		if(pix.second.first == symmXround && pix.second.second == symmYround)
			found = true;
	}

	return found;
}

RealType ImageGraphBuilder::computeMaxDistance(const RegionType& region, RealType Xc, RealType Yc){
	RealType maxDistance = 0;
	list<PixelType>::const_iterator pixelIT = region.begin();

	while (pixelIT != region.end()) {
		PixelType pix = *pixelIT++;

		RealType dist = getEuclideanDistance(pix.second, make_pair(Xc, Yc), false);

		if (dist > maxDistance)
			maxDistance = dist;
	}

	return maxDistance;
}

RealType ImageGraphBuilder::getEuclideanDistance(pair<RealType, RealType> p1, pair<RealType, RealType> p2, bool normalized){
    Euclidean eDiss;
    if(normalized){
        vector<RealType> weights;
        weights.push_back(1.0/(2.0*pow((RealType)H,2)));
        weights.push_back(1.0/(2.0*pow((RealType)W,2)));
        eDiss.WeightSetup(weights);
    }
    vector<RealType> p1vect, p2vect;
    p1vect.push_back(p1.first);
    p1vect.push_back(p1.second);
    p2vect.push_back(p2.first);
    p2vect.push_back(p2.second);
    return eDiss.Diss(p1vect, p2vect);

}



NaturalType ImageGraphBuilder::countPixelsInsideCircle(const RegionType& region, RealType centroidX, RealType centroidY, RealType equivalentRadius){
	NaturalType pixelsInside = 0;
	list<PixelType>::const_iterator pixelIT = region.begin();

	while (pixelIT != region.end()) {
		PixelType pix = *pixelIT++;
		RealType dist = getEuclideanDistance(pix.second, make_pair(centroidX, centroidY), false);
		if (dist <= equivalentRadius)
			pixelsInside++;
	}

	return pixelsInside;
}

void ImageGraphBuilder::computeRegionShape(const RegionType& region, RealType centroidX, RealType centroidY, ComplexType& dir, ComplexType& orient){

	Mat<RealType> regionMatrix(region.size(), 2);
	NaturalType i = 0;

	list<PixelType>::const_iterator pixelIT = region.begin();

		while (pixelIT != region.end()) {
			PixelType pix = *pixelIT++;
			regionMatrix(i, 0) = pix.second.first;
			regionMatrix(i, 1) = pix.second.second;
			i++;
		}

		Mat<RealType> scores;
		Mat<RealType> coeff;
		Col<RealType> latent;
		princomp(coeff,scores, latent, regionMatrix);

		IntegerType maxComponent;
		if(latent(0) >= latent(1))
		    maxComponent = 0;
		else
		    maxComponent = 1;

		Col<RealType> firstPrinComp(2);
		firstPrinComp(0) = coeff(0,maxComponent);
		firstPrinComp(1) = coeff(1,maxComponent);

		if(firstPrinComp(1) < 0)
			firstPrinComp = firstPrinComp*-1;

		RealType angle = acos((firstPrinComp(0))/(norm(firstPrinComp, 2)));
		RealType Dn = angle / boost::math::constants::pi<RealType>();

		Mat<RealType> std = stddev(scores);

		RealType Sdn = pow(1-(std(1)/std(0)), 2);

		dir.real() = Sdn*cos(Dn);
		dir.imag() = Sdn*sin(Dn);

		Col<RealType> px = scores.col(0);
		px = (px - px.min())/(px.max() - px.min());

		RealType mx = accu(px)/region.size() - 0.5;

		if(mx != 0){
			firstPrinComp = abs(mx)*firstPrinComp;

			angle = acos((firstPrinComp(0))/(norm(firstPrinComp, 2)));

			RealType On = angle / (2*boost::math::constants::pi<RealType>());
			RealType Son = abs(mx)*Sdn;

            orient.real() = Son*cos(On);
            orient.imag() = Son*sin(On);
		}
		else{
            orient.real() = 0;
            orient.imag() = 0;
		}

}

void ImageGraphBuilder::addEdgeLabel(eDesc edgeDesc, ImageGraph& g, const RegionType& Region1, const RegionType& Region2,
        vDesc vd1, vDesc vd2){

    ModuleDistance mDiss;

    RealType Xc1 = boost::get(v_info_t(), g, vd1).Xc;
    RealType Xc2 = boost::get(v_info_t(), g, vd2).Xc;
    RealType dXc = mDiss.Diss(Xc1, Xc2);
    boost::get(e_info_t(), g, edgeDesc).dXc = dXc;

    RealType Yc1 = boost::get(v_info_t(), g, vd1).Yc;
    RealType Yc2 = boost::get(v_info_t(), g, vd2).Yc;
    RealType dYc = mDiss.Diss(Yc1, Yc2);
    boost::get(e_info_t(), g, edgeDesc).dYc = dYc;

//    RealType dmin = numeric_limits<RealType>::max();
//    RegionType::const_iterator regionIT1 = Region1.begin();
//    while(regionIT1 != Region1.end()){
//        PixelType pix1 = *regionIT1++;
//        RegionType::const_iterator regionIT2 = Region2.begin();
//        while(regionIT2 != Region2.end()){
//            PixelType pix2 = *regionIT2++;
//            RealType d = getEuclideanDistance(pix1.second, pix2.second, true);
//            //RealType d = 0;
//            if(dmin > d)
//                dmin = d;
//        }
//    }
//    boost::get(e_info_t(), g, edgeDesc).dB = dmin;
    boost::get(e_info_t(), g, edgeDesc).dB = 0.0;


}

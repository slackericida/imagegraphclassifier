/*
 * Main.cpp
 *
 *  Created on: Jun 10, 2013
 *      Author: scardax
 */

#include "Image.h"
#include "WatershedSegmentation.h"
#include <iostream>
#include <boost/graph/graph_utility.hpp>
#include "DatasetBuilder.h"
#include "GraphBasedSegmentation.h"

using namespace std;

int main(int argc, char** argv){


    int ClOpt;
    NaturalType B = 4, M = 1;
    RealType Wbrg = 0.33, Wbin = 0.1, Tbin = 0.3, Tfus = 0.0, alpha = 1.0, Treg = 0, RemoveBackground = 1;
    RealType sigma =1, k = 500;
    NaturalType min_size = 30;
    bool debug = false;
    string SPath="/home/filippo/Desktop/datasets/SPImR1/P23/Training/",
    		DPath="/home/filippo/Desktop/datasets/SPImR1/P23/TR/";
    bool graphSegmenter = false;
    // command line parsing
    while ((ClOpt = getopt(argc, argv, ":1:2:b:m:g:n:t:f:a:s:k:z:r:RdGx")) != -1) {
        switch (ClOpt) {
            case 'b': {
                B = atoi(optarg);
                break;
            }
            case 'm': {
                M = atoi(optarg);
                break;
            }
            case 'g': {
                Wbrg = atof(optarg);
                break;
            }
            case 'n': {
                Wbin = atof(optarg);
                break;
            }
            case 't': {
                Tbin = atof(optarg);
                break;
            }
            case 'f': {
                Tfus = atof(optarg);
                break;
            }
            case 'r': {
                Treg = atof(optarg);
                break;
            }
            case 'a': {
                alpha = atof(optarg);
                break;
            }
            case 'R': {
                RemoveBackground = 1;
                break;
            }
            case 'd': {
                debug = true;
                break;
            }
            case '1': {
                SPath = optarg;
                break;
            }
            case '2': {
                DPath = optarg;
                break;
            }
            case 'G':{
                graphSegmenter = true;
                break;
            }

            case 's': {
                sigma = atof(optarg);
                break;
            }
            case 'k': {
                k = atof(optarg);
                break;
            }
            case 'z':{
                min_size = atoi(optarg);
                break;
            }
        }
    }

    cout<<"B: "<<B<<endl
        <<"M: "<<M<<endl
        <<"Wbrg: "<<Wbrg<<endl
        <<"Wbin: "<<Wbin<<endl
        <<"Tbin: "<<Tbin<<endl
        <<"Tfus: "<<Tfus<<endl
        <<"Treg: "<<Treg<<endl
        <<"alpha: "<<alpha<<endl
        <<"debug: "<<debug<<endl
        <<"RemoveBackground: "<<RemoveBackground<<endl
        <<"Graph segmenter: "<<graphSegmenter<<endl;

    DatasetBuilder dbuild;

    if(graphSegmenter){
        GraphBasedSegmentation* gSegm = new GraphBasedSegmentation();
        gSegm->getK() = k;
        gSegm->getSigma() = sigma;
        gSegm->getMinSize() = min_size;
        gSegm->getRemoveBackground() = RemoveBackground;
        dbuild.wSegm = gSegm;
    }
    else{
        WatershedSegmentation* wSegm = (WatershedSegmentation*) dbuild.wSegm;
        wSegm->getWbin() = Wbin;
        wSegm->getWbrg() = Wbrg;
        wSegm->getTbin() = Tbin;
        wSegm->getTfus() = Tfus;
        wSegm->getTreg() = Treg;
        wSegm->getAlpha() = alpha;
        wSegm->getDebugImages() = debug;
        wSegm->getDebugImagesFinal() = debug;
        wSegm->getRemoveBackground() = RemoveBackground;
    }

	dbuild.wSegm->getB() = B;
	dbuild.wSegm->getM() = M;


	vector<ImageGraph> graphs;
	vector<string> labels;
	bool x;
    dbuild.build(SPath, DPath, true,  graphs, labels,x);

	return 0;

}



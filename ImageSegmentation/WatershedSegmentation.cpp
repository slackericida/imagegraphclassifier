/*
 * WatershedSegmentation.cpp
 *
 *  Created on: Jun 11, 2013
 *      Author: scardax
 */

#include "WatershedSegmentation.h"

WatershedSegmentation::WatershedSegmentation(): Wbrg(0.33), Wbin(0.1), Tbin(0.8),
                                               Tfus(0.1), alpha(0.5), Treg(0), RemoveBackground(1), DebugImages(0), DebugImagesFinal(0){}


list<RegionType> WatershedSegmentation::segmentImage(Image mImg){

    list<RegionType> outputSegments;

	// Extract features from image
	//cout<<"extract features"<<endl;
	matrix<vector<RealType> > features = fExtract.extractFeatures(mImg);

	//compute L matrix
	//cout<<"compute L matrix"<<endl;
	matrix<RealType> distances = this->getLmatrix(features);

	//thresholding
	//cout<<"apply thresholding"<<endl;
	matrix<bool> lbin = this->thresholdMatrix(distances);

	//DEBUG
	if(DebugImages){
        CImgDisplay img_init_disp(mImg.getImg(), "Immagine Iniziale");
        while (!img_init_disp.is_closed() ){
            img_init_disp.wait();
        }
        for (int i = 0; i < mImg.getHeight(); i++) {
            for (int j = 0; j < mImg.getWidth(); j++) {

                if(lbin(i,j)){
                    mImg.getImg()(j,i,0) = 0;
                    if(mImg.getChannelsNumber() > 1){
                        mImg.getImg()(j,i,1) = 0;
                        mImg.getImg()(j,i,2) = 0;
                    }
                }
                else{
                    mImg.getImg()(j,i,0) = 255;
                    if(mImg.getChannelsNumber() > 1){
                        mImg.getImg()(j,i,1) = 255;
                        mImg.getImg()(j,i,2) = 255;
                    }
                }

            }
        }
        CImgDisplay img_bin_disp(mImg.getImg(), "Immagine Binarizzata");
        while (!img_bin_disp.is_closed() ){
            img_bin_disp.wait();
        }
	}

    //compute connected regions
    //cout<<"compute connected regions.. "<<endl;
    list<CentroidType> centroids;
    list<RegionType> regionList;
    matrix<NaturalType> CRegions = this->getConnectedRegions(lbin, features, centroids, regionList);
    //cout<<regionList.size()<<" regions found"<<endl;

    if(regionList.size() == 0)
        return outputSegments;

    //absorb instable regions
    //cout<<"absorb instable regions"<<endl;
    absorption(CRegions, centroids, features, regionList);

    if(regionList.size() == 0)
        return outputSegments;

    //clustering
    if(Tfus > 0){
        //cout<<"clustering"<<endl;
        BsasType bsasAlgo;
        bsasAlgo.Q() = centroids.size();
        bsasAlgo.Theta() = Tfus;
        vector<RealType> weights;
        weights.push_back(1.0 / (2.0*pow((RealType)mImg.getHeight(),2)) );
        weights.push_back(1.0 / (2.0*pow((RealType)mImg.getWidth(),2)) );
        bsasAlgo.RepInit().DissAgent().getEuclideanDiss().WeightSetup(weights);
        bsasAlgo.RepInit().DissAgent().getLWDMDiss().getWbin() = Wbin;
        bsasAlgo.RepInit().DissAgent().getLWDMDiss().getWbin() = Wbrg;
        bsasAlgo.RepInit().DissAgent().getAlpha() = alpha;
        vector<NaturalType> labels;
        labels.resize(centroids.size());
        bsasAlgo.Process(centroids.begin(), centroids.end(), labels.begin());
        vector<RepresentativeType> representatives = bsasAlgo.GetRepresentatives();

        //get final mapping
        //cout<<"get final mapping"<<endl;
        centroidAssociation(outputSegments, representatives, regionList);

        if(DebugImagesFinal){
            //DEBUG
            list<RegionType>::iterator regionIt = outputSegments.begin();
            CImg<float> img_divided(mImg.getWidth(), mImg.getHeight(), 1, 3, 0);
            IntegerType regionNumber = outputSegments.size();
            RealType delta = 1.0/regionNumber;
            IntegerType i = 0;
            while (regionIt != outputSegments.end()) {

                RegionType segm = (*regionIt);

                list<PixelType>::iterator segmIt = segm.begin();

                while (segmIt != segm.end()) {
                    PixelType pix = (*segmIt++);

                    img_divided(pix.second.second, pix.second.first, 0) = 255*delta*i;
                    img_divided(pix.second.second, pix.second.first, 1) = 255*(1-i%2);
                    img_divided(pix.second.second, pix.second.first, 2) = 255*(i%3);
                }

                i++;
                regionIt++;
            }
            CImgDisplay img_divided_disp(img_divided, "clustering");
            while (!img_divided_disp.is_closed() ){
                img_divided_disp.wait();
            }
        }
    } else {
        outputSegments = regionList;
    }

	return outputSegments;

}

matrix<RealType> WatershedSegmentation::getLmatrix(const matrix<vector<RealType> >& featuresMatrix){

    IntegerType H = featuresMatrix.size1();
    IntegerType W = featuresMatrix.size2();

    matrix<RealType> L(H,W);

    LWDM mDiss;
    mDiss.getWbin() = Wbin;
    mDiss.getWbrg() = Wbrg;

    RealType min = numeric_limits<RealType>::max(), max = 0;

    for(IntegerType i = 0; i<H; i++){
        for(IntegerType j = 0; j<W; j++){

            vector<RealType> sij = featuresMatrix(i,j);
            RealType Lij = 0;

            for(IntegerType h = i-1; h <= i+1; h++){
                for(IntegerType k = j-1; k <= j+1; k++){
                    if(h >= 0 && k >= 0 && h < H && k < W && !(h == i && k == j)){
                        vector<RealType> shk = featuresMatrix(h,k);
                        Lij += mDiss.Diss(sij, shk);
                    }
                }
            }

            L(i,j) = ((RealType) 1/8)*Lij;

            if(L(i,j) < min)
                min = L(i,j);
            if(L(i,j) > max)
                max = L(i,j);

        }
    }

    RealType delta = max -min;

    for (IntegerType i = 0; i < H; i++) {
        for (IntegerType j = 0; j < W; j++) {

            L(i, j) = (L(i, j) - min) / delta;

        }
    }

    //cout << "L: "<< L << endl;
    return L;
}


matrix<bool> WatershedSegmentation::thresholdMatrix(const matrix<RealType>& matrixL){

    IntegerType H = matrixL.size1();
    IntegerType W = matrixL.size2();

    matrix<bool> Lbin(H,W);

    for (IntegerType i = 0; i < H; i++) {
        for (IntegerType j = 0; j < W; j++) {

            Lbin(i, j) = (matrixL(i, j) >= Tbin);

        }
    }

    return Lbin;
}

matrix<NaturalType> WatershedSegmentation::getConnectedRegions(const matrix<bool>& lbin,
        const matrix<vector<RealType> >& features, list<CentroidType>& centroids,
        list<RegionType>& regionList){

    IntegerType H = lbin.size1();
    IntegerType W = lbin.size2();

    //initialize the matrix
    matrix<NaturalType> CRegions(H,W,0);

    NaturalType regionID=1;

    for (IntegerType i = 0; i < H; i++) {
        for (IntegerType j = 0; j < W; j++) {

            if (lbin(i, j) == 0) {

                if (CRegions(i, j) == 0) {

                    RegionType region_k;
                    CentroidType centroid_k;
                    centroid_k.signature = vector<RealType>(6,0);
                    centroid_k.ID = regionID;
                    centroid_k.position = make_pair(0,0);
                    setID(CRegions, lbin, i, j, H, W, regionID, region_k, centroid_k, features);
                    regionID++;

                    centroids.push_back(centroid_k);
                    regionList.push_back(region_k);

                }
            }
        }
    }

    //remove smaller regions
    if(Treg > 0){
        list<RegionType>::iterator regionListIT = regionList.begin();
        list<CentroidType>::iterator centroidsIT = centroids.begin();
        while(regionListIT != regionList.end()){
            RegionType r = *regionListIT;
            if(r.size() < H*W*Treg){
                RegionType::iterator regionIT = r.begin();
                while(regionIT != r.end()){
                    PixelType p = *regionIT++;
                    CRegions(p.second.first, p.second.second) = 0;
                }
                regionListIT = regionList.erase(regionListIT);
                centroidsIT = centroids.erase(centroidsIT);
            }
            else{
                regionListIT++;
                centroidsIT++;
            }
        }

    }

    //re-assign ID if some region has been deleted
    if( Treg > 0){
        list<CentroidType>::iterator centroidsIT;
        centroidsIT = centroids.begin();
        list<RegionType>::iterator regionListIT = regionList.begin();
        IntegerType i = 1;

        while(centroidsIT != centroids.end()){
            (*centroidsIT).ID = i;
            RegionType reg = (*regionListIT);

            list<PixelType>::iterator pixelIT = reg.begin();

            while(pixelIT != reg.end()){
                PixelType pix = (*pixelIT++);
                CRegions(pix.second.first, pix.second.second) = i;
            }

            centroidsIT++;
            regionListIT++;
            i++;
        }
    }

    if(DebugImages){
        //-----DEBUG-----
        list<RegionType>::iterator regionIt = regionList.begin();
        CImg<float> img_divided(W, H, 1, 3, 0);
        IntegerType regionNumber = regionList.size();
        RealType delta = 1.0/regionNumber;
        IntegerType i = 0;
        while (regionIt != regionList.end()) {

            RegionType segm = (*regionIt);

            list<PixelType>::iterator segmIt = segm.begin();

            while (segmIt != segm.end()) {
                PixelType pix = (*segmIt++);

                img_divided(pix.second.second, pix.second.first, 0) = 255*delta*i;
                img_divided(pix.second.second, pix.second.first, 1) = 255*(1-i%2);
                img_divided(pix.second.second, pix.second.first, 2) = 255*(i%3);
            }

            i++;
            regionIt++;
        }
        list<CentroidType>::iterator centroidIT = centroids.begin();
        while(centroidIT != centroids.end()){
            CentroidType c = *centroidIT++;
            img_divided(c.position.second, c.position.first, 0) = 255;
            img_divided(c.position.second, c.position.first, 1) = 0;
            img_divided(c.position.second, c.position.first, 2) = 0;
        }
        CImgDisplay img_divided_disp(img_divided, "regioni connesse");
        while (!img_divided_disp.is_closed() ){
            img_divided_disp.wait();
        }
    }

    return CRegions;
}

void WatershedSegmentation::setID(matrix<NaturalType>& CRegions, const matrix<bool>& lbin, IntegerType i, IntegerType j,
        IntegerType H, IntegerType W, NaturalType ID, RegionType& region_k, CentroidType& centroid_k,
        const matrix<vector<RealType> >& features){

    set<pair<IntegerType, IntegerType> > toBeSearched;
    pair<IntegerType, IntegerType> currentPixel;
    IntegerType x, y;
    vector<RealType> pixelFeatures;

    toBeSearched.insert(make_pair(i,j));

    while(!toBeSearched.empty()){
        currentPixel = *(toBeSearched.begin());
        toBeSearched.erase(toBeSearched.begin());

        x = currentPixel.first;
        y = currentPixel.second;

        CRegions(x,y) = ID;

        std::set<pair<IntegerType, IntegerType> >::iterator find1 = toBeSearched.find(make_pair(x+1,y));
        std::set<pair<IntegerType, IntegerType> >::iterator find2 = toBeSearched.find(make_pair(x-1,y));
        std::set<pair<IntegerType, IntegerType> >::iterator find3 = toBeSearched.find(make_pair(x,y+1));
        std::set<pair<IntegerType, IntegerType> >::iterator find4 = toBeSearched.find(make_pair(x,y-1));

        if(find1 == toBeSearched.end() &&  x+1 < H && CRegions(x+1,y) == 0 && lbin(x+1,y) == 0)
            toBeSearched.insert(make_pair(x+1,y));
        if(find2 == toBeSearched.end()&&  x-1 >0 && CRegions(x-1,y) == 0 && lbin(x-1,y) == 0)
            toBeSearched.insert(make_pair(x-1,y));
        if(find3 == toBeSearched.end()&&  y+1 < W  && CRegions(x,y+1) == 0 && lbin(x,y+1) == 0)
            toBeSearched.insert(make_pair(x,y+1));
        if(find4 == toBeSearched.end()&&  y-1 >0 && CRegions(x,y-1) == 0 && lbin(x,y-1) == 0)
            toBeSearched.insert(make_pair(x,y-1));

        pixelFeatures = features(x,y);
        region_k.push_back(make_pair(pixelFeatures, make_pair(x,y)));
        NaturalType regionSize = region_k.size();
        centroid_k.signature[0] = (centroid_k.signature[0] * (regionSize - 1) + pixelFeatures[0]) / (RealType)regionSize;
        centroid_k.signature[1] = (centroid_k.signature[1] * (regionSize - 1) + pixelFeatures[1]) / (RealType)regionSize;
        centroid_k.signature[2] = (centroid_k.signature[2] * (regionSize - 1) + pixelFeatures[2]) / (RealType)regionSize;
        centroid_k.signature[3] = (centroid_k.signature[3] * (regionSize - 1) + pixelFeatures[3]) / (RealType)regionSize;
        centroid_k.signature[4] = (centroid_k.signature[4] * (regionSize - 1) + pixelFeatures[4]) / (RealType)regionSize;
        centroid_k.signature[5] = (centroid_k.signature[5] * (regionSize - 1) + pixelFeatures[5]) / (RealType)regionSize;
        centroid_k.position.first  = (centroid_k.position.first  * ((RealType)regionSize - 1.0) + x)/ (RealType)regionSize;
        centroid_k.position.second = (centroid_k.position.second * ((RealType)regionSize - 1.0) + y)/ (RealType)regionSize;

    }
}

void WatershedSegmentation::absorption(matrix<NaturalType>& CRegions, list<CentroidType>& centroids,
        const matrix<vector<RealType> >& features, list<RegionType>& regionList){

    IntegerType H = CRegions.size1();
    IntegerType W = CRegions.size2();

    Euclidean eDiss;
    vector<RealType> weight;
    weight.push_back(1.0 / (2.0*pow((RealType)H,2)) );
    weight.push_back(1.0 / (2.0*pow((RealType)W,2)) );
    eDiss.WeightSetup(weight);

    LWDM lDiss;
    lDiss.getWbin() = Wbin;
    lDiss.getWbrg() = Wbrg;

    for (IntegerType i = 0; i < H; i++) {
        for (IntegerType j = 0; j < W; j++) {
            if(CRegions(i,j) == 0){

                vector<RealType> feat_ij = features(i,j);
                RealType dmin = numeric_limits<RealType>::max();
                NaturalType regionID = 0;
                list<CentroidType>::iterator itListCentroids = centroids.begin();
                for(NaturalType k = 0; k < centroids.size(); k++){
                    //cout << k << endl;
                    vector<RealType> centroidCoords, featCoords;
                    centroidCoords.push_back((*itListCentroids).position.first);
                    centroidCoords.push_back((*itListCentroids).position.second);
                    featCoords.push_back(i);
                    featCoords.push_back(j);
                    RealType d_e = eDiss.Diss(centroidCoords, featCoords);
                    RealType d_l = lDiss.Diss(feat_ij, (*itListCentroids).signature);
                    RealType d = alpha*d_e + (1-alpha)*d_l;
                    if(d < dmin){
                        dmin = d;
                        regionID = k +1;
                    }
                    itListCentroids++;
                }
                CRegions(i,j) = regionID;

                //insert the instable pixel in the stable region
                list<RegionType>::iterator itListRegions = regionList.begin();
                advance(itListRegions, regionID-1);
                (*itListRegions).push_back(make_pair(feat_ij, make_pair(i,j)));
            }
        }
    }

    if(DebugImages){
        //DEBUG
        list<RegionType>::iterator regionIt = regionList.begin();
        CImg<float> img_divided(W, H, 1, 3, 0);
        IntegerType regionNumber = regionList.size();
        RealType delta = 1.0/regionNumber;
        IntegerType i = 0;
        while (regionIt != regionList.end()) {

            RegionType segm = (*regionIt);

            list<PixelType>::iterator segmIt = segm.begin();

            while (segmIt != segm.end()) {
                PixelType pix = (*segmIt++);

                img_divided(pix.second.second, pix.second.first, 0) = 255*delta*i;
                img_divided(pix.second.second, pix.second.first, 1) = 255*(1-i%2);
                img_divided(pix.second.second, pix.second.first, 2) = 255*(i%3);
            }

            i++;
            regionIt++;
        }
        CImgDisplay img_absorbed_disp(img_divided, "assorbimento zone instabili");
        while (!img_absorbed_disp.is_closed() ){
            img_absorbed_disp.wait();
        }
    }


    //remove background
    if(RemoveBackground){
        list<NaturalType> backGroundIDs;
        NaturalType ID00 = CRegions(0,0);
        NaturalType ID0W = CRegions(0,W-1);
        NaturalType IDH0 = CRegions(H-1,0);
        NaturalType IDHW = CRegions(H-1,W-1);
        if(ID00 != 0)
            backGroundIDs.push_back(ID00);
        if(ID0W != ID00 && ID0W != 0)
            backGroundIDs.push_back(ID0W);
        if(IDH0 != ID00 && IDH0 != ID0W && IDH0 != 0)
            backGroundIDs.push_back(IDH0);
        if(IDHW != ID00 && IDHW != ID0W && IDHW != IDH0 && IDHW != 0)
            backGroundIDs.push_back(IDHW);

        //cout << "regioni: "<< regionList.size() << endl;

        backGroundIDs.sort();
        list<RegionType>::iterator regionListIT;
        list<CentroidType>::iterator centroidsIT;
        list<NaturalType>::iterator backGroundIDsIT = backGroundIDs.end();
        while(backGroundIDsIT != backGroundIDs.begin()){

            backGroundIDsIT--;
            NaturalType ID = *backGroundIDsIT;
            regionListIT = regionList.begin();
            centroidsIT = centroids.begin();
            //cout << "Removing " << ID << " with " << regionList.size() << " elements." << endl;
            advance(regionListIT, ID-1);
            advance(centroidsIT, ID-1);
            regionList.erase(regionListIT);
            centroids.erase(centroidsIT);
        }

        //cout << "regioni: " << regionList.size() << endl;

        centroidsIT = centroids.begin();
        IntegerType i = 1;

        while (centroidsIT != centroids.end()) {
            (*centroidsIT).ID = i;
            centroidsIT++;
            i++;
        }
    }

    if(DebugImages){
        //DEBUG
        list<RegionType>::iterator regionIt = regionList.begin();
        CImg<float> img_divided(W, H, 1, 3, 0);
        IntegerType regionNumber = regionList.size();
        RealType delta = 1.0/regionNumber;
        IntegerType i = 0;
        while (regionIt != regionList.end()) {

            RegionType segm = (*regionIt);

            list<PixelType>::iterator segmIt = segm.begin();

            while (segmIt != segm.end()) {
                PixelType pix = (*segmIt++);

                img_divided(pix.second.second, pix.second.first, 0) = 255*delta*i;
                img_divided(pix.second.second, pix.second.first, 1) = 255*(1-i%2);
                img_divided(pix.second.second, pix.second.first, 2) = 255*(i%3);
            }

            i++;
            regionIt++;
        }
        CImgDisplay img_absorbed_disp(img_divided, "rimozione sfondo");
        while (!img_absorbed_disp.is_closed() ){
            img_absorbed_disp.wait();
        }
    }
}

void WatershedSegmentation::centroidAssociation(list<RegionType>& outputSegments, const vector<RepresentativeType> & representatives,
        const list<RegionType>& regions){

    IntegerType repNumber = representatives.size();

    for (IntegerType i = 0; i < repNumber; i++) {
        RepresentativeType rep = representatives[i];
        vector<CentroidType> repCentroids = rep.GetSamples();

        RegionType tmp;

        IntegerType centroidsNumber = repCentroids.size();
        for(IntegerType j=0; j< centroidsNumber; j++){

            list<RegionType>::const_iterator itList = regions.begin();
            advance(itList, repCentroids[j].ID-1);
            tmp.insert(tmp.end(), (*itList).begin(), (*itList).end());
        }

        outputSegments.push_back(tmp);
    }

}

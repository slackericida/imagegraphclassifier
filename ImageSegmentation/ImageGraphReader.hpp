/*
 * ImageGraphReader.hpp
 *
 *  Created on: Jul 2, 2013
 *      Author: filippo
 */

#ifndef IMAGEGRAPHREADER_HPP_
#define IMAGEGRAPHREADER_HPP_

//STD INCLUDES
#include <string>
#include <vector>
#include <fstream>

// SPARE INCLUDES
#include <spare/SpareTypes.hpp>

// BOOST INCLUDES
#include <boost/config.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_utility.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/graphviz.hpp>
#include <boost/graph/iteration_macros.hpp>
#include <boost/algorithm/string/predicate.hpp>

//LOCAL
#include "ImageGraph.hpp"



///////////////////////
// TMP IMAGE GRAPH DEFINITION
///////////////////////

//vertex label
/**
 * Xc (RealType)            ---> rank
 * Yc (RealType)            ---> priOrImagty
 * Cr (RealType)            ---> color
 * Cb (RealType)            ---> degree
 * Sat (RealType)           ---> attribute
 * Brg (RealType)           ---> index1
 * Wlet0 (RealType)         ---> index2
 * Wlet1 (RealType)         ---> in_degree
 * Wlet2 (RealType)         ---> out_degree
 * Area (RealType)          ---> distance
 * Symm (RealType)          ---> distance2
 * Rnd (RealType)           ---> update
 * Cmp (RealType)           ---> potential
 * DirReal (RealType)          ---> centrality
 * DirImag (RealType)          ---> discover_time
 * OrReal (RealType)           ---> lowpoint
 * OrImag (RealType)           ---> finish_time
 */
typedef boost::property< boost::vertex_name_t, std::string,
        boost::property< boost::vertex_rank_t, std::string,
        boost::property< boost::vertex_priOrImagty_t, std::string,
        boost::property< boost::vertex_color_t, std::string,
        boost::property< boost::vertex_degree_t, std::string,
        boost::property< boost::vertex_attribute_t, std::string,
        boost::property< boost::vertex_index1_t, std::string,
        boost::property< boost::vertex_index2_t, std::string,
        boost::property< boost::vertex_in_degree_t, std::string,
        boost::property< boost::vertex_out_degree_t, std::string,
        boost::property< boost::vertex_distance_t, std::string,
        boost::property< boost::vertex_distance2_t, std::string,
        boost::property< boost::vertex_update_t, std::string,
        boost::property< boost::vertex_potential_t, std::string,
        boost::property< boost::vertex_centrality_t, std::string,
        boost::property< boost::vertex_discover_time_t, std::string,
        boost::property< boost::vertex_lowpoint_t, std::string,
        boost::property< boost::vertex_finish_time_t, std::string
        > > > > > > > > > > > > > > > > > > sgVertexInfoProp;

//edge label
/**
 * dXc (RealType)   ---> weight
 * dYc (RealType)   ---> weight2
 * dB (RealType)    ---> update
 */
typedef boost::property<boost::edge_name_t, std::string,
        boost::property<boost::edge_weight_t, std::string,
        boost::property<boost::edge_weight2_t, std::string,
        boost::property<boost::edge_update_t, std::string
        > > > > sgEdgeInfoProp;

//graph type
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS,
        sgVertexInfoProp, sgEdgeInfoProp> sgGraph;
//iterators for vertices
typedef boost::graph_traits<sgGraph>::vertex_iterator sgVertexIter;
typedef boost::graph_traits<sgGraph>::vertex_descriptor sgVertexDesc;
typedef boost::property_map<sgGraph, sgVertexInfoProp> sgVertexPropertyMap;
//for edges
typedef boost::graph_traits<sgGraph>::edge_iterator sgEdgeIter;
typedef boost::graph_traits<sgGraph>::out_edge_iterator sgOutEdgeIter;
typedef boost::graph_traits<sgGraph>::edge_descriptor sgEdgeDesc;
typedef std::pair<sgOutEdgeIter, sgOutEdgeIter> sgEdgePair;



/** @brief DOT file reader for Image graph.
 */
class ImageGraphReader {

public:

    /**
     * DOT file read operation.
     * @param[in] in The input stream
     * @param[out] g A compatible boost graph defined as a labeled graph
     */
    void read(std::ifstream& in, ImageGraph& g) const;

    /**
     * DOT file read operation. Wrapper method using std::string as input file path.
     * @param[in] in The input path to the file
     * @param[out] g A compatible boost graph defined as a labeled graph
     */
    void read(std::string& in, ImageGraph& g) const;
};


void ImageGraphReader::read(std::ifstream& in, ImageGraph& g) const
{
    // Construct an empty graph and prepare the dynamic_property_maps.
    sgGraph tmpGraph(0);
    boost::dynamic_properties dp;
    std::string nodeid="node_id";

    //vertex properties
    boost::property_map<sgGraph, boost::vertex_name_t>::type name = boost::get(boost::vertex_name, tmpGraph);
    boost::property_map<sgGraph, boost::vertex_rank_t>::type v_Xc = boost::get(boost::vertex_rank, tmpGraph);
    boost::property_map<sgGraph, boost::vertex_priOrImagty_t>::type v_Yc = boost::get(boost::vertex_priOrImagty, tmpGraph);
    boost::property_map<sgGraph, boost::vertex_color_t>::type v_Cr = boost::get(boost::vertex_color, tmpGraph);
    boost::property_map<sgGraph, boost::vertex_degree_t>::type v_Cb = boost::get(boost::vertex_degree, tmpGraph);
    boost::property_map<sgGraph, boost::vertex_attribute_t>::type v_Sat = boost::get(boost::vertex_attribute, tmpGraph);
    boost::property_map<sgGraph, boost::vertex_index1_t>::type v_Brg = boost::get(boost::vertex_index1, tmpGraph);
    boost::property_map<sgGraph, boost::vertex_index2_t>::type v_Wlet0 = boost::get(boost::vertex_index2, tmpGraph);
    boost::property_map<sgGraph, boost::vertex_in_degree_t>::type v_Wlet1 = boost::get(boost::vertex_in_degree, tmpGraph);
    boost::property_map<sgGraph, boost::vertex_out_degree_t>::type v_Wlet2 = boost::get(boost::vertex_out_degree, tmpGraph);
    boost::property_map<sgGraph, boost::vertex_distance_t>::type v_Area = boost::get(boost::vertex_distance, tmpGraph);
    boost::property_map<sgGraph, boost::vertex_distance2_t>::type v_Symm = boost::get(boost::vertex_distance2, tmpGraph);
    boost::property_map<sgGraph, boost::vertex_update_t>::type v_Rnd = boost::get(boost::vertex_update, tmpGraph);
    boost::property_map<sgGraph, boost::vertex_potential_t>::type v_Cmp = boost::get(boost::vertex_potential, tmpGraph);
    boost::property_map<sgGraph, boost::vertex_centrality_t>::type v_DirReal = boost::get(boost::vertex_centrality, tmpGraph);
    boost::property_map<sgGraph, boost::vertex_discover_time_t>::type v_DirImag = boost::get(boost::vertex_discover_time, tmpGraph);
    boost::property_map<sgGraph, boost::vertex_lowpoint_t>::type v_OrReal = boost::get(boost::vertex_lowpoint, tmpGraph);
    boost::property_map<sgGraph, boost::vertex_finish_time_t>::type v_OrImag = boost::get(boost::vertex_finish_time, tmpGraph);
    dp.property(nodeid, name);
    dp.property("Xc", v_Xc);
    dp.property("Yc", v_Yc);
    dp.property("Cr", v_Cr);
    dp.property("Cb", v_Cb);
    dp.property("Sat", v_Sat);
    dp.property("Brg", v_Brg);
    dp.property("Wlet0", v_Wlet0);
    dp.property("Wlet1", v_Wlet1);
    dp.property("Wlet2", v_Wlet2);
    dp.property("Area", v_Area);
    dp.property("Symm", v_Symm);
    dp.property("Rnd", v_Rnd);
    dp.property("Cmp", v_Cmp);
    dp.property("DirReal", v_DirReal);
    dp.property("DirImag", v_DirImag);
    dp.property("OrReal", v_OrReal);
    dp.property("OrImag", v_OrImag);

    //edge props
    boost::property_map<sgGraph, boost::edge_weight_t>::type e_dXc = boost::get(boost::edge_weight, tmpGraph);
    boost::property_map<sgGraph, boost::edge_weight2_t>::type e_dYc = boost::get(boost::edge_weight2, tmpGraph);
    boost::property_map<sgGraph, boost::edge_update_t>::type e_dB = boost::get(boost::edge_update, tmpGraph);
    dp.property("dXc", e_dXc);
    dp.property("dYc", e_dYc);
    dp.property("dB", e_dB);


    //read
    read_graphviz(in, tmpGraph, dp, nodeid);


    //vertices iterators
    sgVertexIter vi=boost::vertices(tmpGraph).first;
    sgVertexIter viEnd=boost::vertices(tmpGraph).second;
    unsigned int vertexCount=0;


    //add vertices...
    while(vi!=viEnd)
    {
        // vertex label...
        std::string Xc=boost::get(boost::vertex_rank_t(), tmpGraph, (unsigned int)*vi);
        std::string Yc=boost::get(boost::vertex_priOrImagty_t(), tmpGraph, (unsigned int)*vi);
        std::string Cr=boost::get(boost::vertex_color_t(), tmpGraph, (unsigned int)*vi);
        std::string Cb=boost::get(boost::vertex_degree_t(), tmpGraph, (unsigned int)*vi);
        std::string Sat=boost::get(boost::vertex_attribute_t(), tmpGraph, (unsigned int)*vi);
        std::string Brg=boost::get(boost::vertex_index1_t(), tmpGraph, (unsigned int)*vi);
        std::string Wlet0=boost::get(boost::vertex_index2_t(), tmpGraph, (unsigned int)*vi);
        std::string Wlet1=boost::get(boost::vertex_in_degree_t(), tmpGraph, (unsigned int)*vi);
        std::string Wlet2=boost::get(boost::vertex_out_degree_t(), tmpGraph, (unsigned int)*vi);
        std::string Area=boost::get(boost::vertex_distance_t(), tmpGraph, (unsigned int)*vi);
        std::string Symm=boost::get(boost::vertex_distance2_t(), tmpGraph, (unsigned int)*vi);
        std::string Rnd=boost::get(boost::vertex_update_t(), tmpGraph, (unsigned int)*vi);
        std::string Cmp=boost::get(boost::vertex_potential_t(), tmpGraph, (unsigned int)*vi);
        std::string DirReal=boost::get(boost::vertex_centrality_t(), tmpGraph, (unsigned int)*vi);
        std::string DirImag=boost::get(boost::vertex_discover_time_t(), tmpGraph, (unsigned int)*vi);
        std::string OrReal=boost::get(boost::vertex_lowpoint_t(), tmpGraph, (unsigned int)*vi);
        std::string OrImag=boost::get(boost::vertex_finish_time_t(), tmpGraph, (unsigned int)*vi);


        //labeled vertex generation
        boost::add_vertex(g);
        sgVertexDesc source=boost::vertex(vertexCount, g);

        ImageVertexLabel vl;
        vl.Xc=atof(Xc.c_str());
        vl.Yc=atof(Yc.c_str());
        std::vector<spare::RealType> vecCrCb;
        vecCrCb.push_back(atof(Cr.c_str()));
        vecCrCb.push_back(atof(Cb.c_str()));
        vl.CrCb=vecCrCb;
        vl.Sat=atof(Sat.c_str());
        vl.Brg=atof(Brg.c_str());
        std::vector<spare::RealType> vecWlet;
        vecWlet.push_back(atof(Wlet0.c_str()));
        vecWlet.push_back(atof(Wlet1.c_str()));
        vecWlet.push_back(atof(Wlet2.c_str()));
        vl.Wlet=vecWlet;
        vl.Area=atof(Area.c_str());
        vl.Symm=atof(Symm.c_str());
        vl.Rnd=atof(Rnd.c_str());
        vl.Cmp=atof(Cmp.c_str());
        vl.Dir.real()=atof(DirReal.c_str());
        vl.Dir.imag()=atof(DirImag.c_str());
        vl.Or.real()=atof(OrReal.c_str());
        vl.Or.imag()=atof(OrImag.c_str());
        boost::get(v_info_t(), g, source)= vl;

        vi++;
        vertexCount++;
    }


    //edges iterators
    sgEdgeIter ei=boost::edges(tmpGraph).first;
    sgEdgeIter eiEnd=boost::edges(tmpGraph).second;
    //unsigned int edgeCount=0;

    //add edges...
    while(ei!=eiEnd)
    {
        sgVertexDesc source=boost::source(*ei, tmpGraph);
        sgVertexDesc ta=boost::target(*ei, tmpGraph);
        sgEdgeDesc edgeDesc=boost::edge(source, ta, tmpGraph).first;

        //edges label...
        std::string dXc=boost::get(boost::edge_weight_t(), tmpGraph, edgeDesc);
        std::string dYc=boost::get(boost::edge_weight2_t(), tmpGraph, edgeDesc);
        std::string dB=boost::get(boost::edge_update_t(), tmpGraph, edgeDesc);

        //edge generation
        ImageEdgeLabel el;
        el.dXc=atof(dXc.c_str());
        el.dYc=atof(dXc.c_str());
        el.dB=atof(dXc.c_str());

        boost::add_edge(source, ta, g);
        edgeDesc=boost::edge(source, ta, g).first;
        boost::get(e_info_t(), g, edgeDesc)= el;

        ei++;
    }

}

void ImageGraphReader::read(std::string& in, ImageGraph& g) const
{
    std::ifstream stream(in.c_str());
    if(stream.good())
    {
        read(stream, g);
        stream.close();
    }
}


#endif /* IMAGEGRAPHREADER_HPP_ */

/*
 * Image.cpp
 *
 *  Created on: Jun 10, 2013
 *      Author: scardax
 */

#include "Image.h"

Image::Image(const char *const fileURI){
    image.load_png(fileURI);
    filePath = fileURI;
}

Image::Image() {
    image(4,4);
    //std::cout<<"pd"<<std::endl;
    for(int i = 0; i <4; i++){
        for(int j = 0; j <4; j++){

            //image(i,j,0,0) = 250;
//            if(i < 2 && j < 2){
//                image(i,j,0,0) = 250;
//                image(i,j,0,1) = 7;
//                image(i,j,0,2) = 7;
//            }
//            else if(i < 2){
//                image(i,j,0,0) = 5;
//                image(i,j,0,1) = 250;
//                image(i,j,0,2) = 5;
//            }
//            else if(j < 2){
//                image(i,j,0,0) = 4;
//                image(i,j,0,1) = 4;
//                image(i,j,0,2) = 250;
//            }
//            else{
//                image(i,j,0,0) = 200;
//                image(i,j,0,1) = 200;
//                image(i,j,0,2) = 200;
//            }
        }
    }

}

double Image::getRed(int x, int y){
	return image(y,x, 0);
}

double Image::getGreen(int x, int y){
	return image(y,x, 1);
}

double Image::getBlue(int x, int y){
	return image(y,x, 2);
}

int Image::getWidth(){
	return image.width();
}

int Image::getHeight(){
	return image.height();
}

int Image::getChannelsNumber(){
	return image.spectrum();
}

Image::~Image() {
	// TODO Auto-generated destructor stub
}


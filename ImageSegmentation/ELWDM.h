/*
 * ELWDM.h
 *
 *  Created on: Jun 25, 2013
 *      Author: filippo
 */

#ifndef ELWDM_H_
#define ELWDM_H_

#include "TypeDefinitions.hpp"
#include "LWDM.h"
#include <spare/Dissimilarity/Euclidean.hpp>
using namespace spare;
using namespace std;

class ELWDM{

private:

    RealType alpha;

    mutable Euclidean eDiss;

    LWDM lDiss;

public:

    ELWDM(){
        alpha = 0.5;
    }

    const RealType& getAlpha() const { return alpha; }
    RealType& getAlpha() { return alpha; }

    const Euclidean& getEuclideanDiss() const { return eDiss; }
    Euclidean& getEuclideanDiss() { return eDiss; }

    const LWDM& getLWDMDiss() const { return lDiss; }
    LWDM& getLWDMDiss() { return lDiss; }

    RealType Diss(const CentroidType c1, const CentroidType c2) const{

        vector<RealType> c1_coord, c2_coord;
        c1_coord.push_back(c1.position.first);
        c1_coord.push_back(c1.position.second);
        c2_coord.push_back(c2.position.first);
        c2_coord.push_back(c2.position.second);

        RealType d = alpha*eDiss.Diss(c1_coord,c2_coord) + (1-alpha)*lDiss.Diss(c1.signature,c2.signature);

        //cout<<"alpha: "<<alpha<<" "<<alpha*eDiss.Diss(c1_coord,c2_coord)<<" "<<(1-alpha)*lDiss.Diss(c1.signature,c2.signature)<<endl;
        return d;
    }
};


#endif /* ELWDM_H_ */

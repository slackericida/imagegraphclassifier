/*
 * Image.h
 *
 *  Created on: Jun 10, 2013
 *      Author: scardax
 */

#ifndef IMAGE_H_
#define IMAGE_H_


#include <string>
#include <CImg.h>


using namespace std;
using namespace cimg_library;

class Image {
public:
	Image(const char *const fileURI);
	Image();
	virtual ~Image();

	double getRed(int x, int y);
	double getGreen(int x, int y);
	double getBlue(int x, int y);

	string getImageURI(){ return filePath; }

	int getWidth();
	int getHeight();
	int getChannelsNumber();

	void Display(string title){
	    CImgDisplay main_disp(image,title.c_str());
	    while (!main_disp.is_closed() ){
	        main_disp.wait();
	    }
	}

	CImg<float>& getImg(){
	    return image;
	}

private:
	CImg<float> image;
	string filePath;
};

#endif /* IMAGE_H_ */

/** @brief File G1FitFun1.hpp, that contains the implementation of fitness evaluating function for strategy 1 of GRALG algorithm.
 * This class implements the fitness evaluating function for the version 1 of GRALG algorithm.
 *
 * @file G1FitFun1.hpp
 * @author Filippo Bianchi
 */

#ifndef MTG1FITFUN1_HPP_
#define MTG1FITFUN1_HPP_

//SPARE
#include <spare/SpareTypes.hpp>

/**
 * @brief This class implements the fitness evaluating function for the version 1 of GRALG algorithm.
 *
 * The code that is optimized represent the parameters needed by the gralg procedure for doing the embedding.
 *
 * <b>Template Parameters</b>
 *  <table class="contents">
 *  <tr>
 *      <td class="indexkey"><b>Name</b></td>
 *      <td class="indexkey"><b>Description</b></td>
 *  </tr>
 *  <tr>
 *  	<td class="indexvalue">GralgType</td>
 *  	<td class="indexvalue"> A class which implements the version 1 of the GRALG algorithm.</td>
 *  </tr>
 *  <tr>
 *  	<td class="indexvalue">SamplesContainer</td>
 *  	<td class="indexvalue">A class which define the type of the container of the structure that must be processed (graphs or subgraphs).</td>
 *  </tr>
 *  <tr>
 *  	<td class="indexvalue">LabelsContainer</td>
 *  	<td class="indexvalue">A class which define the type of the container of labels of the structures.</td>
 *  </tr>
 *  </table>
 */

using namespace spare;

template<class GralgType, class SamplesContainer, class LabelsContainer, class DissimilarityType>
class mtG1FitFun1{

public:

	//access
	RealType& MaxComplexity(){
		return maxComplexity;
	}

	const RealType& MaxComplexity() const{
		return maxComplexity;
	}


	//MEMBER FUNCTION
	/**
	 * Initialization function
	 * @param[in] ExptrSubGr The set of subgraphs of the Training Set used for training the system.
	 * @param[in] trLbl The set of class labels for the subgraphs of Training Set.
	 * @param[in] idtr Container which associate to each subgraph of Training Set the ID of the original graph.
	 * @param[in] ExpvsSubGr The set of subgraphs of the Validation Set used for validating the trained system.
	 * @param[in] vsLbl The set of class labels for the subgraphs of Validation Set.
	 * @param[in] idvs Container which associate to each subgraph of Validation Set the ID of the original graph.
	 * @param[in] originalParamRange Container of natural-typed numbers, representing the range of the genes of genetic code.
	 * @param[in] eps Tolerance value in symbols recognition used during the granulation.
	 * @param[in] K Number of neighbours of k-NN classification algorithm.
	 * @param[in] res Resolution of the search interval for the genetic code.
	 */
	void DataSetup( const SamplesContainer& ExptrSubGr,
					const LabelsContainer& trLbl,
					const vector<NaturalType>& idtr,
					const LabelsContainer& ExptrLbl,
					const SamplesContainer& ExpvsSubGr,
					const LabelsContainer& vsLbl,
					const vector<NaturalType>& idvs,
					const LabelsContainer& Lbl,
					const vector<RealType> originalParamRange,
					const RealType eps,
					const NaturalType k,
					const NaturalType res,
					const RealType s,
					const bool compmed,
					const NaturalType q,
					const bool normembedding,
					const RealType zerow)
	{
		ExptrSubGraphs = ExptrSubGr;
		ExptrLabels = ExptrLbl;
		trLabels = trLbl;
		ExpvsSubGraphs = ExpvsSubGr;
		vsLabels = vsLbl;
		Labels = Lbl;
		RESOLUTION = res;
		mOriginalParamRange = originalParamRange;
		IDtr = idtr;
		IDvs = idvs;
		Eps = eps;
		K = k;
		seed = s;
		compMed=compmed;
		Q = q;
		normEmbedding=normembedding;
		Zerow = zerow;
	}

	/**
	 * Evaluate the fitness of the generated code, which represents an instantiation of the parameters used in GRALG algorithm, which are
	 * - <b>Q</b> (max clusters in BSAS) - range [0, Qrange]
     * - <b>ni</b> (in function F) - range [0, 1]
     * - <b>t_F</b> thresold for cluster's cost function F - range [0, 1]
     * - <b>BMF Vinsertion</b> vertex insertion cost - range [0, 1]
     * - <b>BMF Vdeletion</b> vertex deletion cost - range [0, 1]
     * - <b>BMF Vsubstitution</b> vertex substitution cost - range [0, 1]
     * - <b>BMF Einsertion</b> edge insertion cost - range [0, 1]
     * - <b>BMF Edeletion</b> edge deletion cost - range [0, 1]
     * - <b>BMF Esubstitution</b> edge substitution cost - range [0, 1]
     *
     * @param[in] rCode genetic code of real-typed numbers.
     * @return RR recognition threshold achieved syntethizing a model using rCode as values for the parameters.
	 */
	template<typename CodeType>
	RealType Eval(const CodeType& rCode) const;

private:


	/**
	 * Container of the range of the parameters optimized by genetic algortihm
	 */
	vector<RealType> mOriginalParamRange;

	/**
	 * Resolution of the search space for the genetic code.
	 */
	NaturalType RESOLUTION;

	/**
	 * Containers of subgraphs of Training Set and Validation Set.
	 */
	SamplesContainer ExptrSubGraphs, ExpvsSubGraphs;

	/**
	 * Containers of the labels of Training Set and Validation Set
	 */
	LabelsContainer trLabels, vsLabels, ExptrLabels, Labels;

	/*
	 * ID of the original graphs associated to the subgraphs of tr and vs
	 */
	vector<NaturalType> IDtr, IDvs;

	RealType Eps;
	NaturalType K, seed, Q;

	bool compMed, normEmbedding;

	//imgGraphs only
	RealType Zerow;

	//absolute normalization class variable
	RealType maxComplexity;

};


//IMPL
template<class GralgType, class SamplesContainer, class LabelsContainer, class DissimilarityType>
template<typename CodeType>
RealType mtG1FitFun1<GralgType, SamplesContainer, LabelsContainer, DissimilarityType>::Eval(const CodeType& rCode) const{

	typename CodeType::const_iterator codeIt;
	vector<RealType> params;
	vector<RealType>::const_iterator paramRangeIt;

	//param de-normalization - every gene_i of the code must me mapped in [0, geneRange_i] from [0, RESOLUTION]
	NaturalType gene_i;
	RealType geneRange_i;
	if(mOriginalParamRange.size() == rCode.size()){
		codeIt = rCode.begin();
		paramRangeIt = mOriginalParamRange.begin();
		while(codeIt != rCode.end()){
			gene_i = *codeIt;
			geneRange_i = *paramRangeIt;
			params.push_back( ( (RealType)gene_i/(RealType)RESOLUTION ) * (RealType)geneRange_i );
			//cout << ( (RealType)gene_i/(RealType)RESOLUTION ) * (RealType)geneRange_i <<endl;
			codeIt++;
			paramRangeIt++;
		}
	}
	else{
		cerr<<"Dimension of gene's range vector doesn't match"<<endl;
	}

//	//DEBUG
//    for(NaturalType i=10; i<25; i++)
//        params[i]= 0;


	NaturalType ZeroParams = 0;
	for(NaturalType i=10; i<25; i++){
	    if(round(params[i]) == 0)
	        ZeroParams++;
	}

	//gralg Setup
	GralgType gralg;
	DissimilarityType mDiss;
    gralg.EnsembleAgent().ClustAlgo().RepInit().DissAgent().NormValue() = maxComplexity;
    gralg.EnsembleAgent().ClustAlgo().RepInit().DissAgent().Normalize() = true;
    gralg.EmbeddingAgent().DissAgent().NormValue() = maxComplexity;
    gralg.EmbeddingAgent().DissAgent().Normalize() = true;
    mDiss.NormValue() = maxComplexity;
    mDiss.Normalize() = true;

	//GRANULATION STRATEGY SETUP
	//parameters from optimizator (other parameters are set during initialization)
	RealType bound = params[0];
	gralg.GranulationAgent().Ni() = params[1];
	gralg.GranulationAgent().Phi() = params[2];
	gralg.GranulationAgent().T_F() = params[3];

	//ensemble
	gralg.EnsembleAgent().ClustAlgo().RepInit().DissAgent().PVertexInsertion()= params[4];
	gralg.EnsembleAgent().ClustAlgo().RepInit().DissAgent().PVertexDeletion()= params[5];
	gralg.EnsembleAgent().ClustAlgo().RepInit().DissAgent().PVertexSubstitution()= params[6];
	gralg.EnsembleAgent().ClustAlgo().RepInit().DissAgent().PEdgeInsertion()= params[7];
	gralg.EnsembleAgent().ClustAlgo().RepInit().DissAgent().PEdgeDeletion()= params[8];
	gralg.EnsembleAgent().ClustAlgo().RepInit().DissAgent().PEdgeSubstitution()= params[9];
    gralg.EnsembleAgent().ClustAlgo().RepInit().DissAgent().EdgesDissAgent().WdXc() = round(params[10]);
    gralg.EnsembleAgent().ClustAlgo().RepInit().DissAgent().EdgesDissAgent().WdYc() = round(params[11]);
    gralg.EnsembleAgent().ClustAlgo().RepInit().DissAgent().EdgesDissAgent().WdB() = round(params[12]);
    gralg.EnsembleAgent().ClustAlgo().RepInit().DissAgent().VerticesDissAgent().WXc() = round(params[13]);
    gralg.EnsembleAgent().ClustAlgo().RepInit().DissAgent().VerticesDissAgent().WYc() = round(params[14]);
    gralg.EnsembleAgent().ClustAlgo().RepInit().DissAgent().VerticesDissAgent().WCrCb() = round(params[15]);
    gralg.EnsembleAgent().ClustAlgo().RepInit().DissAgent().VerticesDissAgent().WSat() = round(params[16]);
    gralg.EnsembleAgent().ClustAlgo().RepInit().DissAgent().VerticesDissAgent().WBrg() = round(params[17]);
    gralg.EnsembleAgent().ClustAlgo().RepInit().DissAgent().VerticesDissAgent().WWlet() = round(params[18]);
    gralg.EnsembleAgent().ClustAlgo().RepInit().DissAgent().VerticesDissAgent().WArea() = round(params[19]);
    gralg.EnsembleAgent().ClustAlgo().RepInit().DissAgent().VerticesDissAgent().WSymm() = round(params[20]);
    gralg.EnsembleAgent().ClustAlgo().RepInit().DissAgent().VerticesDissAgent().WRnd() = round(params[21]);
    gralg.EnsembleAgent().ClustAlgo().RepInit().DissAgent().VerticesDissAgent().WCmp() = round(params[22]);
    gralg.EnsembleAgent().ClustAlgo().RepInit().DissAgent().VerticesDissAgent().WDir() = round(params[23]);
    gralg.EnsembleAgent().ClustAlgo().RepInit().DissAgent().VerticesDissAgent().WOr() = round(params[24]);

	//ebmedding
	gralg.EmbeddingAgent().DissAgent().PVertexInsertion()= params[4];
	gralg.EmbeddingAgent().DissAgent().PVertexDeletion()= params[5];
	gralg.EmbeddingAgent().DissAgent().PVertexSubstitution()= params[6];
	gralg.EmbeddingAgent().DissAgent().PEdgeInsertion()= params[7];
	gralg.EmbeddingAgent().DissAgent().PEdgeDeletion()= params[8];
	gralg.EmbeddingAgent().DissAgent().PEdgeSubstitution()= params[9];
    gralg.EmbeddingAgent().DissAgent().EdgesDissAgent().WdXc() = round(params[10]);
    gralg.EmbeddingAgent().DissAgent().EdgesDissAgent().WdYc() = round(params[11]);
    gralg.EmbeddingAgent().DissAgent().EdgesDissAgent().WdB() = round(params[12]);
    gralg.EmbeddingAgent().DissAgent().VerticesDissAgent().WXc() = round(params[13]);
    gralg.EmbeddingAgent().DissAgent().VerticesDissAgent().WYc() = round(params[14]);
    gralg.EmbeddingAgent().DissAgent().VerticesDissAgent().WCrCb() = round(params[15]);
    gralg.EmbeddingAgent().DissAgent().VerticesDissAgent().WSat() = round(params[16]);
    gralg.EmbeddingAgent().DissAgent().VerticesDissAgent().WBrg() = round(params[17]);
    gralg.EmbeddingAgent().DissAgent().VerticesDissAgent().WWlet() = round(params[18]);
    gralg.EmbeddingAgent().DissAgent().VerticesDissAgent().WArea() = round(params[19]);
    gralg.EmbeddingAgent().DissAgent().VerticesDissAgent().WSymm() = round(params[20]);
    gralg.EmbeddingAgent().DissAgent().VerticesDissAgent().WRnd() = round(params[21]);
    gralg.EmbeddingAgent().DissAgent().VerticesDissAgent().WCmp() = round(params[22]);
    gralg.EmbeddingAgent().DissAgent().VerticesDissAgent().WDir() = round(params[23]);
    gralg.EmbeddingAgent().DissAgent().VerticesDissAgent().WOr() = round(params[24]);

	//dissimilarity
	mDiss.PVertexInsertion()= params[4];
	mDiss.PVertexDeletion()= params[5];
	mDiss.PVertexSubstitution()= params[6];
	mDiss.PEdgeInsertion()= params[7];
	mDiss.PEdgeDeletion()= params[8];
	mDiss.PEdgeSubstitution()= params[9];
    mDiss.EdgesDissAgent().WdXc() = round(params[10]);
    mDiss.EdgesDissAgent().WdYc() = round(params[11]);
    mDiss.EdgesDissAgent().WdB() = round(params[12]);
    mDiss.VerticesDissAgent().WXc() = round(params[13]);
    mDiss.VerticesDissAgent().WYc() = round(params[14]);
    mDiss.VerticesDissAgent().WCrCb() = round(params[15]);
    mDiss.VerticesDissAgent().WSat() = round(params[16]);
    mDiss.VerticesDissAgent().WBrg() = round(params[17]);
    mDiss.VerticesDissAgent().WWlet() = round(params[18]);
    mDiss.VerticesDissAgent().WArea() = round(params[19]);
    mDiss.VerticesDissAgent().WSymm() = round(params[20]);
    mDiss.VerticesDissAgent().WRnd() = round(params[21]);
    mDiss.VerticesDissAgent().WCmp() = round(params[22]);
    mDiss.VerticesDissAgent().WDir() = round(params[23]);
    mDiss.VerticesDissAgent().WOr() = round(params[24]);


	//identifico un range ragionevole dei valori di theta
	typename SamplesContainer::const_iterator sampIT;
	RealType min, max, SOD, min100=0, max100=0, SOD100=0, tMin, tMax, tStep;;
	NaturalType Nsamples = 500;

	for(NaturalType i = 0; i < Nsamples; i++){
		NaturalType index = (seed*i)%ExptrSubGraphs.size();
		min=2.0, max=0, SOD=0;
		sampIT = ExptrSubGraphs.begin();

		while(sampIT!=ExptrSubGraphs.end())
		{
			RealType d=mDiss.Diss(*sampIT, ExptrSubGraphs[index]);

			SOD+=d;

			if(d!=0 && d<min)
				min=d;

			if(d>max)
				max=d;

			sampIT++;
		}
		SOD=SOD/(RealType)(ExptrSubGraphs.size()-1);

		SOD100+=SOD;
		min100+=min;
		max100+=max;
	}
	tMin = min100/Nsamples + (SOD100/Nsamples - min100/Nsamples)*bound;
	tMax = max100/Nsamples - (max100/Nsamples - SOD100/Nsamples)*bound;
	tMin = 0;
	tMax = 1;
	tStep = (tMax - tMin)/10.0;
	//cout<<"bound: "<<bound<<", tMin: "<<tMin<<", tMax: "<<tMax<<", tStep: "<<tStep<<", Q: "<<Q<<", [min100, max100] = "<<min100<<","<<max100<<endl;


	gralg.GetIDTR() = IDtr;
	gralg.GetIDTS() = IDvs;
	gralg.ExtractionRequired() = false;
	gralg.EnsembleAgent().thetaRange() = make_pair(tMin, tMax);
	gralg.EnsembleAgent().thetaStep() = tStep;
	gralg.GranulationAgent().Eps() = Eps;
	gralg.ClassificationAgent().K() = K;
	gralg.EnsembleAgent().getQ() = Q;
	gralg.EnsembleAgent().ClustAlgo().RepInit().RandSeedSetup(seed);
	gralg.GranulationAgent().CompMed() = compMed;
	gralg.NormEmbedding() = normEmbedding;

	RealType res = gralg.Process(ExptrSubGraphs, trLabels, ExptrLabels, ExpvsSubGraphs, vsLabels, Labels);

	cout << "RR: "<<res;
	res = Zerow*(RealType)ZeroParams/15.0 + (1-Zerow)*res;
    cout << ", zeroParams: "<<ZeroParams<<", fitness: "<<res<<endl;

	return res;
}

#endif /* G1FITFUN1_HPP_ */

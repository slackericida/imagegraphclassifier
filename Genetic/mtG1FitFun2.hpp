/** @brief File G1FitFun2.hpp, that contains the implementation of fitness evaluating function for feature selection in strategy 1 of GRALG algorithm.
 * This class implements the fitness evaluating function for feature selection in the version 1 of GRALG algorithm.
 *
 * @file G1FitFun2.hpp
 * @author Filippo Bianchi
 */

#ifndef MTGRALGFITFUN2_HPP_
#define MTGRALGFITFUN2_HPP_

#include "../FS_and_Test/Projection.hpp"

/**
 * @brief This class implements the fitness evaluating function for feature selection in the version 1 of GRALG algorithm.
 *
 * The code that is optimized represent the projection mask used for selecting the relevant features.
 *
 * <b>Template Parameters</b>
 *  <table class="contents">
 *  <tr>
 *      <td class="indexkey"><b>Name</b></td>
 *      <td class="indexkey"><b>Description</b></td>
 *  </tr>
 *  <tr>
 *  	<td class="indexvalue">ClassificationStrategy</td>
 *  	<td class="indexvalue"> A class which implements a classifier.</td>
 *  </tr>
 *  <tr>
 *  	<td class="indexvalue">SamplesContainer</td>
 *  	<td class="indexvalue">A class which define the type of the container of the structure that must be processed.</td>
 *  </tr>
 *  <tr>
 *  	<td class="indexvalue">LabelsContainer</td>
 *  	<td class="indexvalue">A class which define the type of the container of labels of the structures.</td>
 *  </tr>
 *  </table>
 */
template<class ClassificationStrategy, class SamplesContainer, class LabelsContainer>
class mtG1FitFun2{

public:

	/**
	 * Type of the genetic code.
	 */
	typedef std::vector<spare::NaturalType> CodeType;



	//MEMBER FUNCTION
	/**
	 * Initialization function
	 * @param[in] samplesTR Samples of Training Set used for training the system, i.e. find the mask which selects the relevant features.
	 * @param[in] labelsTR The set of class labels for the samples of Training Set.
	 * @param[in] samplesVS Samples of Validation Set used for evaluating the goodness of the extracted masks.
	 * @param[in] labelsVS The set of class labels for the samples of Validation Set.
	 * @param[in] K Number of neighbours of k-NN classification algorithm.
	 * @param[in] alpha Parameter used for estimating the value of fitness, which weights the complexity of the mask (# of "1"), over the recognition rate achieved with the mask.
	 */
	void DataSetup( SamplesContainer samplesTR, LabelsContainer labelsTR, SamplesContainer samplesVS, LabelsContainer labelsVS, spare::NaturalType k, spare::RealType alpha){
		mSamples = samplesTR;
		mLabels = labelsTR;
		mSamplesVS = samplesVS;
		mLabelsVS = labelsVS;
		K = k;
		alfa = alpha;
	}

	spare::RealType Eval(const CodeType& rCode) const;

private:


	/**
	 * Containers of Training Set and Validation Set samples.
	 */
	SamplesContainer mSamples, mSamplesVS;

	/**
	 * Containers of labels of Training Set and Validation Set samples.
	 */
	LabelsContainer mLabels, mLabelsVS;

	/**
	 * Weight alpha in recognition rate value.
	 */
	spare::RealType alfa;

	spare::NaturalType K;

};


//IMPL

template<class ClassificationStrategy, class SamplesContainer, class LabelsContainer>
spare::RealType mtG1FitFun2<ClassificationStrategy, SamplesContainer, LabelsContainer>::Eval(const CodeType& rCode) const{

	//check if current mask has all zero-elements
	spare::NaturalType zcount=0;
	for(spare::NaturalType i=0; i < rCode.size(); i++){
		zcount+= rCode[i];
	}
	if(zcount==0) return 0;

	SamplesContainer reducedSamples, reducedSamplesVS;
	spare::NaturalType count, ErrorCount=0;
	spare::RealType ErrorCost;
	Projection pr;

	//project tr
	pr.Project(mSamples, rCode, reducedSamples, count);
	//project vs
	pr.Project(mSamplesVS, rCode, reducedSamplesVS, count);

	ClassificationStrategy mClassifier;
	mClassifier.K()=K;

	mClassifier.Incremental()="Off";
	mClassifier.Learn(reducedSamples.begin(), reducedSamples.end(), mLabels.begin());

	LabelsContainer labelsOutput(mLabelsVS.size(), "UNDETERMINED");
	mClassifier.Process(reducedSamplesVS.begin(), reducedSamplesVS.end(), labelsOutput.begin());

	typename LabelsContainer::const_iterator vsLabelsIt = mLabelsVS.begin();
	typename LabelsContainer::const_iterator labelsOutputIt = labelsOutput.begin();
	while(vsLabelsIt != mLabelsVS.end()){
		if(*vsLabelsIt != *labelsOutputIt){
			ErrorCount++;
		}
		vsLabelsIt++;
		labelsOutputIt++;

	}

	ErrorCost= ErrorCount / spare::RealType(mLabelsVS.size());

	//% of feature selected
	spare::RealType nFeat = 1- (spare::RealType)(count -1) / (spare::RealType)rCode.size();
	//cout << "RR: "<<1-ErrorCost<<", nFeat: "<<nFeat<<endl;
	return alfa*(1-ErrorCost) + (1-alfa)*nFeat;
}


#endif /* GRALGFITFUN2_HPP_ */

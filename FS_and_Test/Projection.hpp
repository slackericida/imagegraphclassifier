/** @brief File Projection.hpp, implement a projection function
 *
 * @file Projection.hpp
 * @author Filippo Bianchi
 */

#ifndef PROJECTION_HPP_
#define PROJECTION_HPP_

#include <spare/SpareTypes.hpp>

/** @brief implement a projection function.
 *  A datastructure (e.g. a vector) is a projected in a lower dimension space, according to an input mask.
 */
class Projection{

public:

	//MEMBER FUNCTION
	/**
	 * Project an element in a lower dimension space, according to a mask
	 * @param[in] rSource Element to be projected.
	 * @param[in] rCode Mask used for projection. It is a container made of "1" and "0" in correspondence to the elements that must be projected or not.
	 * @param[out] rDest Projected element.
	 * @param[out] rCount Dimension of the projected element (i.e. number of "1" in rCode).
	 */
	template <typename SamplesContainer, typename CodeType>
	void Project( const SamplesContainer&    	rSource,
				  const CodeType&    			rCode,
				  SamplesContainer&   	    	rDest,
				  spare::NaturalType& 			rCount){

		typedef typename SamplesContainer::value_type SampleType;

		if (rSource.empty())
		{
			rDest.clear();
			rCount= 0;
			return;
		}

		if ((*rSource.begin()).size() != rCode.size())
		{
			rDest.clear();
			rCount= 0;
			return;
		}

		spare::NaturalType Count= 0;
		typename CodeType::const_iterator cit= rCode.begin();
		while (rCode.end() != cit)
		{
			if (*cit++)
			{
				++Count;
			}
		}

		if (!Count)
		{
			rDest.clear();
			rCount= 0;
			return;
		}

		rCount= Count;
		rDest.resize(rSource.size());
		SampleType Buf(Count);
		typename SamplesContainer::const_iterator sit= rSource.begin();
		typename SamplesContainer::iterator dit= rDest.begin();
		while (rSource.end() != sit)
		{
			cit= rCode.begin();
			typename SampleType::const_iterator oit= sit->begin();
			typename SampleType::iterator pit= Buf.begin();
			while (sit->end() != oit)
			{
				if (*cit++)
				{
					(*pit++)= *oit++;
				}
				else
				{
					++oit;
				}
			}
			(*dit++)= Buf;
			++sit;
		}


	}

};


#endif /* PROJECTION_HPP_ */

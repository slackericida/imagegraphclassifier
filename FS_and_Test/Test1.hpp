/** @brief File Test1.hpp, that contains the implementation of the feature selection and performance test of the model on the Test Set for startegy 1 of GRALG.
 *
 * @file Test1.hpp
 * @author Filippo Bianchi
 */

#ifndef TEST1_HPP_
#define TEST1_HPP_

//STD INCLUDES
#include <vector>
#include <string>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "boost/filesystem.hpp"

//LOCAL
#include "../GRALG_IO.hpp"

/** @brief This class implements the feature selection and performance test of the model on the Test Set for startegy 1 of GRALG.
 *
 * Once the parameters optimization is completed by the optimization done by the first genetic algorithm, a second one is
 * istatiated for the feature selection. The object of optimization is a mask responsible of selecting the minimum number
 * of features  (i.e. symbols of the alphabet) which maximize the performance, according to a given performance cost function.
 *
 * <b>Template Parameters</b>
 *  <table class="contents">
 *  <tr>
 *      <td class="indexkey"><b>Name</b></td>
 *      <td class="indexkey"><b>Description</b></td>
 *  </tr>
 *  <tr>
 *  	<td class="indexvalue">ProjectionType</td>
 *  	<td class="indexvalue">A class which implements a projection function, used for selecting the features identified by a mask.</td>
 *  </tr>
 *  <tr>
 *  	<td class="indexvalue">GranulationStrategy</td>
 *  	<td class="indexvalue">A class which implements a granulator that extract an alphabet of symbols from a set of subgraphs.</td>
 *  </tr>
 *  <tr>
 *  	<td class="indexvalue">GeneticType</td>
 *  	<td class="indexvalue">A class which define the type of the mask that we want to optimize.</td>
 *  </tr>
 *  <tr>
 *  	<td class="indexvalue">EmbeddingStrategy</td>
 *  	<td class="indexvalue">A class which implements an embedderwhich transform a graph in a real-typed vector.</td>
 *  </tr>
 *  <tr>
 *  	<td class="indexvalue">ClassificationStrategy</td>
 *  	<td class="indexvalue">A class which implements a classifier</td>
 *  </tr>
 *  <tr>
 *  	<td class="indexvalue">SymbolsContainer</td>
 *  	<td class="indexvalue">A class which define the type of the container of the symbols of the alphabet.</td>
 *  </tr>
 *  <tr>
 *  	<td class="indexvalue">EnvType</td>
 *  	<td class="indexvalue">A class which define the type of the environment used in the fitness function of the genetic algorithm.</td>
 *  </tr>
 *  </table>
 */
template<class GranulationStrategy, class EmbeddingStrategy, class ClassificationStrategy, class GeneticType, class ProjectionType, class SymbolContainer, class EnvType>
class Test1{

public:

    //CONSTRUCTOR
    /**
     * Default Constructor
     */
    Test1(){
        fitRank = 0;
    }

	//DEF
	/**
	 * Type of the mask to be optimized.
	 */
	typedef std::vector<spare::NaturalType> CodeType;

	/**
	 * Type of the ensemble agent.
	 */
	typedef typename GranulationStrategy::EnsembleStrategy EnsembleStrategy;

	//ACCESS
	/**
	 * Read-Write access to granulation agent.
	 */
	GranulationStrategy& GranulationAgent(){
		return granulationAgent;
	}

	/**
	 * Read-only access to granulation agent.
	 */
	const GranulationStrategy& GranulationAgent() const{
		return granulationAgent;
	}

	/**
	 * Read-Write access to esnemble agent.
	 */
	EnsembleStrategy& EnsembleAgent(){
		return granulationAgent.EnsembleAgent();
	}

	/**
	 * Read-only access to ensemble agent.
	 */
	const EnsembleStrategy& EnsembleAgent() const{
		return granulationAgent.EnsembleAgent();
	}

	/**
	 * Read/Write access to embedding agent.
	 */
	EmbeddingStrategy& EmbeddingAgent(){
		return embeddingAgent;
	}

	/**
	 * Read-only access to embedding agent.
	 */
	const EmbeddingStrategy& EmbeddingAgent() const{
		return embeddingAgent;
	}

	/**
	 * Read/Write access to classification agent.
	 */
	ClassificationStrategy& ClassificationAgent(){
		return classificationAgent;
	}

	/**
	 * Read-only access to classification agent.
	 */
	const ClassificationStrategy& ClassificationAgent() const{
		return classificationAgent;
	}

	/**
	 * Read/Write access to genetic algorithm agent.
	 */
	GeneticType* getGeneticAgent(){
		return GeneticAlgo;
	}

	/**
	 * Read-only access to genetic algorithm agent.
	 */
	void setGeneticAgent(GeneticType* genalg) const{
		GeneticAlgo = genalg;
	}

	/**
	 * Read/Write acess to projection agent.
	 */
	ProjectionType& ProjectionAgent(){
		return projectionAgent;
	}

	/**
	 * Read-only access to projection agent.
	 */
	const ProjectionType& ProjectionAgent() const{
		return projectionAgent;
	}

	/**
	 * Read-only access to alphabet size.
	 */
	const spare::NaturalType& AlphabetSize() const{
		return alphabetSize;
	}

	/**
	 * Read-only access to the number of features selected after optimization
	 */
	const spare::NaturalType& SelectedFeatures() const{
		return selectedFeat;
	}

	bool& PrintEmbedding(){
		return printEmbedding;
	}

	const bool& PrintEmbedding() const{
		return printEmbedding;
	}

	/**
	 * R/W access to the flag for normalize the embedding
	 */
	bool& NormEmbedding(){
		return normEmbedding;
	}

	/**
	 * R-Only access to the flag for normalize the embedding
	 */
	const bool& NormEmbedding() const{
		return normEmbedding;
	}

    NaturalType& FitRank(){
        return fitRank;
    }

    const NaturalType& FitRank() const{
        return fitRank;
    }


	//MEMBER FUNCTION
	/**
	 * Perform an optimization for feature selection. Once the best fitting mask has been identifyied,
	 * a model is syntethized using the optimal parameters (found during a previous optimization) and the optimal mask.
	 * Then, the performance of the classifcation model is evaluated upon the Test Set.
	 *
	 * @param[in] ExptrGraphs Subgraphs of the Training set.
	 * @param[in] trLabels Class labels of the subgraphs of the Training Set.
	 * @param[in] IDTR Container which associate to each subgraph of Training Set the ID of the original graph.
	 * @param[in] ExpvsGraphs Subgraphs of the Validation set.
	 * @param[in] vsLabels Class labels of the subgraphs of the Validation Set.
	 * @param[in] IDVS Container which associate to each subgraph of Validation Set the ID of the original graph.
	 * @param[in] ExptsGraphs Subgraphs of the Test set.
	 * @param[in] tsLabels Class labels of the subgraphs of the Test Set.
	 * @param[in] IDTS Container which associate to each subgraph of Test Set the ID of the original graph.
	 * @param[in] fs_evolutions Maximum number of evolutions of feature selection optimization algorithm.
	 * @param[in] seed Random seed which initialize the genetic algorithm.
	 * @param[in] K Number of neighbours for k-NN classification algorithm.
	 * @param[in] alfa Value used for evaluating the fitness, which weights the complexity of the model generated by the code over its performace.
	 * @param[in] t_FS Minimum recognition rate required during the optimization.
	 * @ return RR Recognition rate on the Test Set achieved with the classification model build with highest fitness mask.
	 */
	template<typename SamplesContainer, typename LabelsContainer>
	spare::RealType Process(const SamplesContainer& ExptrGraphs, const LabelsContainer& trLabels, const LabelsContainer& ExptrLabels, const std::vector<spare::NaturalType>& IDTR,
							const SamplesContainer& ExpvsGraphs, const LabelsContainer& vsLabels, const std::vector<spare::NaturalType>& IDVS,
							const SamplesContainer& ExptsGraphs, const LabelsContainer& tsLabels, const std::vector<spare::NaturalType>& IDTS,
							const LabelsContainer& Labels,
							const spare::NaturalType fs_evolutions, const spare::RealType seed, const spare::NaturalType K,
							const spare::RealType alfa, const spare::RealType t_FS);

	inline void setGenAlgo(std::string onoff){
		GeneticAlgo = new GeneticType(onoff);
	}

	inline void delGenAlgo(){
		delete GeneticAlgo;
	}

private:

	/**
	 * Granulation agent
	 */
	GranulationStrategy granulationAgent;

	/**
	 * Embedding agent
	 */
	EmbeddingStrategy embeddingAgent;

	/**
	 * Classification agent
	 */
	ClassificationStrategy classificationAgent;

	/**
	 * Pointer to the genetic algorithm agent
	 */
	GeneticType* GeneticAlgo;

	/**
	 * Projection agent
	 */
	ProjectionType projectionAgent;

	/*
	 * Variables that will contain the size of the alphabet and the number of feature selected by feat sel algo
	 */
	spare::NaturalType alphabetSize, selectedFeat;

	/**
	 * Flag for writing the embedding on file
	 */
	bool printEmbedding;

	/**
	 * Flag for doing a component-wise normalization of the embedding
	 */
	bool normEmbedding;

	NaturalType fitRank;
};


// **************** IMPL *********************
template<class GranulationStrategy, class EmbeddingStrategy, class ClassificationStrategy, class GeneticType, class ProjectionType, class SymbolContainer, class EnvType>
template<typename SamplesContainer, typename LabelsContainer>
spare::RealType Test1<GranulationStrategy, EmbeddingStrategy, ClassificationStrategy,GeneticType, ProjectionType, SymbolContainer, EnvType>::
				Process(const SamplesContainer& ExptrGraphs, const LabelsContainer& trLabels, const LabelsContainer& ExptrLabels, const std::vector<spare::NaturalType>& IDTR,
						const SamplesContainer& ExpvsGraphs, const LabelsContainer& vsLabels, const std::vector<spare::NaturalType>& IDVS,
						const SamplesContainer& ExptsGraphs, const LabelsContainer& tsLabels, const std::vector<spare::NaturalType>& IDTS,
						const LabelsContainer& Labels,
						const spare::NaturalType fs_evolutions, const spare::RealType seed, const spare::NaturalType K,
						const spare::RealType alfa, const spare::RealType t_FS){

	// ---- INIT ----
	SymbolContainer trSymbols;
	std::vector<std::vector<spare::RealType> > vvTR, vvVS, vvTS, vvTRr, vvTSr;
	GRALG_IO gio;


	// ---- GRANULATION ----
	granulationAgent.Granulate(ExptrGraphs, ExptrLabels, trSymbols, Labels);
	alphabetSize = trSymbols.size();


	// ---- EMBEDDING ----
	//tr embedding
	embeddingAgent.SubstructuresIds() = IDTR;
	embeddingAgent.NumberOfStruct() = trLabels.size();
	embeddingAgent.getVectors(ExptrGraphs, trSymbols, vvTR);
	//vs embedding
	embeddingAgent.SubstructuresIds() = IDVS;
	embeddingAgent.NumberOfStruct() = vsLabels.size();
	embeddingAgent.getVectors(ExpvsGraphs, trSymbols, vvVS);

	//embedding normalization
	if(normEmbedding){
		for(spare::NaturalType i=0; i<vvTR[0].size(); i++){
			spare::RealType maxi=0;
			for(spare::NaturalType j=0; j<vvTR.size();j++){
				if(vvTR[j][i]>maxi)
					maxi=vvTR[j][i];
			}
			for(spare::NaturalType j=0; j<vvTR.size();j++){
				vvTR[j][i]=vvTR[j][i]/maxi;
			}
		}

		for(spare::NaturalType i=0; i<vvVS[0].size(); i++){
			spare::RealType maxi=0;
			for(spare::NaturalType j=0; j<vvVS.size();j++){
				if(vvVS[j][i]>maxi)
					maxi=vvVS[j][i];
			}
			for(spare::NaturalType j=0; j<vvVS.size();j++){
				vvVS[j][i]=vvVS[j][i]/maxi;
			}
		}
	}

	// ---- FEATURE SELECTION ----
	setGenAlgo("On");
	CodeType mask(trSymbols.size(), 1);

	cout << "#TrSymbols: "<<trSymbols.size()<<endl;

	if(fs_evolutions > 1){
		if(trSymbols.size() < GeneticAlgo->PopSize()){ //cazzo è sta robba?
			GeneticAlgo->PopSize() = trSymbols.size();
		}
	    //GeneticAlgo->PopSize() = 100;
		GeneticAlgo->EnvAgent()= EnvType(trSymbols.size(), 0, 1);
		GeneticAlgo->EnvAgent().RandSeedSetup(seed, seed);
		GeneticAlgo->RandSeedSetup(seed);

		GeneticAlgo->EnvAgent().FitnessAgent().DataSetup(vvTR, trLabels, vvVS, vsLabels, K, alfa);

		std::cout<<"Initializing genetic algorithm for F.S. .."<<std::endl;
		//GeneticAlgo->Initialize();
        //initialize custom solutions
        vector<vector<NaturalType> > initSolution;
        for(NaturalType i= 0; i<trSymbols.size(); i++){
            vector<NaturalType> vi(trSymbols.size(),0);
            vi[i] = 1;
            initSolution.push_back(vi);
        }
        vector<NaturalType> vi(trSymbols.size(),1);
        initSolution.push_back(vi);
        GeneticAlgo->Initialize(initSolution.begin(), initSolution.end());
		std::cout<<"Done"<<std::endl;

		//optimization cycle
		spare::NaturalType i;
		for (i= 0; i < fs_evolutions && GeneticAlgo->GetPerformance() < t_FS; ++i)
		{
			GeneticAlgo->StepUp();
		}
		std::cout << "Evol "<< i<<". RR: " << GeneticAlgo->GetPerformance()<<" %" << std::endl;

		//reading solution
		mask = GeneticAlgo->GetSolution();
	}
	else{
		std::cout<<"No fs optimization"<<std::endl;
	}

	delGenAlgo();

    string J = static_cast<ostringstream*>( &(ostringstream() <<fitRank)) ->str();
    string dir = "/home/filippo/Desktop/Granules/Fit"+J+"/";
    boost::filesystem::remove_all(dir);
    mkdir(dir.c_str(),0700);

	// Write Final Granules on Folder
	typename SymbolContainer::iterator symb_it = trSymbols.begin();
	for(NaturalType i = 0; i < mask.size(); i++){
	    typename SymbolContainer::value_type symb = *symb_it++;
	    if(mask[i]){
	        gio.WriteGranule(dir, symb, i);
	    }
	}

	// ---- FINAL TEST ----

	//ts embedding
	embeddingAgent.SubstructuresIds() = IDTS;
	embeddingAgent.NumberOfStruct() = tsLabels.size();
	embeddingAgent.getVectors(ExptsGraphs, trSymbols, vvTS);

	//projection of tr and ts embedding using the optimal mask found by gen algo
	spare::NaturalType count;
	projectionAgent.Project(vvTR, mask, vvTRr, count);
	projectionAgent.Project(vvTS, mask, vvTSr, count);
	spare::RealType nFeat = (spare::RealType)count / (spare::RealType)(*(vvTR.begin())).size()*100;
	std::cout <<nFeat<<" % features selected ("<<count<<")."<<std::endl;

	selectedFeat=count;

	//classifier initialization
	classificationAgent.K() = K;
	classificationAgent.Incremental() = "Off";
	classificationAgent.Learn(vvTRr.begin(), vvTRr.end(), trLabels.begin());

	//output labels allocation
	LabelsContainer labelsOutput(tsLabels.size(), "UNDETERMINED");
	classificationAgent.Process(vvTSr.begin(), vvTSr.end(), labelsOutput.begin());

	spare::RealType err = 0.;

	typename LabelsContainer::iterator itOutLabels = labelsOutput.begin();
	typename LabelsContainer::const_iterator itTSLabels = tsLabels.begin();

	//recognition rate cycle
	while (itOutLabels != labelsOutput.end()) {
		if (*itOutLabels != *itTSLabels)
			err += 1;

		itOutLabels++;
		itTSLabels++;
	}

	spare::RealType RR = (1-err/tsLabels.size());

	if(printEmbedding){
		cout <<"Writing embedding on file.."<<endl;
		string J = static_cast<ostringstream*>( &(ostringstream() <<fitRank)) ->str();
		gio.WriteEmbedding(dir, "TREmb", vvTRr);
		gio.WriteLabel(dir,"TRlabels",trLabels);
		gio.WriteEmbedding(dir, "TSEmb", vvTSr);
		gio.WriteLabel(dir,"TSlabels",tsLabels);
	}

	return RR;
}

#endif /* TEST1_HPP_ */

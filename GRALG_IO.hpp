/** @brief File GRALG_IO.hpp
 *
 * The file contains some I/O functions for printing data structures on the screen,
 * saving them on disk and reading them back again from file.
 *
 * @file GRALG_IO.hpp
 * @author James Ronnie Dio
 */

#ifndef GRALG_IO_HPP_
#define GRALG_IO_HPP_

//std
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include <spare/SpareTypes.hpp>

using namespace std;

/** @brief class for I/O functions
 * The file contains some Input-Output functions for printing data structures on the screen,
 * saving on a file and reading them back again from the file.
 */
class GRALG_IO{
public:

	/**
	 * Writes a set of granules on the disk.
	 * Generates files with the informations contained in "sym" and stores them in the location
	 * specified by "destinationDir".
	 */
	template<typename SymbolsContainer>
	void WriteGranuleBatch(const std::string& destinationDir, const SymbolsContainer& sym) const;

	/**
	 * Writes a single granule contained into "sym" on the disk at "destinationDir" location, with the "id" in the name
	 */
	template<typename Symbol>
	void WriteGranule(const std::string& destinationDir,const Symbol& sym, const spare::NaturalType& id) const;

	/**
	 * Reads a set of granules from the disk at "sourceDir" location and stores them into "sym"
	 */
	template<typename SymbolsContainer>
	void ReadGranule(const std::string& sourceDir, SymbolsContainer& sym) const;

	/**
	 * Writes an embedding of a (set of) graph(s) stored into "embed" on the disk at "destinationDir"
	 * location, in a file called "name"
	 */
	template<typename EmbeddingContainer>
	void WriteEmbedding(const std::string& destinationDir, const std::string& name, const EmbeddingContainer& embed);

	/**
	 * Reads an embedding from location"sourceDir" of the disk and stores it into "embed" container
	 */
	template<typename EmbeddingContainer>
	void ReadEmbedding(const std::string& sourceDir, EmbeddingContainer& embed);

	/**
	 * Writes a set of graphs whose structure is contained in "samples" and labels in "label" on the disk at
	 * "destinationDir" location, with the identifiers contained in "ids"
	 */
	template <typename BoostGraphsContainer, typename LabelsContainer, typename IdsContainer>
	void WriteGraphBatch(const std::string& destinationDir, const BoostGraphsContainer& samples, const LabelsContainer& labels, const IdsContainer& ids) const;

	/**
	 * Writes a set of graphs whose structure is contained in "samples" and labels in "label" on the disk at
	 * "destinationDir" location
	 */
	template <typename BoostGraphsContainer, typename LabelsContainer>
	void WriteGraphBatch(const std::string& destinationDir, const BoostGraphsContainer& samples, const LabelsContainer& labels) const;

	/**
	 * Write a single graph with identifier "id", structure "sample" and label "label" on the disk at location
	 * "destinationDir"
	 */
	template <typename BoostGraphType, typename LabelType>
	void WriteGraph(const std::string& destinationDir, const BoostGraphType& sample, const LabelType& label, const spare::NaturalType& id) const;

	/**
	 * @param[in] sameples sub graphs extracted from a (set of) graph(s)
	 * @param[in] labels classes of the original(s) graph(s)
	 * @param[in] ids association with the subgraph and the original graph that generated it
	 */
	template <typename BoostGraphsContainer, typename IdsContainer>
	void WriteSubGraphs(const std::string& GdestinationDir, const BoostGraphsContainer& samples, const IdsContainer& ids) const;

	/**
	 * Write genetic code on file
	 */
	template<typename GenCode>
	void WriteGenCode(const GenCode& code, const std::string& destinationDir) const;

	/**
	 * Read genetic code from file
	 */
	template<typename GenCode>
	void ReadGenCode(GenCode& code, const std::string& destinationDir) const;

	/**
	 * Write log file
	 */
	void WriteLog( 	const std::string& name,
					const spare::NaturalType K,
					const spare::RealType time,
					const spare::RealType RR) const;

	/**
	 * Write a verbouse log file
	 */
	template<typename FitnessVect>
	void WriteLogV( const std::string& name,
            const spare::RealType seed,
            const spare::NaturalType K,
            const spare::RealType time,
            const vector<spare::RealType> RRvec,
            const FitnessVect TopFit,
            const vector<spare::NaturalType> alphabetVec,
            const vector<spare::NaturalType> featVec,
            const spare::NaturalType totevol) const;

	/**
	 * print the graph "g" on the screen
	 */
	template<typename GraphType>
	void stampaGrafo(const GraphType& g);

	/**
	 * print the container (e.g. embedding) "c" on the screen
	 */
	template<typename ContainerType>
	void printContainer(const ContainerType& c);

	void WriteLabel(const std::string& destinationDir, const std::string& name, const std::vector<std::string>& labels);

	string doubleToStr(const spare::RealType d) const;

};

//IMPL
template<typename SymbolsContainer>
void GRALG_IO::WriteGranuleBatch(const std::string& destinationDir, const SymbolsContainer& sym) const{

	typedef typename SymbolsContainer::const_iterator symIT;

	symIT sit = sym.begin();
	spare::NaturalType id = 0;

	std::string infopath = destinationDir+"/INFO.txt";
	remove(infopath.c_str());

	while(sit != sym.end()){

		WriteGranule(destinationDir, *sit, id);

		sit++;
		id++;
	}
}

template<typename Symbol>
void GRALG_IO::WriteGranule(const std::string& destinationDir,const Symbol& sym, const spare::NaturalType& id) const{

	WriteGraph(destinationDir, sym.getSubstructure(), 0, id);

//	std::ofstream stream;
//	std::stringstream ss;
//	std::string fullPath;
//
//	fullPath=destinationDir+"/INFO.txt";
//
//
//	stream.open(fullPath.c_str(), std::ios::out| std::ios::app);
//	if(stream.is_open())
//	{
//		ss<<sym.getDissMetric()<<" "<<sym.getTheta()<<" "<<sym.getF()<<" "<<sym.getCard()<<" "<<sym.getComp()<<"\n";
//		stream<<ss.str();
//		ss.str(""); //levare
//		stream.close();
//	}
//	else{
//		cout << "Error in opening "<<fullPath<<endl;
//	}

}

template<typename SymbolsContainer>
void GRALG_IO::ReadGranule(const std::string& destinationDir, SymbolsContainer& sym) const{

//	typedef typename SymbolsContainer::value_type granule;
//	typedef typename granule::RepresentantType representant;
//	std::vector<representant> repBatch;
//	typename std::vector<representant>::iterator repBatchIt;
//	std::vector<string> tempLabels;                                 //not used
//	std::vector<std::string> graphsID;
//	std::vector<std::string>::iterator graphsIDit;
//	spare::NaturalType Id;
//	DataSetType readingSet;
//
//	readingSet.readDataset(destinationDir, repBatch, tempLabels, graphsID);
//
//	std::string fileName = destinationDir+"/INFO.txt", s;
//	std::ifstream instream;
//	std::vector<spare::RealType> dissMetricv, thetav, Fv, cardv, compv;
//	spare::RealType dissMetric, theta, F, card, comp;
//
//	instream.open(fileName.c_str(), ios::in|ios::binary);
//	if(instream.is_open()){
//		do
//		{
//		    getline(instream, s);
//		    if(instream.good())
//		    {
//	            istringstream ss(s);
//	            ss >> dissMetric;
//	            ss >> theta;
//	            ss >> F;
//	            ss >> card;
//	            ss >> comp;
//	            dissMetricv.push_back(dissMetric);
//	            thetav.push_back(theta);
//	            Fv.push_back(F);
//	            cardv.push_back(card);
//	            compv.push_back(comp);
//			}else{
//				break;
//			}
//
//		}while(true);
//
//		instream.close();
//	}
//	else{
//		cout << "Error in opening "<<fileName<<endl;
//	}
//
//	vector<granule> aux(graphsID.size());
//	sym = aux;
//	repBatchIt = repBatch.begin();
//	graphsIDit = graphsID.begin();
//	while(repBatchIt != repBatch.end())
//	{
//		granule g;
//		g.getSubstructure() = *repBatchIt;
//		Id = atoi((*graphsIDit).c_str());
//		g.getDissMetric() = dissMetricv[Id];
//		g.getTheta() = thetav[Id];
//		g.getF() = Fv[Id];
//		g.getCard() = cardv[Id];
//		g.getComp() = compv[Id];
//
//		graphsIDit++;
//		repBatchIt++;
//		sym[Id] = g;
//	}
//	cout <<endl;
}


template<typename EmbeddingContainer>
void GRALG_IO::WriteEmbedding(const std::string& destinationDir,const std::string& name, const EmbeddingContainer& embed){

	typename EmbeddingContainer::const_iterator EmbeddingContainerIt = embed.begin();
	typedef typename EmbeddingContainer::value_type EmbeddedSampleType;
	typename EmbeddedSampleType::const_iterator EmbeddedSampleTypeIt;
	std::ofstream stream;
	std::stringstream ss;
	std::string fileName;

	fileName = destinationDir+"/"+name+".txt";
	if(remove(fileName.c_str())!=0)
		//cout <<"Error: file not deleted"<<endl;
	stream.open(fileName.c_str(), std::ios::out);
	if(stream.is_open()){

		while(EmbeddingContainerIt !=embed.end()){
			EmbeddedSampleType e = *EmbeddingContainerIt;
			EmbeddedSampleTypeIt = e.begin();
			while(EmbeddedSampleTypeIt != --e.end()){
				ss<<*EmbeddedSampleTypeIt<<"\t";
				EmbeddedSampleTypeIt++;
			}
			EmbeddingContainerIt++;
			ss<<*EmbeddedSampleTypeIt<<"\n";
		}
		stream<<ss.str();
		stream.close();
	}
	else{
		cout << "Error in opening "<<fileName<<endl;
	}
}

template<typename EmbeddingContainer>
void GRALG_IO::ReadEmbedding(const std::string& sourceDir, EmbeddingContainer& embed){

	typedef typename EmbeddingContainer::value_type EmbeddedSampleType;
	spare::RealType ins;
	std::string s;
	std::ifstream infile;

	infile.open(sourceDir.c_str(), ios::in);

	if(infile.is_open()){

		do{
			getline(infile, s);
			if(infile.good()){
				EmbeddedSampleType e;
				istringstream ss(s);
				while(ss.good()){
					ss >> ins;
					e.push_back(ins);
				}
				embed.push_back(e);
			}
			else{
				break;
			}
		}while(1);

		infile.close();
	}

	else{
		cout <<"Error in opening "<<sourceDir<<endl;
	}

}

template <typename BoostGraphsContainer, typename LabelsContainer, typename IdsContainer>
void GRALG_IO::WriteGraphBatch(const std::string& destinationDir, const BoostGraphsContainer& samples, const LabelsContainer& labels, const IdsContainer& ids) const
{
    typedef typename BoostGraphsContainer::const_iterator SamplesItType;
    typedef typename LabelsContainer::const_iterator LabelsItType;
    typedef typename IdsContainer::const_iterator IdsItType;

    SamplesItType it=samples.begin(), itEnd=samples.end();
    LabelsItType itLabels=labels.begin();
    IdsItType itIds=ids.begin();

    while(it!=itEnd)
    {
        WriteGraph(destinationDir, *it, *itLabels, *itIds);

        it++;
        itLabels++;
        itIds++;
    }
}

template <typename BoostGraphsContainer, typename LabelsContainer>
void GRALG_IO::WriteGraphBatch(const std::string& destinationDir, const BoostGraphsContainer& samples, const LabelsContainer& labels) const
{
    typedef typename BoostGraphsContainer::const_iterator SamplesItType;
    typedef typename LabelsContainer::const_iterator LabelsItType;

    spare::NaturalType id=0;
    SamplesItType it=samples.begin(), itEnd=samples.end();
    LabelsItType itLabels=labels.begin();

    while(it!=itEnd)
    {
        WriteGraph(destinationDir, *it, *itLabels, id);

        it++;
        itLabels++;
        id++;
    }
}


template <typename BoostGraphType, typename LabelType>
void GRALG_IO::WriteGraph(const std::string& fileURI, const BoostGraphType& hConst, const LabelType& label, const spare::NaturalType& id) const
{
    BoostGraphType h = hConst;
    ofstream stream;
    stringstream ss;
    string fileName;
    string fullPath;
    typename boost::graph_traits<ImageGraph>::vertex_descriptor vDesc, vDescSource, vDescTarget;
    typename boost::graph_traits<ImageGraph>::vertex_iterator vIT, vITend;
    typename boost::graph_traits<ImageGraph>::edge_iterator edgeIT, edgeITend;
    typename boost::graph_traits<ImageGraph>::edge_descriptor eDesc;
    //std::cout << "vertici: "<<boost::num_vertices(sample)<<", archi: "<<boost::num_edges(sample)<<endl;
    //typename boost::property_map<BoostGraphType, VertexInfoProp>::type vlabel=boost::get(v_info_t(), sample);
    //typename boost::property_map<BoostGraphType, EdgeInfoProp>::type elabel=boost::get(e_info_t(), sample);

    ss<<"graph_"<<id<<"_"<<label<<".dot";
    fileName = ss.str();
    ss.str("");
    fullPath=fileURI+fileName;


  stream.open(fullPath.c_str(), std::ios::out);
  if(stream.is_open())
  {
      ss<<"graph graph_"<<id<<"_"<<label<< " { \n";//\t graph [classid="<<label<<"]; \n";

      vIT = boost::vertices(h).first;
      vITend = boost::vertices(h).second;
      while(vIT != vITend){
          vDesc = boost::vertex(*vIT++, h);
          ss <<"\t vertex_"<<vDesc<<" [\n";
          VertexLabelType v = boost::get(v_info_t(), h, vDesc);
          ss << "\t\t Xc=" << doubleToStr(v.Xc) <<",\n";
          ss << "\t\t Yc=" << doubleToStr(v.Yc) <<",\n";
          ss << "\t\t Cr=" << doubleToStr(v.CrCb[0]) << ",\n";
          ss << "\t\t Cb=" << doubleToStr(v.CrCb[1]) << ",\n";
          ss << "\t\t Sat=" << doubleToStr(v.Sat) <<",\n";
          ss << "\t\t Brg=" << doubleToStr(v.Brg) <<",\n";
          ss << "\t\t Wlet0=" << doubleToStr(v.Wlet[0]) << ",\n";
          ss << "\t\t Wlet1=" << doubleToStr(v.Wlet[1]) << ",\n";
          ss << "\t\t Wlet2=" << doubleToStr(v.Wlet[2]) << ",\n";
          ss << "\t\t Area=" << doubleToStr(v.Area) <<",\n";
          ss << "\t\t Symm=" << doubleToStr(v.Symm) <<",\n";
          ss << "\t\t Rnd=" << doubleToStr(v.Rnd) <<",\n";
          ss << "\t\t Cmp=" << doubleToStr(v.Cmp) <<",\n";
          ss << "\t\t DirReal=" << doubleToStr(v.Dir.real()) << ",\n";
          ss << "\t\t DirImag="<< doubleToStr(v.Dir.imag()) << ",\n";
          ss << "\t\t OrReal=" << doubleToStr(v.Or.real()) << ",\n";
          ss << "\t\t OrImag="<< doubleToStr(v.Or.imag()) << "\n \t];\n";

          stream<<ss.str();
          ss.str("");

      }



      edgeIT = boost::edges(h).first;
      edgeITend = boost::edges(h).second;
      while(edgeIT != edgeITend){

          eDesc = *edgeIT++;
          vDescSource = boost::source(eDesc,h);
          vDescTarget = boost::target(eDesc,h);
          EdgeLabelType e = boost::get(e_info_t(), h, eDesc);

          ss <<"\t vertex_"<<vDescSource<<" -- vertex_"<<vDescTarget<<"[\n";
          ss << "\t\t dXc=" << doubleToStr(e.dXc) <<",\n";
          ss << "\t\t dYc=" << doubleToStr(e.dYc) <<",\n";
          ss << "\t\t dB=" << doubleToStr(e.dB) <<"\n \t]\n";

          stream<<ss.str();
          ss.str("");
      }


      stream<<"}";
      stream.close();
  }
  else{
      cout << "Error in opening "<<fullPath<<endl;
  }
}


template <typename BoostGraphsContainer, typename IdsContainer>
void GRALG_IO::WriteSubGraphs(const std::string& GdestinationDir, const BoostGraphsContainer& samples, const IdsContainer& ids) const{

	typename BoostGraphsContainer::const_iterator gIT = samples.begin();
	typename IdsContainer::const_iterator idIT = ids.begin();
	spare::NaturalType progressiveID = 0;

	while(gIT != samples.end()){

		WriteGraph(GdestinationDir, *gIT, *idIT, progressiveID);

		progressiveID++;
		idIT++;
		gIT++;
	}

}

template<typename GenCode>
void GRALG_IO::WriteGenCode(const GenCode& code, const std::string& destinationDir) const{

	typename GenCode::const_iterator cIt = code.begin();
	std::ofstream stream;
	std::stringstream ss;
	std::string fileName;

	fileName = destinationDir+"/GeneticCode.txt";
	stream.open(fileName.c_str(), std::ios::out);
	if(stream.is_open()){

		while(cIt != code.end()){
			ss<<*cIt<<" ";
			cIt++;
		}
		stream<<ss.str();
		stream.close();
	}
	else{
		cout << "Error in opening "<<fileName<<endl;
	}
}

template<typename GenCode>
void GRALG_IO::ReadGenCode(GenCode& code, const std::string& sourceDir) const{

	spare::NaturalType gene;
	std::string s;
	std::ifstream infile;

	infile.open(sourceDir.c_str(), ios::in);

	if(infile.is_open()){

		getline(infile, s);

		if(infile.good()){

			istringstream ss(s);

			while(ss.good()){

				ss >> gene;
				code.push_back(gene);

			}

		}

		cout << "Read a genetic code of "<<code.size()<<" elements."<<endl;
		infile.close();
	}

	else{
		cout <<"Error in opening "<<sourceDir<<endl;
	}
}

void GRALG_IO::WriteLog( 	const std::string& name,
										const spare::NaturalType K,
										const spare::RealType time,
										const spare::RealType RR) const{
	std::ofstream stream;
	std::stringstream ss;
	std::string fileName;

	fileName = name+"_log.txt";
	stream.open(fileName.c_str(), std::ios::out | std::ios::app);
	if(stream.is_open()){
		ss<<K<<"\t"<<time<<"\t"<<RR<<"\n";
		stream<<ss.str();
		stream.close();
	}
	else{
		cout << "Error in opening "<<fileName<<endl;
	}
}

template<typename FitnessVect>
void GRALG_IO::WriteLogV( 	const std::string& name,
							const spare::RealType seed,
							const spare::NaturalType K,
							const spare::RealType time,
							const vector<spare::RealType> RRvec,
							const FitnessVect TopFit,
							const vector<spare::NaturalType> alphabetVec,
							const vector<spare::NaturalType> featVec,
							const spare::NaturalType totevol) const{
	std::ofstream stream;
	std::stringstream ss;
	std::string fileName;

	fileName = name+"VERBlog.txt";
    if(remove(fileName.c_str())!=0)
        cout <<"Error: file not deleted"<<endl;
	stream.open(fileName.c_str(), std::ios::out | std::ios::app);
	if(stream.is_open()){
		ss<<"Seed: "<<seed<<"\n"
		  <<"K of KNN: "<<K<<"\n"
		  <<"Total time: "<<time<<"\n"
		  <<"Tot evol: "<<totevol<<"\n";
		ss<<"-----------------------------------------------------\n";
		for(spare::NaturalType i = 0; i < TopFit.size(); i++){
			std::vector<spare::RealType> gencode = TopFit[i].first;
			ss<<"Individual #"<<i<<"\n"
			  <<"bound: "<<gencode[0]<<"\n"
			  <<"ni: "<<gencode[1]<<"\n"
			  <<"phi: "<<gencode[2]<<"\n"
			  <<"t_F: "<<gencode[3]<<"\n"
			  <<"Vinsertion: "<<gencode[4]<<"\n"
			  <<"Vdeletion: "<<gencode[5]<<"\n"
			  <<"Vsubstitution: "<<gencode[6]<<"\n"
			  <<"Einsertion: "<<gencode[7]<<"\n"
			  <<"Edeletion: "<<gencode[8]<<"\n"
			  <<"Esubstitution: "<<gencode[9]<<"\n"
			  <<"wdXc: "<<gencode[10]<<"\n"
			  <<"wdYc: "<<gencode[11]<<"\n"
			  <<"wdB: "<<gencode[12]<<"\n"
			  <<"wXc: "<<gencode[13]<<"\n"
			  <<"wYc: "<<gencode[14]<<"\n"
			  <<"wCrCb: "<<gencode[15]<<"\n"
			  <<"wSat: "<<gencode[16]<<"\n"
			  <<"wBrg: "<<gencode[17]<<"\n"
			  <<"wWlet: "<<gencode[18]<<"\n"
			  <<"wArea: "<<gencode[19]<<"\n"
			  <<"wSymm: "<<gencode[20]<<"\n"
			  <<"wRnd: "<<gencode[21]<<"\n"
			  <<"wCmp: "<<gencode[22]<<"\n"
			  <<"wDir: "<<gencode[23]<<"\n"
			  <<"wOr: "<<gencode[24]<<"\n"
              <<"Alphabet symbols: "<<alphabetVec[i]<<"\n"
			  <<"RR (before feat sel): "<<TopFit[i].second<<"\n"
              <<"Features selected: "<<featVec[i]<<" ("<<(spare::RealType)featVec[i]/(spare::RealType)alphabetVec[i]*100<<"%)\n"
              <<"Recognition Rate: "<<RRvec[i]<<"\n"
              <<"***********************************************************"<<"\n";
		}
		stream<<ss.str();
		stream.close();
	}
	else{
		cout << "Error in opening "<<fileName<<endl;
	}
}

template<typename GraphType>
void GRALG_IO::stampaGrafo(const GraphType& g){
//	typedef typename boost::graph_traits<GraphType>::vertex_iterator vertexIT;
//	typedef typename boost::graph_traits<GraphType>::out_edge_iterator outedgeit;
//	typedef typename boost::graph_traits<GraphType>::vertex_descriptor vDesc;
//	std::pair<outedgeit, outedgeit> Outedge;
//
//	vertexIT vi = boost::vertices(g).first;
//	vertexIT viEnd = boost::vertices(g).second;
//	while(vi != viEnd){
//		vDesc vd1 = boost::vertex(*vi, g);
//		std::vector<spare::RealType> v_info = get(v_info_t(), g, vd1);
//		std::cout << v_info[0] << ": ";
//		Outedge = boost::out_edges(*vi, g);
//		outedgeit ei = Outedge.first;
//		outedgeit eiEnd = Outedge.second;
//		while (ei != eiEnd){
//			vDesc vd2 = boost::target(*ei, g);
//			v_info = get(v_info_t(), g, vd2);
//			std::cout <<"("<< v_info[0] << "), ";
//			ei++;
//		}
//		std::cout << std::endl;
//		vi++;
//	}
}

template<typename ContainerType>
void GRALG_IO::printContainer(const ContainerType& c){

	    typename ContainerType::const_iterator itcc;
	    typedef typename ContainerType::value_type subcontainer;
	    typename subcontainer::const_iterator itc;

	    subcontainer x = *(c.begin());
	    cout << "---------> Printing a container of "<<c.size()<<"x"<<x.size()<<" elements <---------"<<endl;

	    itcc = c.begin();
	    while(itcc != c.end()){
	    	subcontainer sc = *itcc;
	    	itc = sc.begin();
	    	cout<<"(";
	    	while(itc != sc.end()){
	    		cout<<*itc<<",";
	    		itc++;
	    	}
	    	cout<<"),"<<endl;
	    	itcc++;
	    }
}

void GRALG_IO::WriteLabel(const std::string& destinationDir, const std::string& name, const std::vector<std::string>& labels){

	std::vector<std::string>::const_iterator labelsIt = labels.begin();
	std::ofstream stream;
	std::stringstream ss;
	std::string fileName;

	fileName = destinationDir+"/"+name+".txt";
	if(remove(fileName.c_str())!=0)
		//cout <<"Error: file not deleted"<<endl;
	stream.open(fileName.c_str(), std::ios::out);
	if(stream.is_open()){
		while(labelsIt !=labels.end()){
			ss<<*labelsIt++<<endl;
		}
		stream<<ss.str();
		stream.close();
	}
	else{
		cout << "Error in opening "<<fileName<<endl;
	}
}

string GRALG_IO::doubleToStr(const spare::RealType d) const
{
    stringstream ss;
    ss << fixed << setprecision(5) << d;
    return ss.str();

}


#endif /* GRALG_IO_HPP_ */

/** @brief File GRALG1.hpp, that contains the implementation of the strategy 1 of GRALG algorithm.
 * This class implements the version 1 of GRALG algorithm.
 *
 * @file GRALG1.hpp
 * @author Filippo Bianchi
 */

#ifndef GRALG1_HPP_
#define GRALG1_HPP_

//STD INCLUDES
#include <vector>

//SPARE INCLUDES
#include <spare/SpareTypes.hpp>

#include "GRALG_IO.hpp"

using namespace std;

/** @brief This class implements the version 1 of GRALG algorithm.
 *
 *  A set of graphs is converted in real-type vectors by the following operations
 * - Extracting all the subgraphs from the input Training Set up to an order r.
 * - Finding all the possibile cluster partitions of the just found set of subgraphs.
 * - Synthetizing an alphabet of symbols, using the representants of the most compact and populated clusters.
 * - Generating an embedding of Training Set using the alphabet.
 *
 * A final classification is done embedding the Test Set (using the same procedure and parameters of the Training Set case),
 * and classify it using a classifier trained with the embedded Training Set.
 *
 * <b>Template Parameters</b>
 *  <table class="contents">
 *  <tr>
 *      <td class="indexkey"><b>Name</b></td>
 *      <td class="indexkey"><b>Description</b></td>
 *  </tr>
 *  <tr>
 *  	<td class="indexvalue">ExtractionStrategy</td>
 *  	<td class="indexvalue"> A class which implements a subgraphs extractor.</td>
 *  </tr>
 *  <tr>
 *  	<td class="indexvalue">GranulationStrategy</td>
 *  	<td class="indexvalue">A class which implements a granulator that extract an alphabet of symbols from a set of subgraphs.</td>
 *  </tr>
 *  <tr>
 *  	<td class="indexvalue">SymbolsContainer</td>
 *  	<td class="indexvalue">A class which define the type of the container of the symbols of the alphabet.</td>
 *  </tr>
 *  <tr>
 *  	<td class="indexvalue">EmbeddingStrategy</td>
 *  	<td class="indexvalue">A class which implements an embedderwhich transform a graph in a real-typed vector.</td>
 *  </tr>
 *  <tr>
 *  	<td class="indexvalue">ClassificationStrategy</td>
 *  	<td class="indexvalue">A class which implements a classifier</td>
 *  </tr>
 *  </table>
 *
 *  <b>Parameters Summary</b>
 *  <table class="contents">
 *  <tr>
 *  	<td class="indexkey"><b>Name</b></td>
 *  	<td class="indexkey"><b>Domain</b></td>
 *  	<td class="indexkey"><b>Description</b></td>
 *  	<td class="indexkey"><b>Default</b></td>
 *  </tr>
 *  <tr>
 *  	<td class="indexvalue">extractionRequired</td>
 *  	<td class="indexvalue">[true, false]</td>
 *  	<td class="indexvalue">Flag indicating if extraction is required or not</td>
 *  	<td class="indexvalue">true</td>
 *  </tr>
 *  </table>
 *
 */
template<class ExtractionStrategy, class GranulationStrategy, class EmbeddingStrategy, class ClassificationStrategy, class SymbolsContainer>
class GRALG1{

public:

	//CONSTRUCTOR
	/**
	 * Default constructor.
	 * Automatically sets the flag for doing graphs extraction to 1.
	 */
	GRALG1(){
		extractionRequired = true;
	}

	//DEF
	/**
	 * Type of the ebmedder used.
	 */
	typedef typename GranulationStrategy::EnsembleStrategy EnsembleStrategy;

	//ACCESS
	/**
	 * Read/Write access to extraction agent.
	 */
	ExtractionStrategy& ExtractionAgent(){
		return extractionAgent;
	}

	/**
	 * Read-only access to extraction agent.
	 */
	const ExtractionStrategy& ExtractionAgent() const{
		return extractionAgent;
	}

	/**
	 * Read/Write access to granulation agent.
	 */
	GranulationStrategy& GranulationAgent(){
		return granulationAgent;
	}

	/**
	 * Read-only access to granulation agent.
	 */
	const GranulationStrategy& GranulationAgent() const{
		return granulationAgent;
	}

	/**
	 * Read/Write access to ensemble agent.
	 */
	EnsembleStrategy& EnsembleAgent(){
		return granulationAgent.EnsembleAgent();
	}

	/**
	 * Read-only access to ensemble agent.
	 */
	const EnsembleStrategy& EnsembleAgent() const{
		return granulationAgent.EnsembleAgent();
	}

	/**
	 * Read/Write access to embedding agent.
	 */
	EmbeddingStrategy& EmbeddingAgent(){
		return embeddingAgent;
	}

	/**
	 * Read-only access to embedding agent.
	 */
	const EmbeddingStrategy& EmbeddingAgent() const{
		return embeddingAgent;
	}

	/**
	 * Read/Write access to classification agent.
	 */
	ClassificationStrategy& ClassificationAgent(){
		return classificationAgent;
	}

	/**
	 * Read-only access to classification agent.
	 */
	const ClassificationStrategy& ClassificationAgent() const{
		return classificationAgent;
	}

    /**
     * Read/Write access to flag for subgraphs extraction.
     * - If set to 1 subgraphs are extracted from graphs of the input set (select this option if yuo pass graphs to the Process function).
     * - If set to 0 extraction is not performed (select this option if you pass subgraphs to the Process function).
     */
    bool& ExtractionRequired(){
    	return extractionRequired;
    }

    /**
     * Read-only access to flag for subgraphs extraction.
     */
    const bool& ExtractionRequired() const{
    	return extractionRequired;
    }

	/**
	 * R/W access to the flag for normalize the embedding
	 */
	bool& NormEmbedding(){
		return normEmbedding;
	}

	/**
	 * R-Only access to the flag for normalize the embedding
	 */
	const bool& NormEmbedding() const{
		return normEmbedding;
	}

    /**
     * Read/Write access to the ID of the subgraphs of the Training Set.
     * If subgraphs extraction is performed outside GRALG1, this method is used to set the ID of the subgraphs which are passed to Process method
     */
    std::vector<spare::NaturalType>& GetIDTR(){
     	return IDTR;
     }

     /**
      * Read-only access to the ID of the subgraphs of the Training Set.
      * If subgraphs extraction is performed inside GRALG1, this method is used for read the ID  of the subgraphs from outside.
      */
     const std::vector<spare::NaturalType>& GetIDTR() const{
 		return IDTR;
 	}


    /**
     * Read/Write access to the ID of the subgraphs of the Test Set.
	 * If subgraphs extraction is performed outside GRALG1, this method is used to set the ID of the subgraphs which are passed to Process method
	 */
	std::vector<spare::NaturalType>& GetIDTS(){
		return IDTS;
	}

	/**
     * Read-only access to the ID of the subgraphs of the Test Set.
     * If subgraphs extraction is performed inside GRALG1, this method is used for read the ID  of the subgraphs from outside.
     */
	const std::vector<spare::NaturalType>& GetIDTS() const{
		return IDTS;
	}

	//MEMBER FUNCTIONS
	/**
	 * Takes as input two set of structures which can represent either the original graphs (in this case subgraphs extraction is required) or the subgraphs.
	 * The first set is used for extracting the alphabet of symbols and doing the embedding.
	 * After that, an ambedding of the second set is realized, using the alphabet extracted before. Finally a classifier, trained with the embedding coming from the first set and its labels,
	 * try to classify the second set. The assigned labels are compared with the labels of the second set, and the percentage of errors is returned.
	 * @param[in] trGraphs This is the set of structures used for extracting the alphabet and for training the classifier. It usually coincide with the Training set.
	 * @param[in] trlabels Labels associated to trGraphs (in case those are graphs) or to the graphs from wich the subgraph has been extracted (in case trGraphs are subgraphs).
	 * @param[in] tsGraphs This is the set of structures using for validating the model created with trGraphs. It usually concide with the Validation Set (training phase) or Test Set (test phase).
	 * @param[in] tslabels Labels associated to tsGraphs (in case those are graphs) or to the graphs from wich the subgraph has been extracted (in case tsGraphs are subgraphs).
	 */
	template<typename ContainerType, typename LabelContainer>
	spare::RealType Process(const ContainerType& trGraphs, const LabelContainer& trLabels, const LabelContainer& ExptrLabels, const ContainerType& tsGraphs, const LabelContainer& tsLabels, const LabelContainer& Labels);

private:

	/**
	 * Extraction agent.
	 */
	ExtractionStrategy extractionAgent;

	/**
	 * Granulation agent.
	 */
	GranulationStrategy granulationAgent;

	/**
	 * Embedding agent.
	 */
	EmbeddingStrategy embeddingAgent;

	/**
	 * Classification agent.
	 */
	ClassificationStrategy classificationAgent;

	/**
	 * A flag indicating if subgraphs need to be extracted.
	 */
	bool extractionRequired;

	/**
	 * Flag for doing a component-wise normalization of the embedding
	 */
	bool normEmbedding;

	/**
	 * ID of the subgraphs (used when already extracted subgraphs are passed to the Process method)
	 */
	std::vector<spare::NaturalType> IDTR, IDTS;
};


// ********************************* IMPLEMENTATION *******************************************
template<class ExtractionStrategy, class GranulationStrategy, class EmbeddingStrategy, class ClassificationStrategy, class SymbolsContainer>
template<typename ContainerType, typename LabelContainer>
spare::RealType GRALG1<ExtractionStrategy, GranulationStrategy, EmbeddingStrategy, ClassificationStrategy, SymbolsContainer>::
Process(const ContainerType& trGraphs, const LabelContainer& trLabels, const LabelContainer& ExptrLabels, const ContainerType& tsGraphs, const LabelContainer& tsLabels, const LabelContainer& Labels){

	//init
	SymbolsContainer trSymbols;
	std::vector<std::vector<spare::RealType> > vvTR, vvTS;
	spare::NaturalType ErrorCount=0;
	spare::RealType ErrorCost;
	ContainerType ExptrGraphs, ExptsGraphs;

	//granulation
	if(extractionRequired){
		//extract all subgraphs from the input graph containers (remember to set maxOrder in extractionAgent when calling GRALG1)
		extractionAgent.ExtractBatch(trGraphs, ExptrGraphs);
		IDTR = extractionAgent.getIDcontainer();
		extractionAgent.ExtractBatch(tsGraphs, ExptsGraphs);
		IDTS = extractionAgent.getIDcontainer();
		granulationAgent.Granulate(ExptrGraphs, ExptrLabels, trSymbols, Labels);
	}else{
		if(IDTR.empty() || IDTS.empty())
			std::cerr<<"ID of extracted subgraphs not initialized";
		else{
			ExptrGraphs = trGraphs;
			ExptsGraphs = tsGraphs;
			granulationAgent.Granulate(ExptrGraphs, ExptrLabels, trSymbols, Labels);
		}
	}

	//cout<<"symbols extracted: " <<trSymbols.size() <<endl;

	//tr embedding
	embeddingAgent.SubstructuresIds() = IDTR;
	embeddingAgent.NumberOfStruct() = trLabels.size();
	embeddingAgent.getVectors(ExptrGraphs, trSymbols, vvTR);
//	GRALG_IO gio;
//	gio.printContainer(vvTR);

	//ts embedding
	embeddingAgent.SubstructuresIds() = IDTS;
	embeddingAgent.NumberOfStruct() = tsLabels.size();
	embeddingAgent.getVectors(ExptsGraphs, trSymbols, vvTS);

	//embedding component-wise normalization
	if(normEmbedding){
		for(spare::NaturalType i=0; i<vvTR[0].size(); i++){
			spare::RealType maxi=0;
			for(spare::NaturalType j=0; j<vvTR.size();j++){
				if(vvTR[j][i]>maxi)
					maxi=vvTR[j][i];
			}
			for(spare::NaturalType j=0; j<vvTR.size();j++){
				vvTR[j][i]=vvTR[j][i]/maxi;
			}
		}

		for(spare::NaturalType i=0; i<vvTS[0].size(); i++){
			spare::RealType maxi=0;
			for(spare::NaturalType j=0; j<vvTS.size();j++){
				if(vvTS[j][i]>maxi)
					maxi=vvTS[j][i];
			}
			for(spare::NaturalType j=0; j<vvTS.size();j++){
				vvTS[j][i]=vvTS[j][i]/maxi;
			}
		}
	}

	//Classification stage - training
	classificationAgent.Incremental()="Off";
	classificationAgent.Learn(vvTR.begin(), vvTR.end(), trLabels.begin());

	//Classification stage - test
	LabelContainer labelsOutput(tsLabels.size(), "UNDETERMINED");
	classificationAgent.Process(vvTS.begin(), vvTS.end(), labelsOutput.begin());

	typename LabelContainer::const_iterator tsLabelsIt = tsLabels.begin();
	typename LabelContainer::const_iterator labelsOutputIt = labelsOutput.begin();
	while(tsLabelsIt != tsLabels.end()){
		if(*tsLabelsIt != *labelsOutputIt){
			ErrorCount++;
		}
		tsLabelsIt++;
		labelsOutputIt++;
	}
	ErrorCost= ErrorCount / spare::RealType(tsLabels.size());

	return 1. - ErrorCost;

}

#endif /* GRALG1_HPP_ */

/** @brief File subGraphStruct.hpp, that contains the definition of the data struct implementing the subgraph representation.
 *
 * @file subGraphStruct.hpp
 * @author Filippo Bianchi
 */

#ifndef SUBGRAPHSTRUCT_HPP_
#define SUBGRAPHSTRUCT_HPP_

//STL
#include <list>
#include <vector>

//SPARE
#include <spare/SpareTypes.hpp>


/**
 * @brief Data struct implementing the subgraph representation.
 */
template<class GraphType>
struct subGraphStruct{

	/**
	 * The graph object representing the subgraph.
	 */
	GraphType subgraph;

	/**
	 * List of ID of the nodes associated to the original graph.
	 */
	std::list<std::pair<spare::NaturalType, spare::NaturalType> > vertices;

	/**
	 * Array of boolean which represent the a subgraph: 1 the node/edge is present, 0 is absent.
	 */
	std::vector<bool> presence;
};



#endif /* SUBGRAPHSTRUCT_HPP_ */

/** @brief File Extractor1.hpp, that contains the implementation of subgraphs extractor for the strategy 1 of GRALG algorithm.
 *
 * @file Extractor1.hpp
 * @author Filippo Bianchi
 */

#ifndef EXTRACTOR1_HPP_
#define EXTRACTOR1_HPP_

//STD
#include <vector>
#include <list>

//SPARE
#include <spare/SpareTypes.hpp>

///BOOST
#include <boost/graph/graph_utility.hpp>

using namespace spare;

/**
 * @brief Implements subgraphs extractor for the strategy 1 of GRALG algorithm.
 *
 * Class implementing the search of all the subgraphs of a graph from order 1 to
 * maxOder, where the last is an arbitrary settable parameter.<b>Template Parameters</b>
 *  <table class="contents">
 *  <tr>
 *      <td class="indexkey"><b>Name</b></td>
 *      <td class="indexkey"><b>Description</b></td>
 *  </tr>
 *  <tr>
 *  	<td class="indexvalue">Substructure</td>
 *  	<td class="indexvalue"> A class which contains the definition of the struct representing a subgraph.</td>
 *  </tr>
 *  </table>
 *
 *  <b>Parameters Summary</b>
 *  <table class="contents">
 *  <tr>
 *  	<td class="indexkey"><b>Name</b></td>
 *  	<td class="indexkey"><b>Domain</b></td>
 *  	<td class="indexkey"><b>Description</b></td>
 *  	<td class="indexkey"><b>Default</b></td>
 *  </tr>
 *  <tr>
 *  	<td class="indexvalue">maxOrder</td>
 *  	<td class="indexvalue">[1, inf]</td>
 *  	<td class="indexvalue">The maximum order of subgraph that will be extracted</td>
 *  	<td class="indexvalue">1</td>
 *  </tr>
 *  </table>
 */
template <class Substructure>
class Extractor1
{
public:

	/**
	 * Default constructor which sets maxOrder to 1 by default
	 */
	Extractor1(){
		gID = 0;
		maxOrder = 1;
	}

    /**
     * Extract all the subgraps of size from 1 to maxOrder from a container of graphs
     * @param[in] samples container of the subgraphs
     * @param[out] structures container of all the extracted subgraphs
     */
    template <typename SamplesContainer, typename StructuresContainer>
    void ExtractBatch(const SamplesContainer& samples, StructuresContainer& structures);


    /**
     * Extract all the subgraphs of a graph from order 1 to maxOrder
     * @param[in] sample graph from which the subgraphs will be extracted
     * @param[out] structures container that will be filled with the extracted subgraphs
     */
    template <typename SampleType, typename StructuresContainer>
    void Extract(const SampleType& sample, StructuresContainer& structures);

    /**
     * type of the container of the not-expanded structure's ID
     */
    typedef std::vector<spare::NaturalType> IDcontainerType;

    /**
     * R/W access to max order of subgraphs
     */
    spare::NaturalType& MaxOrder(){ return maxOrder; }

    /**
     * R-only access to max order of subgraphs
     */
    const spare::NaturalType& MaxOrder() const { return maxOrder; }

    /**
     * R-Only access to the container of os subgraphs ID
     */
    const IDcontainerType& getIDcontainer() const{
        	return IDcont;
    }

private:

    /*
     * private function which assign a numeric label to each edge.
     * @param[in] edged descriptor of the edge (according to boost adjacency lists)
     * @param[in] g graph to which the edge belongs
     * @param[in] n max number of nodes in the graph
     * @return unique numeric identifier for the edge described by edge descriptor
     */
    template<typename edgeDesc, typename GraphType>
    inline spare::NaturalType getEdgeID(edgeDesc& edged, GraphType& g, spare::NaturalType n) const;

    /**
     * max order of the subgraphs that are to be extracted from the graph
     */
    spare::NaturalType maxOrder;

    /**
     * container of not-expanded structure's ID
     */
    IDcontainerType IDcont;

    /**
     * ID of the structure currently exctract
     */
    spare::NaturalType gID;
};


//IMPL


template <class Substructure>
template<typename edgeDesc, typename GraphType>
inline spare::NaturalType Extractor1<Substructure>::getEdgeID(edgeDesc& edged, GraphType& g, spare::NaturalType n) const
{
    spare::NaturalType s = boost::source(edged, g);
    spare::NaturalType t = boost::target(edged, g);
    spare::NaturalType res = (s > t)? s*n+t : t*n+s;
    return res;
}


template <class Substructure>
template <typename SampleType, typename StructuresContainer>
void Extractor1<Substructure>::Extract(const SampleType& sample, StructuresContainer& structures)
{
    typedef SampleType GraphType;
    typedef typename boost::graph_traits<GraphType>::vertex_descriptor vertexDesc;
    typedef typename boost::graph_traits<GraphType>::edge_descriptor edgeDesc;
    typedef typename std::pair<edgeDesc, bool> edgePair;
    typedef typename boost::graph_traits<GraphType>::vertex_iterator vertexIT;
    typedef typename boost::graph_traits<GraphType>::adjacency_iterator AdjacencyIt;
    typedef Substructure subGraph;
    typedef std::list<subGraph> subGraphList;

    typename subGraphList::iterator SLit, SLend;
    typename std::list<std::pair <spare::NaturalType, spare::NaturalType> >::iterator subGvIT, subGvEnd, subGvIT2, subGvITij;
    vertexDesc source_g, source_sub, neighbour_g, neighbour_sub, neighbour_gij, neighbour_subij;

    spare::NaturalType numV = boost::num_vertices(sample);
    spare::NaturalType numE = numV*(numV+1); //da ottimizzare: massimo numero di archi presenti in un grafo con numV nodi è n(n-1)/2
    vertexIT vi=boost::vertices(sample).first;
    vertexIT viEnd=boost::vertices(sample).second;

    subGraphList SL;
    boost::unordered_set<std::vector<bool> > uset;

    //STEP 1
    //extract subgraph of order 1
    while(vi != viEnd)
    {
        vertexDesc vig = boost::vertex(*vi, sample);

        //create new subgraph data structure
        subGraph s;

        //create a new subgraph
        GraphType gnew = GraphType(0);
        vertexDesc vignew = boost::add_vertex(boost::get(v_info_t(), sample, vig), gnew);
        s.subgraph = gnew;

        //initialization of the list of the nodes of the subgraph
        std::pair<spare::NaturalType, spare::NaturalType> newVertexPair(vig, vignew);
        s.vertices.push_back(newVertexPair);

        //creating list of boolean vector, describing the presence of nodes and edges of the graph in the subgraph
        std::vector<bool> newPresence(numV+numE, 0);
        newPresence[vig] = 1;
        s.presence = newPresence;

        //update resulting set and hashset
        structures.insert(structures.end(), s.subgraph);
        //inserisco il vettore booleano in un hashset
        uset.insert(s.presence);
        //assign source graph ID to the new generated subgraph
        IDcont.insert(IDcont.end(), gID);

        SL.push_back(s);

        vi++;
    }


    //STEP 2
    //extracting subgraphs from order 2 to maxOrder
    for(spare::NaturalType Ord = 2; Ord <= maxOrder; Ord++)
    {
        subGraphList SLnew;

        //process the list of subgraphs. at each iteration this list is substitued with the list of the new augmented subgraphs
        SLit = SL.begin();
        SLend = SL.end();
        while(SLit != SLend)
        {
            //processing a subgraph
            subGraph s = *SLit;
            subGvIT = s.vertices.begin();
            subGvEnd = s.vertices.end();

            //analyzing all the node of the subgraph
            while(subGvIT != subGvEnd)
            {
                std::pair<spare::NaturalType, spare::NaturalType> vPair_i = *subGvIT;
                source_g = boost::vertex(vPair_i.first, sample);
                source_sub = boost::vertex(vPair_i.second, s.subgraph);

                //analyzing all the neighbours of the node source_g
                AdjacencyIt ni = boost::adjacent_vertices(source_g, sample).first;
                AdjacencyIt niEnd = boost::adjacent_vertices(source_g, sample).second;

                while(ni != niEnd){

					neighbour_g = boost::vertex(*ni, sample);

					//node already in subgraph?
					if(s.presence[neighbour_g]){//yes
						//do nothing
					}

					//node not in subgraph
					else
					{
						subGraph sNew = s;
						sNew.presence[neighbour_g] = 1;

						//retrieve the edge
						edgePair edgeP_g = edge(source_g, neighbour_g, sample);
						edgeDesc edged = edgeP_g.first;
						spare::NaturalType edge_g = getEdgeID(edged, sample, numV);
						sNew.presence[numV + edge_g] = 1;

						//node and edge in other subgraphs?
						if(uset.find(sNew.presence) != uset.end()){
							//do nothing
						}

						else
						{
							//add node and edge
							neighbour_sub = boost::add_vertex(boost::get(v_info_t(), sample, neighbour_g), sNew.subgraph);
							boost::add_edge(source_sub, neighbour_sub, boost::get(e_info_t(), sample, edged), sNew.subgraph);

							//update struct
							std::pair<spare::NaturalType, spare::NaturalType> n_Pair_i(neighbour_g, neighbour_sub);
							sNew.vertices.push_back(n_Pair_i);

							//update resulting set and hashset
							structures.push_back(sNew.subgraph);
							//assign source graph ID to the new generated subgraph
							IDcont.insert(IDcont.end(), gID);
							uset.insert(sNew.presence);
							 //insert the new subgraph in the new list
							SLnew.push_back(sNew);
//****************************
							//check all neighbours of neighbour_g
							AdjacencyIt ninj = boost::adjacent_vertices(neighbour_g, sample).first;
							AdjacencyIt ninjEnd = boost::adjacent_vertices(neighbour_g, sample).second;
							while(ninj != ninjEnd){

								neighbour_gij = boost::vertex(*ninj, sample);

								//node already in subgraph?
								if(sNew.presence[neighbour_gij]){

									edgePair edgeP_gij = boost::edge(neighbour_g, neighbour_gij, sample);
									edgeDesc edgedij = edgeP_gij.first;
									spare::NaturalType edge_gij = getEdgeID(edgedij, sample, numV);

									//edge already present in subgraph?
									if(sNew.presence[numV + edge_gij]){
										//do nothing
									}

									else{ //add the edge
										subGraph sNewij = sNew;

										//search node ID among nodes of the subgraph itself
										subGvITij = sNewij.vertices.begin();
										while((*subGvITij).first != neighbour_gij){
											subGvITij++;
										}
										neighbour_subij = (*subGvITij).second;

										sNewij.presence[numV + edge_gij] = 1;

										//subgraph already exist?
										if(uset.find(sNewij.presence) != uset.end()){
											//do nothing
										}
										else{
											boost::add_edge(neighbour_sub, neighbour_subij, boost::get(e_info_t(), sample, edgedij), sNewij.subgraph);


											//update resulting set and hashset
											structures.insert(structures.end(), sNewij.subgraph);
											//assign source graph ID to the new generated subgraph
											IDcont.insert(IDcont.end(), gID);
											uset.insert(sNewij.presence);
											 //insert the new subgraph in the new list
											SLnew.push_back(sNew);
											sNew = sNewij;
										}
									}
								}

								ninj++;
							}
//*****************************************
						}

					}

					//move to the next neighbour of the node
					ni++;
				}

                //move to the next node of the subgraph
                subGvIT++;
            }

            //move to the next subgraph
            SLit++;
        }

        //erase old list of subraph of order n and replace it with the newly generated list of subraph of order n+1
        SL = SLnew;

    }//end step 2
}

template <class Substructure>
template <typename SamplesContainer, typename StructuresContainer>
void Extractor1<Substructure>::ExtractBatch(const SamplesContainer& samples, StructuresContainer& structures)
{
    typedef typename SamplesContainer::const_iterator GraphIT;
    typedef typename SamplesContainer::value_type GraphType;

    GraphIT samplesIT = samples.begin();
    IDcont.clear();

    //main cycle
    while(samplesIT != samples.end())
    {
         GraphType g = *samplesIT;

         //extract subgraphs for g
         Extract(g, structures);

         //move to the next graph
         samplesIT++;
         gID++;

         //debug
         //std::cout <<"Total subgraphs extracted so far: "<< structures.size()<<std::endl;

    }

    gID = 0;
}

#endif /* EXTRACTOR1_HPP_ */

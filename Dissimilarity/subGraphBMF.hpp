/** @brief File subGraphBMF.hpp, that contains a wrapper class for using a graph distance with subgraph structs.
 *
 * This class implements a version of BMF graph distance for subgraph structs used in version 2 of GRALG algorithm.
 *
 * @file subGraphBMF.hpp
 * @author Filippo Bianchi
 */

#ifndef SUBGRAPHBMF_HPP_
#define SUBGRAPHBMF_HPP_

//SPARE INCLUDES
#include <spare/SpareTypes.hpp>
#include <spare/Graph/Dissimilarity/BMF.hpp>

/**
 * @brief a wrapper class for using a graph distance with subgraph structs.
 *
 * 	<b>Template Parameters</b>
 *  <table class="contents">
 *  <tr>
 *      <td class="indexkey"><b>Name</b></td>
 *      <td class="indexkey"><b>Description</b></td>
 *  </tr>
 *  <tr>
 *  	<td class="indexvalue">BMFType</td>
 *  	<td class="indexvalue"> A class which implements a graph dissimilarity distance (e.g. BMF class in Spare).</td>
 *  </tr>
 *  </table>
 */
template<class BMFType>
class subGraphBMF{
public:

	//TYPE DEF
	/**
	 * Vertices dissimilarity type.
	 */
	typedef typename BMFType::VerticesDissType VerticesDiss;

	/**
	 * Edges dissimilarity type.
	 */
	typedef typename BMFType::EdgesDissType EdgesDiss;

	//MEMBER FUNCTION
	/**
	 * Evaluate the dissimilarity between two structures.
	 * @param[in] g1 first structure.
	 * @param[in] g2 second structure.
	 * @return the value of the distance.
	 */
	template<typename subGraph >
	inline RealType Diss(const subGraph& g1, const subGraph& g2) const{
		return mBMFDiss.Diss(g1.subgraph, g2.subgraph);
	}

	//ACCESS

	/**
	 * Read/Write access to the vertices dissimilarity agent.
	 */
	VerticesDiss& VerticesDissAgent(){
		return mBMFDiss.VerticesDissAgent();
	}

	/**
	 * Read-only access to the vertices dissimilarity agent.
	 */
	const VerticesDiss& VerticesDissAgent() const{
		return mBMFDiss.VerticesDissAgent();
	}

    /**
     * Read/Write access to the edges dissimilarity agent.
     */
	EdgesDiss& EdgesDissAgent(){
		return mBMFDiss.EdgesDissAgent();
	}

    /**
     * Read-only access to the edges dissimilarity agent.
     */
	const EdgesDiss& EdgesDissAgent() const{
		return mBMFDiss.EdgesDissAgent();
	}


    /**
     * Read/Write access to the value used for the normalization (default 1)
     */
	RealType& NormValue(){
		return mBMFDiss.NormValue();
	}

    /**
     * Read-only access to the normalization flag
     */
	const RealType& NormValue() const{
		return mBMFDiss.NormValue();
	}

    /**
     * Read/Write access to the normalization flag
     */
	bool& Normalize(){
		return mBMFDiss.Normalize();
	}

    /**
     * Read-only access to the normalization flag
     */
	const bool& Normalize() const{
		return mBMFDiss.Normalize();
	}

	/**
	 * Read/Write access to the parameter for vertices substitutions weight.
	 */
	spare::RealType& PVertexSubstitution() { return mBMFDiss.PVertexSubstitution(); }

	/**
	 * Read-only access to the parameter for vertices substitutions weight.
	 */
	const spare::RealType& PVertexSubstitution() const { return mBMFDiss.PVertexSubstitution(); }

	/**
	 * Read/Write access to the parameter for vertices insertion weight.
	 */
	spare::RealType& PVertexInsertion() { return mBMFDiss.PVertexInsertion(); }

	/**
	 * Read-only access to the parameter for vertices insertion weight.
	 */
	const spare::RealType& PVertexInsertion() const { return mBMFDiss.PVertexInsertion(); }

	/**
	 * Read/Write access to the parameter for vertices deletion weight.
	 */
	spare::RealType& PVertexDeletion() { return mBMFDiss.PVertexDeletion(); }

	/**
	 * Read-only access to the parameter for vertices deletion weight.
	 */
	const spare::RealType& PVertexDeletion() const { return mBMFDiss.PVertexDeletion(); }

	/**
	 * Read/Write access to the parameter for edges substitutions weight.
	 */
	spare::RealType& PEdgeSubstitution() { return mBMFDiss.PEdgeSubstitution(); }

	/**
	 * Read-only access to the parameter for vertices substitutions weight.
	 */
	const spare::RealType& PEdgeSubstitution() const { return mBMFDiss.PEdgeSubstitution(); }

	/**
	 * Read/Write access to the parameter for edges insertion weight.
	 */
	spare::RealType& PEdgeInsertion() { return mBMFDiss.PEdgeInsertion(); }

	/**
	 * Read-only access to the parameter for vertices insertion weight.
	 */
	const spare::RealType& PEdgeInsertion() const { return mBMFDiss.PEdgeInsertion(); }

	/**
	 * Read/Write access to the parameter for vertices deletion weight.
	 */
	spare::RealType& PEdgeDeletion() { return mBMFDiss.PEdgeDeletion(); }

	/**
	 * Read-only access to the parameter for vertices deletion weight.
	 */
	const spare::RealType& PEdgeDeletion() const { return mBMFDiss.PEdgeDeletion(); }


	/**
	 * Read/Write access to the alpha parameter
	 */
	spare::RealType& PAlpha() { return mBMFDiss.PAlpha(); }

	/**
	 * Read-only access to the alpha parameter
	 */
	const spare::RealType& PAlpha() const { return mBMFDiss.PAlpha(); }

	/**
	 * Read/Write access to the beta parameter
	 */
	spare::RealType& PBeta() { return mBMFDiss.PBeta(); }

	/**
	 * Read-only access to the beta parameter
	 */
	const spare::RealType& PBeta() const { return mBMFDiss.PBeta(); }

	/**
	 * Read/Write access to the gamma parameter
	 */
	spare::RealType& PGamma() { return mBMFDiss.PGamma(); }

	/**
	 * Read-only access to the gamma parameter
	 */
	const spare::RealType& PGamma() const { return mBMFDiss.PGamma(); }


private:

	 /**
	 * Standard BMF algorithm
	 */
	BMFType mBMFDiss;

};




#endif /* SUBGRAPHBMF_HPP_ */

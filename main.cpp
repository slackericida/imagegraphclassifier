/** @brief File main.cpp, that contains the main main file of GRALG project.
 *
 * This is the main file of GRALG projet, which include all the needed classes and shows how they must
 * be configured. This file can be compiled with different setup, which differs for the preprocessor
 * directives specified.
 *
 * The program can be run with different options, using the following arguments
 * -1 arg   sets training set path
 * -2 arg   sets test set path
 * -3 arg	sets validation set path
 * -r arg   sets maximum sugraphs order to be extracted
 * -l arg 	sets theta lower bound in logaritmic search in creating partition ensembles
 * -u arg	sets theta upper bound in logaritmic search in creating partition ensembles
 * -t arg   sets the resolution of theta in logaritmic search in creating partition ensembles
 * -e arg	set tolerance in symbol's matching during embedding phase
 * -s arg	set seed value for genetic algotirhms and BSAS
 * -i arg   set the numeber of evolutions of genetic algorithms for parameters optimization
 * -n arg   set the number of evolutions of gen alg for features selection (only ver. 1)
 * -a arg 	set the weight alpha used in feature selection fitness function (only ver. 1)
 * -k arg 	set k of KNN
 * -o arg 	set t_RR (fitness threshold in parameters optimization)
 * -f arg	set t_FS (fitness threshold in feature selection optimization - only ver. 1)
 * -x arg 	set value of reduction step (stop criterion)
 * -y arg	set value of increasing step (stop criterion)
 * -V arg   prints out Versions
 * -h       prints out usage information, the program is not run
 *
 *
 * @file main.cpp
 * @author Filippo Bianchi
 */



// ************** INCLUDES **************

//STD INCLUDES
#include <getopt.h>
#include <cstdlib>
#include <iostream>

//SPARE INCLUDES
//graph and reader types
#include "IMG/ImageGraph.hpp"
#include "IMG/ImageGraphEdgeDiss.hpp"
#include "IMG/ImageGraphVertexDiss.hpp"
#include "IMG/ImageGraphReader.hpp"
#include <spare/Graph/Graph/GraphsDataset.hpp>
//dis(similarities)
#include <spare/Dissimilarity/Euclidean.hpp>
#include <spare/Graph/Dissimilarity/BMF.hpp>
//clustering and classification
#include <spare/Clustering/Bsas.hpp>
#include <spare/Clustering/Ensembler/BsasPartitions.hpp>
#include <spare/Representative/MinSod.hpp>
#include <spare/SpareTypes.hpp>
#include <spare/Supervised/KnnClass.hpp>
//genetic optimizer
#include <spare/Environment/DiscreteCode.hpp>
#include <spare/Optimizer/Genetic.hpp>
//representation
#include <spare/Representation/SymbolicHistograms.hpp>

//LOCAL
#include "GRALG1.hpp"
#include "Extractor/Extractor1.hpp"
#include "Extractor/subGraphStruct.hpp"
#include "Granulation/Granulator1.hpp"
#include "Genetic/mtG1FitFun1.hpp"
#include "Genetic/mtG1FitFun2.hpp"
#include "FS_and_Test/Test1.hpp"
#include "FS_and_Test/Projection.hpp"
#include "GRALG_IO.hpp"
#include "Granulation/Granule.hpp"

using namespace spare;
using namespace std;

// ************** DEFINITIONS **************

//graph and reader type
typedef ImageGraph GraphType;
typedef ImageGraphReader GraphReaderType;
typedef ImageGraphVertexDissimilarity VerticesDissimilarity;
typedef ImageGraphEdgeDiss EdgesDissimilarity;

//graphs datastructures
typedef GraphsDataset<GraphReaderType, GraphType> DatasetType;
typedef vector<GraphType> GraphsBatch;
typedef GraphsBatch::iterator GraphsBatchIt;
typedef string LabelType;
typedef vector<LabelType> LabelsBatch;
typedef LabelsBatch::iterator LabelsBatchIt;
//typedef subGraphStruct<GraphType> SubGraphType;
typedef Granule<GraphType> GranuleType;
typedef list<GranuleType> GranuleBatch;
typedef boost::graph_traits<GraphType>::vertex_descriptor vDesc;
typedef boost::graph_traits<GraphType>::vertex_iterator vIterator;
typedef boost::graph_traits<GraphType>::edge_descriptor eDesc;
typedef boost::graph_traits<GraphType>::edge_iterator eIterator;

//inexact graph matching (GED)
typedef BMF<VerticesDissimilarity, EdgesDissimilarity> GraphDissimilarityType;

//clustering algorithm
typedef MinSod<GraphType, GraphDissimilarityType> Representative;
typedef Bsas<Representative> BsasType;

//ensembles and granulation strategy
typedef BsasPartitions<BsasType> EnsemblesStrategy;
typedef Extractor1<subGraphStruct<GraphType> > ExtractionStrategy;
typedef Granulator1<EnsemblesStrategy, GranuleType, GraphType> GranulationStrategy;
typedef SymbolicHistograms<GraphDissimilarityType> EmbeddingStrategy;

//classification strategy
typedef KnnClass<vector<spare::RealType>, spare::Euclidean, LabelType> ClassifierType;

//gralg
typedef GRALG1<ExtractionStrategy, GranulationStrategy, EmbeddingStrategy, ClassifierType, GranuleBatch> GralgType;

//genetic algo
typedef NaturalType GeneType;
typedef vector<GeneType> CodeType;
typedef mtG1FitFun1 <GralgType, GraphsBatch, LabelsBatch, GraphDissimilarityType> myFitFun;
typedef mtG1FitFun2 <ClassifierType, vector<vector<spare::RealType> >, LabelsBatch> myFitFun_fs;
typedef DiscreteCode<GeneType, myFitFun_fs> EnvironmentType_fs;
typedef Genetic<EnvironmentType_fs> GeneticType_fs;
typedef DiscreteCode<GeneType, myFitFun> EnvironmentType;
typedef Genetic<EnvironmentType> GeneticType;

//test phase
typedef Test1<GranulationStrategy, EmbeddingStrategy, ClassifierType, GeneticType_fs, Projection, GranuleBatch, EnvironmentType_fs> TestType;


//command list and version
const string version = "GRALG version Image Graphs";


const string usageStr ="\
Usage: GRALG [OPTIONS]\n\
OPTIONS:\n\
   -1 arg   sets training set path to to arg\n\
   -2 arg   sets test set path to arg\n\
   -3 arg	sets validation set path to arg\n\
   -r arg   sets maximum sugraphs order to arg\n\
   -t arg   sets the step of theta to arg\n\
   -e arg	set tolerance in symbol's matching\n\
   -s arg	set seed value\n\
   -i arg   set the numeber of evolutions of gen alg\n\
   -n arg   set the number of evolutions of gen alg for features selection (only ver 1)\n\
   -a arg 	set the weight alpha used in feature selection fitness function (only ver 1)\n\
   -k arg 	set k of KNN\n\
   -o arg 	set t_RR (fitness threshold in parameters optimization)\n\
   -f arg	set t_FS (fitness threshold in feature selection optimization) \n\
   -x arg 	set value of reduction step (stop criterion) \n\
   -y arg	set value of increasing step (stop criterion) \n\
   -V arg   prints out Versions\n\
   -E arg 	prints out Embeddings\n\
   -M arg 	use median in cluster's compactness cost\n\
   -N arg 	use component-wise normalization for vector embeddings\n\
   -h       prints out usage information, the program is not run";

ptrdiff_t myrandom(ptrdiff_t i){
	srand(time(NULL));
	return rand()%i;}

ptrdiff_t (*p_myrandom)(ptrdiff_t) = myrandom;


int main(int argc, char* argv[]){

	// ******************** variable definition ***********************
	int ClOpt;
	bool printUsage = false, printVersion = false, cmlError = false, printEmbedding=false, compMed=false, normEmbedding=false, gecCodeInit=true;
	string trPath = "../GRALG_datasets/Training/";
	string tsPath = "../GRALG_datasets/Test/";
	string vsPath = "../GRALG_datasets/Validation/";

	DatasetType dataSet;
	GraphsBatch trGraphs, tsGraphs, vsGraphs, ExptrGraphs, ExpvsGraphs, ExptsGraphs;
	vector<spare::NaturalType> ExptrIDcontainer, ExpvsIDcontainer, ExptsIDcontainer;
	LabelsBatch trLabels, tsLabels, vsLabels;
	vector<pair<vector<RealType>, RealType> > TopFit;
	GralgType gralg;
	NaturalType maxOrder = 3, seed=0, Q = 1000, K = 1, evolutions = 1, RESOLUTION=100, genevol=0, fs_evolutions=500, regSize = 5;
	RealType eps=1.1, t_RR=1, tf = 0.7, phi = 0.7, ni = 0.5, reductionStep=0, increaseStep=1, bound=0.5, alpha=0.99, t_FS=1, zeroW = 0.1;
	size_t start_test, end_test, start_opt, end_opt,start_exp, end_exp;
	//BMF parameters
	spare::RealType pVInsertion=1, pVDeletion=1, pVSubs=1, pEInsertion=1, pEDeletion=1, pESubs=1;
	//Image Graphs label dissimilarity parameters
	RealType wdXc=0,wdYc=0,wdB=0,wXc=0,wYc=0,wCrCb=0,wSat=0,wBrg=0,wWlet=0,wArea=0,wSymm=0,wRnd=0,wCmp=0,wDir=0,wOr=0;

	//optimizator (On/Off --> registry enabled/disabled)
	GeneticType GeneticAlgo("On");

	//I/O class
	GRALG_IO gio;


	// command line parsing
	while ((ClOpt = getopt(argc, argv, ":1:2:3:r:l:u:s:i:f:e:n:z:x:y:o:a:k:hVEMN"))
			!= -1) {
		switch (ClOpt) {
		case 'a': {
			alpha = atof(optarg);
			break;
		}
		case 'n': {
			fs_evolutions = atoi(optarg);
			break;
		}
		case 'f': {
			t_FS = atof(optarg);
			break;
		}
		case 'V': {
			printVersion = true;
			break;
		}
		case 'E': {
			printEmbedding = true;
			break;
		}
		case 'M': {
			compMed = true;
			break;
		}
        case 'z': { //imgGraph only
            zeroW = atof(optarg);
            break;
        }
		case 'N': {
			normEmbedding = true;
			break;
		}
		case 'r': {
			maxOrder = atoi(optarg);
			break;
		}
		case 'q': {
			Q = atoi(optarg);
			break;
		}
		case 'e': {
			eps = atoi(optarg);
			break;
		}
		case 'h': {
			printUsage = true;
			break;
		}
		case 'i': {
			evolutions = atoi(optarg);
			break;
		}
		case 's': {
			seed = atof(optarg);
			break;
		}
		case 'o': {
			t_RR = atof(optarg);
			break;
		}
		case 'k': {
			K = atoi(optarg);
			break;
		}
		case '1': {
			trPath = optarg;
			break;
		}
		case '2': {
			tsPath = optarg;
			break;
		}
		case '3': {
			vsPath = optarg;
			break;
		}
		case 'x': {
			reductionStep = atof(optarg);
			break;
		}
		case 'y': {
			increaseStep = atof(optarg);
			break;
		}
		}
	}

	if (printUsage) {
		cout << usageStr << endl;
		return EXIT_SUCCESS;
	} else if (printVersion) {
		//print error
		cout << version << endl;
		return EXIT_SUCCESS;
	} else if (cmlError)
	{
		//print error
		cerr << usageStr << endl;
		return EXIT_FAILURE;
	}

	// ******************** print out configuration ************************
	cout    << "::RUN CONFIGURATION::" << endl
			<< "Maximum Subgraphs Order (r): "<< maxOrder << endl
			<< "Eps: "<<eps<<endl
			<< "BSAS Q: "<<Q<<endl
			<< "K (KNN): " << K <<endl
			<< "Evolutions: " << evolutions << endl
			<< "Seed: " << seed<<endl
			<< "Gen code resolution: " <<RESOLUTION<<endl
			<< "Accepted RR: "<<t_RR<< endl
			<< "ReductionStep: "<<reductionStep<<endl
			<< "IncreaseStep: "<<increaseStep<<endl;
	if(compMed)
		cout<< "Compactness: median"<<endl;
	else
		cout<< "Compactness: mean"<<endl;
	if(printEmbedding)
		cout<< "Printing Embeddings"<<endl;
	if(normEmbedding)
		cout<< "Component-wise embedding normalization"<<endl;
	cout 	<< "FS evolutions: "<<fs_evolutions<<endl
	        << "stop FS: "<<t_FS<<endl
			<< "alpha: "<<alpha<<endl;

	// --------- Dataset Reading ------------
	//training set load
	cout << "Loading training graphs from: " << trPath << endl;
	if (dataSet.readDataset(trPath, trGraphs, trLabels))
		cout << "OK. Loaded graphs (labels): " << trGraphs.size() << " ("
				<< trLabels.size() << ")" << endl;
	else {
		cerr << "KO loading graphs from: " << trPath << endl;
		return EXIT_FAILURE;
	}

	if (trGraphs.size() == 0) {
		cerr << "Error. Loaded 0 graphs for the training set: " << trPath
				<< endl;
		return EXIT_FAILURE;
	}

	//test set load
	cout << "Loading test graphs from: " << tsPath << endl;
	if (dataSet.readDataset(tsPath, tsGraphs, tsLabels))
		cout << "OK. Loaded graphs (labels): " << tsGraphs.size() << " ("
				<< tsLabels.size() << ")" << endl;
	else {
		cerr << "KO loading graphs from: " << tsPath << endl;
		return EXIT_FAILURE;
	}

	if (tsGraphs.size() == 0) {
		cerr << "Error. Loaded 0 graphs for the test set: " << tsPath << endl;
		return EXIT_FAILURE;
	}

	//validation set load
	cout << "Loading validation graphs from: " << vsPath << endl;
	if (dataSet.readDataset(vsPath, vsGraphs, vsLabels))
		cout << "OK. Loaded graphs (labels): " << vsGraphs.size() << " ("
				<< vsLabels.size() << ")" << endl;
	else {
		cerr << "OK loading graphs from: " << vsPath << endl;
		return EXIT_FAILURE;
	}

	if (vsGraphs.size() == 0) {
		cerr << "Error. Loaded 0 graphs for the validation set: " << vsPath
				<< endl;
		return EXIT_FAILURE;
	}

	cout <<"Processing now "<<trGraphs.size()<<" graphs"<<endl;

	//max complexity of the subgraphs (used for normalization)
	spare::NaturalType maxComplexity = maxOrder + (maxOrder*(maxOrder -1))/2;


	// *************** TR and VS Expansion *******************
	start_exp = clock();

	//build a vector with the labels of the classes
	LabelsBatch ExptrLabels, ExptsLabels, ExpvsLabels, Labels;
	for(NaturalType i=0; i<trLabels.size(); i++){
		bool check=false;
		for(NaturalType j=0; j<Labels.size(); j++){
			if(trLabels[i]==Labels[j])
				check=true;
		}
		if(!check)
			Labels.push_back(trLabels[i]);
	}


	ExtractionStrategy extractor;
	extractor.MaxOrder() = maxOrder;

	//extract subgraphs from TR
	extractor.ExtractBatch(trGraphs, ExptrGraphs);
	ExptrIDcontainer = extractor.getIDcontainer();
	for(NaturalType i=0; i<ExptrIDcontainer.size(); i++){
		ExptrLabels.push_back(trLabels[ExptrIDcontainer[i]]);
	}

	//extract subgraphs from VS
	extractor.ExtractBatch(vsGraphs, ExpvsGraphs);
	ExpvsIDcontainer = extractor.getIDcontainer();
	for(NaturalType i=0; i<ExptsIDcontainer.size(); i++){
		ExptsLabels.push_back(tsLabels[ExptsIDcontainer[i]]);
	}

	//extract subgraphs from TS
	extractor.ExtractBatch(tsGraphs, ExptsGraphs);
	ExptsIDcontainer = extractor.getIDcontainer();
	for(NaturalType i=0; i<ExpvsIDcontainer.size(); i++){
		ExpvsLabels.push_back(vsLabels[ExpvsIDcontainer[i]]);
	}

	//put the subgraphs of TR in random order
	vector<NaturalType> s;
	for(NaturalType j=0; j<ExptrGraphs.size(); j++){
		s.push_back(j);
	}
	random_shuffle (s.begin(), s.end(),p_myrandom);
	LabelsBatch sLabels;
	vector<NaturalType> sID;
	GraphsBatch sGraphs;
	for(NaturalType j=0; j<s.size(); j++){
		sLabels.push_back(ExptrLabels[s[j]]);
		sID.push_back(ExptrIDcontainer[s[j]]);
		sGraphs.push_back(ExptrGraphs[s[j]]);
	}
	ExptrLabels = sLabels;
	ExptrIDcontainer = sID;
	ExptrGraphs = sGraphs;

	end_exp = clock();
	cout << "Extracted "<<ExptrGraphs.size()<<" from TR,  "<<ExpvsGraphs.size()<<" from VS and "<<ExptsGraphs.size()<<" from TS in: "<< (double)((end_exp-start_exp)/CLOCKS_PER_SEC)<<" sec"<<endl;


	///////////////////////////////////////////////////////////
	/////////////////// OPTIMIZATION  PHASE ///////////////////
	///////////////////////////////////////////////////////////

	start_opt = clock();

	if(evolutions > 0){

	//initialization
    NaturalType codeSize = 25;/*
                               * 0 - bound (theta interval) - range [0, Qrange]
                               * 1 - ni (in function F) - range [0, 1]
                               * 2 - phi (entropy weight) - range [0, 1]
                               * 3 - t_F (thresold for F) - range [0, 1]
                               * 4 - BMF Vinsertion - range [0, 1]
                               * 5 - BMF Vdeletion - range [0, 1]
                               * 6 - BMF Vsubstitution - range [0, 1]
                               * 7 - BMF Einsertion - range [0, 1]
                               * 8 - BMF Edeletion - range [0, 1]
                               * 9 - BMF Esubstitution - range [0, 1]
                               * 10- wdXc
                               * 11- wdYc
                               * 12- wdB
                               * 13- wXc
                               * 14- wYc
                               * 15- wCrCb
                               * 16- wSat
                               * 17- wBrg
                               * 18-
                               * 19- wArea
                               * 20- wSymm
                               * 21- wRnd
                               * 22- wCmp
                               * 23- wDir
                               * 24- wOr
                               */

		cout<<"Codesize: "<<codeSize<<endl;
		vector<RealType> originalParamRange(codeSize, 1);
		//originalParamRange[0] = Qrange; //range of Q
		GeneticAlgo.EnvAgent()= EnvironmentType(codeSize, 0, RESOLUTION);
		GeneticAlgo.EnvAgent().RandSeedSetup(seed, seed);
		GeneticAlgo.RandSeedSetup(seed);
		GeneticAlgo.PopSize() = 100;

		GeneticAlgo.EnvAgent().FitnessAgent().DataSetup(ExptrGraphs, 		//expanded TR
														trLabels,			//class labels of TR graphs
														ExptrIDcontainer,   //ID of the original TR graph from which subgraphs are extracted
														ExptrLabels,		//class labels of TR subgraphs
														ExpvsGraphs,		//expanded VS
														vsLabels,			//class labels of VS graphs
														ExpvsIDcontainer,   //ID of the original VS graph from which subgraphs are extracted
														Labels,				//All the labels
														originalParamRange,	//range-values of the genes
														eps,				//tolerance value in symbols recognition
														K,					//K parameter for KNN classifier
														RESOLUTION,			//resolution of the search interval for code values
														seed,				//random seed for bsas clustering
														compMed,			//set modality for cluster's compactness computation
														Q,					//Q of BSAS
														normEmbedding,      //use component-wise normalization for the embeddings
														zeroW);             //imgGraph only


		GeneticAlgo.EnvAgent().FitnessAgent().MaxComplexity() = maxComplexity;


		cout<<"Initializing genetic algorithm..."<<endl;
		if(gecCodeInit){
	        //initialize custom solutions
	        vector<vector<NaturalType> > initSolution;
	        for(NaturalType i= 0; i<codeSize; i++){
	            vector<NaturalType> vi(codeSize,0);
	            vi[i] = RESOLUTION;
	            initSolution.push_back(vi);
	        }
	        vector<NaturalType> vi(codeSize,RESOLUTION);
	        initSolution.push_back(vi);
	        GeneticAlgo.Initialize(initSolution.begin(), initSolution.end());
		}
		else{
		    GeneticAlgo.Initialize();
		}
		cout<<"Done"<<endl;

		// -----  Optimization Cycle  -----
		spare::RealType oldFit=GeneticAlgo.GetPerformance();
		for (NaturalType i= 0; i < evolutions && GeneticAlgo.GetPerformance() < t_RR && GeneticAlgo.GetPerformance() < 1; ++i)
		{
			GeneticAlgo.StepUp();

                RealType zeroParams = 0;
                for(NaturalType j = 10; j < 25; j++){
                    if(round((RealType)GeneticAlgo.GetSolution()[j]/(RealType)RESOLUTION) == 0)
                        zeroParams++;
                }
                RealType perf = GeneticAlgo.GetPerformance();
                RealType eRR = (perf - zeroW*(RealType)zeroParams/15.0)/(1-zeroW);
			    cout << "Evol "<< i<<". Fitness: "<<perf<<", RR: " << eRR<<", zeroParams : "<<zeroParams<< endl;

			genevol++;

			if(GeneticAlgo.GetPerformance() == oldFit)
				t_RR = t_RR - reductionStep;
			else{
				if( (t_RR + increaseStep*(GeneticAlgo.GetPerformance() - oldFit) >= 1) )
						t_RR = 1;
				else
					t_RR = t_RR + increaseStep*(GeneticAlgo.GetPerformance() - oldFit);
			}

			oldFit = GeneticAlgo.GetPerformance();
			cout<<"t_RR: "<<t_RR<<endl;
		}

		//reading solutions
		bound = (RealType)GeneticAlgo.GetSolution()[0]/(RealType)RESOLUTION;
		ni = (RealType)GeneticAlgo.GetSolution()[1]/(RealType)RESOLUTION; 				 //normalize in [0,1]
		phi = (RealType)GeneticAlgo.GetSolution()[2]/(RealType)RESOLUTION;
		tf = (RealType)GeneticAlgo.GetSolution()[3]/(RealType)RESOLUTION;

        pVInsertion = (RealType)GeneticAlgo.GetSolution()[4]/(RealType)RESOLUTION;
        pVDeletion = (RealType)GeneticAlgo.GetSolution()[5]/(RealType)RESOLUTION;
        pVSubs = (RealType)GeneticAlgo.GetSolution()[6]/(RealType)RESOLUTION;
        pEInsertion = (RealType)GeneticAlgo.GetSolution()[7]/(RealType)RESOLUTION;
        pEDeletion = (RealType)GeneticAlgo.GetSolution()[8]/(RealType)RESOLUTION;
        pESubs = (RealType)GeneticAlgo.GetSolution()[9]/(RealType)RESOLUTION;

        wdXc = round((RealType)GeneticAlgo.GetSolution()[10]/(RealType)RESOLUTION);
        wdYc = round((RealType)GeneticAlgo.GetSolution()[11]/(RealType)RESOLUTION);
        wdB = round((RealType)GeneticAlgo.GetSolution()[12]/(RealType)RESOLUTION);
        wXc = round((RealType)GeneticAlgo.GetSolution()[13]/(RealType)RESOLUTION);
        wYc = round((RealType)GeneticAlgo.GetSolution()[14]/(RealType)RESOLUTION);
        wCrCb = round((RealType)GeneticAlgo.GetSolution()[15]/(RealType)RESOLUTION);
        wSat = round((RealType)GeneticAlgo.GetSolution()[16]/(RealType)RESOLUTION);
        wBrg = round((RealType)GeneticAlgo.GetSolution()[17]/(RealType)RESOLUTION);
        wWlet = round((RealType)GeneticAlgo.GetSolution()[18]/(RealType)RESOLUTION);
        wArea = round((RealType)GeneticAlgo.GetSolution()[19]/(RealType)RESOLUTION);
        wSymm = round((RealType)GeneticAlgo.GetSolution()[20]/(RealType)RESOLUTION);
        wRnd = round((RealType)GeneticAlgo.GetSolution()[21]/(RealType)RESOLUTION);
        wCmp = round((RealType)GeneticAlgo.GetSolution()[22]/(RealType)RESOLUTION);
        wDir = round((RealType)GeneticAlgo.GetSolution()[23]/(RealType)RESOLUTION);
        wOr = round((RealType)GeneticAlgo.GetSolution()[24]/(RealType)RESOLUTION);

		//extracting top regSize fitness genetic codes
		GeneticType::RegistryMap registry = GeneticAlgo.GetRegistryData();
		spare::RealType maxFit=0;
		GeneticType::CodeType maxFitCode;
		GeneticType::RegistryMap::iterator mapit;
		for(spare::NaturalType i = 0; i < regSize; i++){
			mapit = registry.begin();
			while(mapit != registry.end()){
				if((*mapit).second > maxFit){
					maxFit = (*mapit).second;
					maxFitCode = (*mapit).first;
				}
				mapit++;
			}
			std::vector<spare::RealType> maxFitCodeR;
			for(spare::NaturalType i=0; i < 10; i++){
				maxFitCodeR.push_back((spare::RealType)maxFitCode[i]/(spare::RealType)RESOLUTION*(spare::RealType)originalParamRange[i]);
			}
			for(spare::NaturalType i=10; i < maxFitCode.size(); i++){
			    maxFitCodeR.push_back(round((spare::RealType)maxFitCode[i]/(spare::RealType)RESOLUTION*(spare::RealType)originalParamRange[i]));
			}
			TopFit.push_back(std::make_pair(maxFitCodeR, maxFit));
			registry.erase(maxFitCode);
			maxFit=0;
		}


	}
	else{

		cout<<endl<<"No optimization..."<<endl;

	}

	end_opt = clock();

	//print optimization results
	cout 	<<"Parameters: "<<endl
			<<"bound: "<<bound<<endl
			<<"ni: "<<ni<<endl
			<<"phi: "<<phi<<endl
			<<"tf: "<<tf<<endl
	        <<"pVInsertion: "<<pVInsertion<<endl
            <<"pVDeletion: "<<pVDeletion<<endl
            <<"pVSubs: "<<pVSubs<<endl
            <<"pEInsertion: "<<pEInsertion<<endl
            <<"pEDeletion: "<<pEDeletion<<endl
            <<"pESubs: "<<pESubs<<endl
	        <<"wdXc: "<<wdXc<<endl
            <<"wdYc: "<<wdYc<<endl
            <<"wdB: "<<wdB<<endl
            <<"wXc: "<<wXc<<endl
            <<"wYc: "<<wYc<<endl
            <<"wCrCb: "<<wCrCb<<endl
            <<"wSat: "<<wSat<<endl
            <<"wBrg: "<<wBrg<<endl
            <<"wWlet: "<<wWlet<<endl
            <<"wArea: "<<wArea<<endl
            <<"wSymm: "<<wSymm<<endl
            <<"wRnd: "<<wRnd<<endl
            <<"wCmp: "<<wCmp<<endl
            <<"wDir: "<<wDir<<endl
            <<"wOr: "<<wOr<<endl;


	//////////////////////////////////////////////////////
	///////////////////// TEST PHASE /////////////////////
	//////////////////////////////////////////////////////

	start_test = clock();
	vector<RealType> RRvec;
	vector<NaturalType> alphabetVec, featVec;

	vector<pair<vector<RealType>, RealType> >::iterator TopFitIT = TopFit.begin();
	for(NaturalType it = 0; it < regSize; it++){

	    vector<RealType> geneticCode_i = (*TopFitIT).first;
	    TopFitIT++;

        bound = geneticCode_i[0];
        ni = geneticCode_i[1];
        phi = geneticCode_i[2];
        tf = geneticCode_i[3];
        pVInsertion = geneticCode_i[4];
        pVDeletion = geneticCode_i[5];
        pVSubs = geneticCode_i[6];
        pEInsertion = geneticCode_i[7];
        pEDeletion = geneticCode_i[8];
        pESubs = geneticCode_i[9];
        wdXc = geneticCode_i[10];
        wdYc = geneticCode_i[11];
        wdB = geneticCode_i[12];
        wXc = geneticCode_i[13];
        wYc = geneticCode_i[14];
        wCrCb = geneticCode_i[15];
        wSat = geneticCode_i[16];
        wBrg = geneticCode_i[17];
        wWlet = geneticCode_i[18];
        wArea = geneticCode_i[19];
        wSymm = geneticCode_i[20];
        wRnd = geneticCode_i[21];
        wCmp = geneticCode_i[22];
        wDir = geneticCode_i[23];
        wOr = geneticCode_i[24];

        TestType testAgent;
        GraphDissimilarityType mDiss;

        //setup ensembler
        testAgent.EnsembleAgent().ClustAlgo().RepInit().DissAgent().PVertexInsertion()= pVInsertion;
        testAgent.EnsembleAgent().ClustAlgo().RepInit().DissAgent().PVertexDeletion()= pVDeletion;
        testAgent.EnsembleAgent().ClustAlgo().RepInit().DissAgent().PVertexSubstitution()= pVSubs;
        testAgent.EnsembleAgent().ClustAlgo().RepInit().DissAgent().PEdgeInsertion()= pEInsertion;
        testAgent.EnsembleAgent().ClustAlgo().RepInit().DissAgent().PEdgeDeletion()= pEDeletion;
        testAgent.EnsembleAgent().ClustAlgo().RepInit().DissAgent().PEdgeSubstitution()= pESubs;

        testAgent.EnsembleAgent().ClustAlgo().RepInit().DissAgent().NormValue() = maxComplexity;
        testAgent.EnsembleAgent().ClustAlgo().RepInit().DissAgent().Normalize() = true;

        testAgent.EnsembleAgent().ClustAlgo().RepInit().DissAgent().EdgesDissAgent().WdXc() = wdXc;
        testAgent.EnsembleAgent().ClustAlgo().RepInit().DissAgent().EdgesDissAgent().WdYc() = wdYc;
        testAgent.EnsembleAgent().ClustAlgo().RepInit().DissAgent().EdgesDissAgent().WdB() = wdB;
        testAgent.EnsembleAgent().ClustAlgo().RepInit().DissAgent().VerticesDissAgent().WXc() = wXc;
        testAgent.EnsembleAgent().ClustAlgo().RepInit().DissAgent().VerticesDissAgent().WYc() = wYc;
        testAgent.EnsembleAgent().ClustAlgo().RepInit().DissAgent().VerticesDissAgent().WCrCb() = wCrCb;
        testAgent.EnsembleAgent().ClustAlgo().RepInit().DissAgent().VerticesDissAgent().WSat() = wSat;
        testAgent.EnsembleAgent().ClustAlgo().RepInit().DissAgent().VerticesDissAgent().WBrg() = wBrg;
        testAgent.EnsembleAgent().ClustAlgo().RepInit().DissAgent().VerticesDissAgent().WWlet() = wWlet;
        testAgent.EnsembleAgent().ClustAlgo().RepInit().DissAgent().VerticesDissAgent().WArea() = wArea;
        testAgent.EnsembleAgent().ClustAlgo().RepInit().DissAgent().VerticesDissAgent().WSymm() = wSymm;
        testAgent.EnsembleAgent().ClustAlgo().RepInit().DissAgent().VerticesDissAgent().WRnd() = wRnd;
        testAgent.EnsembleAgent().ClustAlgo().RepInit().DissAgent().VerticesDissAgent().WCmp() = wCmp;
        testAgent.EnsembleAgent().ClustAlgo().RepInit().DissAgent().VerticesDissAgent().WDir() = wDir;
        testAgent.EnsembleAgent().ClustAlgo().RepInit().DissAgent().VerticesDissAgent().WOr() = wOr;

        //setup embedder
        testAgent.EmbeddingAgent().DissAgent().PVertexInsertion()= pVInsertion;
        testAgent.EmbeddingAgent().DissAgent().PVertexDeletion()= pVDeletion;
        testAgent.EmbeddingAgent().DissAgent().PVertexSubstitution()= pVSubs;
        testAgent.EmbeddingAgent().DissAgent().PEdgeInsertion()= pEInsertion;
        testAgent.EmbeddingAgent().DissAgent().PEdgeDeletion()= pEDeletion;
        testAgent.EmbeddingAgent().DissAgent().PEdgeSubstitution()= pESubs;

        testAgent.EmbeddingAgent().DissAgent().NormValue() = maxComplexity;
        testAgent.EmbeddingAgent().DissAgent().Normalize() = true;

        testAgent.EmbeddingAgent().DissAgent().EdgesDissAgent().WdXc() = wdXc;
        testAgent.EmbeddingAgent().DissAgent().EdgesDissAgent().WdYc() = wdYc;
        testAgent.EmbeddingAgent().DissAgent().EdgesDissAgent().WdB() = wdB;
        testAgent.EmbeddingAgent().DissAgent().VerticesDissAgent().WXc() = wXc;
        testAgent.EmbeddingAgent().DissAgent().VerticesDissAgent().WYc() = wYc;
        testAgent.EmbeddingAgent().DissAgent().VerticesDissAgent().WCrCb() = wCrCb;
        testAgent.EmbeddingAgent().DissAgent().VerticesDissAgent().WSat() = wSat;
        testAgent.EmbeddingAgent().DissAgent().VerticesDissAgent().WBrg() = wBrg;
        testAgent.EmbeddingAgent().DissAgent().VerticesDissAgent().WWlet() = wWlet;
        testAgent.EmbeddingAgent().DissAgent().VerticesDissAgent().WArea() = wArea;
        testAgent.EmbeddingAgent().DissAgent().VerticesDissAgent().WSymm() = wSymm;
        testAgent.EmbeddingAgent().DissAgent().VerticesDissAgent().WRnd() = wRnd;
        testAgent.EmbeddingAgent().DissAgent().VerticesDissAgent().WCmp() = wCmp;
        testAgent.EmbeddingAgent().DissAgent().VerticesDissAgent().WDir() = wDir;
        testAgent.EmbeddingAgent().DissAgent().VerticesDissAgent().WOr() = wOr;

        //setup dissimilarity
        mDiss.EdgesDissAgent().WdXc() = wdXc;
        mDiss.EdgesDissAgent().WdYc() = wdYc;
        mDiss.EdgesDissAgent().WdB() = wdB;
        mDiss.VerticesDissAgent().WXc() = wXc;
        mDiss.VerticesDissAgent().WYc() = wYc;
        mDiss.VerticesDissAgent().WCrCb() = wCrCb;
        mDiss.VerticesDissAgent().WSat() = wSat;
        mDiss.VerticesDissAgent().WBrg() = wBrg;
        mDiss.VerticesDissAgent().WWlet() = wWlet;
        mDiss.VerticesDissAgent().WArea() = wArea;
        mDiss.VerticesDissAgent().WSymm() = wSymm;
        mDiss.VerticesDissAgent().WRnd() = wRnd;
        mDiss.VerticesDissAgent().WCmp() = wCmp;
        mDiss.VerticesDissAgent().WDir() = wDir;
        mDiss.VerticesDissAgent().WOr() = wOr;

        mDiss.PVertexInsertion()= pVInsertion;
        mDiss.PVertexDeletion()= pVDeletion;
        mDiss.PVertexSubstitution()= pVSubs;
        mDiss.PEdgeInsertion()= pEInsertion;
        mDiss.PEdgeDeletion()= pEDeletion;
        mDiss.PEdgeSubstitution()= pESubs;

        mDiss.NormValue() = maxComplexity;
        mDiss.Normalize() = true;

        //evaluate theta range
        GraphsBatchIt sampIT;
        RealType min, max, SOD, min100=0, max100=0, SOD100=0, tMin, tMax, tStep;
        NaturalType Nsamples = 500;

        for(NaturalType i = 0; i < Nsamples; i++){
            NaturalType index = (seed*i)%ExptrGraphs.size();
            min=2.0, max=0, SOD=0;
            sampIT = ExptrGraphs.begin();

            while(sampIT!=ExptrGraphs.end())
            {
                RealType d=mDiss.Diss(*sampIT, ExptrGraphs[index]);

                SOD+=d;

                if(d!=0&&d<min)
                    min=d;

                if(d>max)
                    max=d;

                sampIT++;
            }
            SOD=SOD/(RealType)(ExptrGraphs.size()-1);

            SOD100+=SOD;
            min100+=min;
            max100+=max;
        }
        tMin = min100/Nsamples + (SOD100/Nsamples - min100/Nsamples)*bound;
        tMax = max100/Nsamples - (max100/Nsamples - SOD100/Nsamples)*bound;
        tMin = 0;
        tMax = 1;
        tStep = (tMax - tMin)/10;

        //initialization of test phase
        testAgent.GranulationAgent().Ni() = ni;
        testAgent.GranulationAgent().Ni() = phi;
        testAgent.GranulationAgent().T_F() = tf;
        testAgent.GranulationAgent().Eps() = eps;
        testAgent.EnsembleAgent().thetaStep() = tStep;
        testAgent.EnsembleAgent().thetaRange() = std::make_pair(tMin, tMax);
        testAgent.EnsembleAgent().getQ() = Q;
        testAgent.EnsembleAgent().ClustAlgo().RepInit().RandSeedSetup(seed);
        testAgent.ClassificationAgent().K() = K;
        testAgent.FitRank() = it;

        //set modality for cluster's compactness computation
        if(compMed){
            testAgent.GranulationAgent().CompMed() = true;
        }

        //use component-wise embedding normalization
        if(normEmbedding){
            testAgent.NormEmbedding()=true;
        }

        //write embedding on file
        if(printEmbedding){
            testAgent.PrintEmbedding() = true;
        }

        //processing
        RealType RR;
        RR = testAgent.Process(ExptrGraphs, trLabels, ExptrLabels, ExptrIDcontainer, ExpvsGraphs, vsLabels, ExpvsIDcontainer, ExptsGraphs, tsLabels, ExptsIDcontainer, Labels, fs_evolutions, seed, K, alpha, t_FS);

        RRvec.push_back(RR);
        alphabetVec.push_back(testAgent.AlphabetSize());
        featVec.push_back(testAgent.SelectedFeatures());
	}

    end_test = clock();

    spare::RealType timeOpt = (spare::RealType)((end_opt-start_opt)/CLOCKS_PER_SEC)/60;
    spare::RealType timeTest = (spare::RealType)((end_test-start_test)/CLOCKS_PER_SEC)/60;

    //write log
    gio.WriteLogV("/home/filippo/Desktop/Granules/", seed, K, timeOpt+timeTest, RRvec, TopFit, alphabetVec, featVec, genevol);


	return EXIT_SUCCESS;
}

